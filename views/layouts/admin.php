<?php

/* @var $this \yii\web\View */
/* @var $content string */
/* @var $page_title string */
/* @var $main_title string */
/* @var $main_url string */

extract($this->params['admin']);

?>
<?php $this->beginContent('@app/views/layouts/main.php'); ?>
    <div class="breadcrumbs">
        <div class="inner_width">
            <a href="/admin"><i class="ichome"></i>Админ-панель</a>
            <?php if (isset($main_url)): ?>
                <a href="/<?= $main_url ?>"><?= $main_title ?></a>
                <span class="curr"><?= $page_title ?></span>
            <?php else: ?>
                <span class="curr"><?= $page_title ?></span>
            <?php endif ?>
        </div>
    </div>
    <div class="page_ttl admin_page">
        <div class="inner_width">
            <h1><?=$page_title?></h1>
        </div>
    </div>
<div class="admin-content">
<?= $content; ?>
</div>

<?php $this->endContent(); ?>