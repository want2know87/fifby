<?php
use yii\helpers\Html;
use yii\data\ActiveDataProvider;

use app\models\SiteTemplates;

/** @var SiteTemplates $template */
$template = SiteTemplates::findOne(['fscode' => 'news/index']);
$this->title = $template->fstitle;
//$this->seoFields = $template->fstemplate_seo;
eval("?>" . $template->fstemplate_code . "<?php ");
return;

/* @var $this yii\web\View */

$years = Yii::$app->db->createCommand('SELECT DISTINCT YEAR(fdcreate_date) year FROM news order by 1 desc')->queryAll();

$query = \app\models\News::find()->orderBy(['fdcreate_date' => SORT_DESC]);
if(isset($_GET['year'])) {
  $query->where(['YEAR(fdcreate_date)' => (int)$_GET['year']]);
}

$provider = new ActiveDataProvider([
    'query' => $query,
    'pagination' => [
        'pageSize' => 7,
    ],
]);

?>
<div class="breadcrumbs">
  <div class="inner_width">
    <a href="<?php echo \yii\helpers\Url::to(['site/index']) ?>"><i class="ichome"></i>Главная</a>
    <span class="curr">Новости</span>
  </div>
</div>
<div class="page_ttl mod">
  <div class="inner_width">
    <h1>Новости</h1>
  </div>
</div>
  <div class="inner_width ">
    <ul class="aboutlist">
      <li class="act"><a href="#">Новости компании</a></li>
    </ul>
    <ul class="newsfilter">

      <?php foreach($years as $year): ?>
        <li><a href="<?php echo \yii\helpers\Url::to(['news/index', 'year' => $year['year']]) ?>" class="year <?php if((@$_GET['year'] == $year['year']) || count($years) == 1) echo " act" ?>">
          <?php echo $year['year'] ?>
        </a></li>
      <?php endforeach; ?>
    </ul>

    <div class="newswr">
      <div class="filterwrap mod">
        <div class="barinfo  hidden1040">
          <span class="big">Следите за новостями</span>
          Мы пишем только о том, что проверили на собственном опыте. Подпишитесь на обновления блога, чтобы не упускать полезные знания.
          <span class="subscribe_text"></span>
          <form class="subscr_form">
            <input type="text" name="mail" placeholder="Введите ваш email">
            <input type="submit" value="OK">
          </form>
        </div>
      </div>
      <div class="catalog mod">
        <div class="catalog_panel">
          <div class="numshow">
            <?php
            $provider->pagination->pageSize = 10;
            $provider->pagination->page = max((int)Yii::$app->request->get('page') -1, 0);
            $provider->pagination->totalCount = $provider->totalCount;
            for ( $i = 0; $i<$provider->pagination->pageCount; $i++) {
              echo "<a href=\"", \yii\helpers\Url::to(['news/index', 'page' => $i + 1 ]),"\" class=\"", ($i == $provider->pagination->page ? 'curr' : '') ,"\">", $i+1,"</a>";
            }
            ?>
          </div>
        </div>

<?php

foreach ($provider->models as $model) {
  echo $this->render('view', ['model' => $model]);
}

?>
    </div>
    </div>
  </div>
