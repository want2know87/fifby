<?php

use yii\helpers\ArrayHelper;

$prices = ['BYN', 'RUR', 'USD'];

$findedItems = \app\models\SelectsItems::find()->where('fiselect_id = ' . (int) $id)->all();
$in = [0];
foreach ($findedItems as $fi) {
    $in[] = $fi->fiitem_id;
}

$items = Yii::$app->db->createCommand('SELECT
                                            i.*, s.fssale_name
                                        FROM
                                            items i
                                        LEFT JOIN sales s ON i.fisale_id = s.fisale_id
                                        /*LEFT JOIN params_values pv ON pv.fiitem_id = i.fiitem_id
                                        LEFT JOIN params_cat pc ON pv.fiparam_id = pc.fiparam_id*/
                                        WHERE
                                            i.fiitem_id IN(
                                                ' . implode(',', $in) . ')
                                        ORDER BY fiorder')->queryAll();

$itemsIds = ArrayHelper::map($items, 'fiitem_id', 'fsitem_name');

$itemsValues = (new yii\db\Query())
    ->select(['pv.fiitem_id', 'pv.fiparam_id', 'pc.fsparam_name', 'pv.fsparam_value'])
    ->from('params_values pv')
    ->leftJoin('params_cat pc', 'pc.fiparam_id = pv.fiparam_id')
    ->where(['pv.fiitem_id' => array_keys($itemsIds)])
    ->orderBy('pv.fiorder')
    ->all(Yii::$app->db);

$itemsValues = ArrayHelper::map($itemsValues, 'fiparam_id', function ($el) {
    return [$el['fsparam_name'] => $el['fsparam_value']];
}, 'fiitem_id');

?>

<?php foreach ($items as $item): ?>
    <?php echo $this->render('/catalog/item-new', ['item' => $item, 'prices' => $prices, 'itemsValues' => $itemsValues, 'breadCrumbs' => null]) ?>
<?php endforeach; ?>