<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Partners;

/**
 * PartnersSearch represents the model behind the search form about `app\models\Partners`.
 */
class PartnersSearch extends Partners
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fipartner_id'], 'integer'],
            [['fspartner_name', 'fspartner_image', 'fspartner_link'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Partners::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'fipartner_id' => $this->fipartner_id,
        ]);

        $query->andFilterWhere(['like', 'fspartner_name', $this->fspartner_name])
            ->andFilterWhere(['like', 'fspartner_image', $this->fspartner_image])
            ->andFilterWhere(['like', 'fspartner_link', $this->fspartner_link]);

        return $dataProvider;
    }
}
