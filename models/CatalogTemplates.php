<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "catalog_templates".
 *
 * @property integer $fitemplate_id
 * @property string $fstemplate_code
 * @property string $fstemplate_text
 */
class CatalogTemplates extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'catalog_templates';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fstemplate_code'], 'required'],
            [['fstemplate_text'], 'string'],
            [['fstemplate_code'], 'string', 'max' => 32],
            [['fstemplate_code'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fitemplate_id' => Yii::t('app', 'Fitemplate ID'),
            'fstemplate_code' => Yii::t('app', 'Fstemplate Code'),
            'fstemplate_text' => Yii::t('app', 'Fstemplate Text'),
        ];
    }
}
