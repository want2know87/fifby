<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ItemsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Items');
$page = (int)Yii::$app->request->get('page');
$this->params['admin'] = [
    'page_title' => $this->title,
];
$js = <<< JS
$(function() {
    $('table').on('click', 'tr button.save-order', function() {
        var button = $(this);
        button.button('loading');
        var name = $(this).closest('td').find('input').attr('name');
        $.get('%urlSaveOrder%', {id : $(this).closest('td').find('input').data('id'), order : $(this).closest('td').find('input').val()})
        .always(function() {
            button.button('reset');
        });
    });
});
JS;

$js = str_replace(
    array(
        '%urlSaveOrder%'
    ), array(
    \yii\helpers\Url::to(['items/update-order']),
), $js);
\yii\bootstrap\BootstrapPluginAsset::register($this);
$this->registerJs($js);


?>
<div class="items-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create {modelClass}', [
    'modelClass' => Yii::t('app','Item'),
]), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'fiitem_id',
            'fsitem_name',
            /*'fsitem_old_desc:ntext',*/
            #'fsitem_picture' => [
            #    'class' => \yii\grid\DataColumn::className(),
            #    'attribute' => 'fsitem_picture',
            #    'format' => 'raw',
            #    'value' => function($item) {
            #        /** @var $item app\Models\Items */
            #      return Html::img(Yii::$app->request->baseUrl . '/' . $item->fsitem_picture, ['style' => 'max-width:200px;']);
            #    },
            #],
            'fsitem_small_desc:ntext',
            'order' => [
                'class' => \yii\grid\DataColumn::className(),
                'attribute' => 'fiorder',
                'format' => 'raw',
                'value' => function($model) {
                    /** @var \app\models\Items $model */
                    return Html::textInput('order[' . $model->fiitem_id . ']', $model->fiorder, ['style' => 'width:100%;', 'data-id' => $model->fiitem_id]) . Html::button('Сохранить', ['class' => 'btn btn-primary save-order', "data-loading-text" => "Сохранение...", 'style' => 'margin-left:5px;']);
                },
                'filter' => true, // CatalogRu::find()->orderBy('fiorder')->asArray()->all(),
            ],
            // 'fsparams_array:ntext',
            // 'fianother_customer',
            // 'fsitem_customer',
            // 'fitemplate_id',
            // 'fiitem_catalog_id',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'urlCreator' => function($action, $model, $key, $index) use($page) {
                    $params = is_array($key) ? $key : ['id' => (string) $key];
                    $params[0] = $action;
                    $params += ['page' => $page];

                    return Url::toRoute($params);
                },
            ],
        ],
    ]); ?>

</div>
