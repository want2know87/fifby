<?php

use yii\helpers\Html;
use app\models\CatalogRu;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CatalogSearchRu */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $subcat int */

$prices = ['BYN', 'RUR', 'USD'];

$js = <<<JS

jQuery(function($) {
  var cache = $('.filterwrap');
  function fixDiv() {
    if ($(window).scrollTop() > 448 && $('.catalog').height() > $('.filterwrap').height() && !$('#js-filtertogg').is(':visible')) {
      if( $(window).scrollTop() > ($(document).height() - 80 - $('.filterwrap').height() - $('footer').height())) {
        cache.css({
          'position': 'relative',
          'top': ($(document).height() - 530 - $('footer').height() - $('.filterwrap').height()) + 'px'
        });
        return;
      }
      else {
        cache.css({
          'position': 'fixed',
          'top': '10px'
        });
      }
    }
    else
      cache.css({
        'position': 'relative',
        'top': 'auto'
      });
  }
  $(window).scroll(fixDiv);
  fixDiv();
});


         function fixaside(){
            if($(window).height()>931){
                var scroll_top = $(this).scrollTop();
                var off0 = $(".catalog").offset();
                var barr0 = off0.top;
             if (scroll_top < barr0) {
                 $('.filterwrap').removeClass('fix');
             }
             if (scroll_top > barr0) {
                 $('.filterwrap').addClass('fix');
             }
            }
        }
        $(document).ready(function(){
            //fixaside();
        });
        $(window).scroll(function() {
            //fixaside();
        });

  $(function() {

    var el = $(".filter");
    var start = el.offset();



    var fixedFilters = function() {
      if($('.catalog').height() > window.pageYOffset && !$('#js-filtertogg').is(':visible')) {
        el.css('margin-top', (0 > (window.pageYOffset-start.top) ? 0 : (window.pageYOffset-start.top)) + "px");
      }
    };

    //window.addEventListener("scroll", fixedFilters, false);
    //fixedFilters();

    var currentFilters = {};

    var filterFunction = function() {
      $('.catalog .catalog_item').each(function() {
        var show = true;
        var item = $(this);
        var params = JSON.parse(item.find('.hidden-info').html());
        for(var index in currentFilters) {
          if(currentFilters[index] == null) {
            continue;
          }
          if(index == 'sale' && currentFilters[index] == true) {
            show = item.hasClass('sale');
            if(!show) {
              break;
            }
            continue;
          }
          if(index == 'newFlag' && currentFilters[index] == true) {
            show = item.hasClass('newFlag');
            if(!show) {
              break;
            }
            continue;
          }
          if(params == null || !params.hasOwnProperty(index) || params[index] != currentFilters[index]) {
            show = false;
            break;
          }
        }

        if(show) {
          item.show();
        }
        else {
          item.hide();
        }

      });

      checkFilters();
      $('html, body').animate({
        scrollTop: $('.container').offset().top
      }, 500);
    };
    function declOfNum(number, titles) {
        cases = [2, 0, 1, 1, 1, 2];
        return titles[ (number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5] ];
    }
    var checkFilters = function () {
      var qty_div = $('.catalog .catalog_item:visible').length;
      $('.catalog .catalog_panel .num').text(qty_div + ' ' + declOfNum(qty_div, ['товар', 'товара', 'товаров']));
      if(qty_div == 0) {
          $('.catalog .catalog_error').show();
        }else {
          $('.catalog .catalog_error').hide();
        }
      var filters = $('.filter .filter_item:not(.sale):not(.reset)');
      filters.find('select').removeClass('disabled');
      filters.find('li').removeClass('disabled');
      var elements = $('.catalog .catalog_item:visible');
      filters.each(function() {
        var self = $(this);
        var id = self.find('select').data('filter');
        if(currentFilters.hasOwnProperty(id) && currentFilters[id] != null) {
          return true;
        }
        else {
          var findedElements = [];
          elements.each(function() {
            var element = $(this);
            var params = JSON.parse(element.find('.hidden-info').html());
            if(params.hasOwnProperty(id)) {
              findedElements.push(params[id]);
            }
          });
          if(findedElements.length) {
            self.find('select option:not([value=""])').addClass('disabled').attr('disabled', 'disabled');
            for(var i = 0; i< findedElements.length; i++) {
              self.find('select option:contains("'+ findedElements[i] + '")').removeClass('disabled').removeAttr('disabled');
            }
          }
          else {
            //self.find('button').addClass('disabled');
            self.find('select option:not([value=""])').attr('disabled', 'disabled').addClass('disabled');
          }
        }
      });
    };

    function delay(f, ms) {

      return function() {
        var savedThis = this;
        var savedArgs = arguments;
        $('#loader').show(0).delay(ms).hide(0);

        setTimeout(function() {
          f.apply(savedThis, savedArgs);
        }, ms);
      };
    }

    var delayFilter = delay(filterFunction, 500);

    $('.reserfilter').click(function() {
      $('.filter .filter_item').each(function() {
        var el = $(this);
        el.find('select').val('');
        if(el.find('input[type=checkbox]').length && el.find('input[type=checkbox]').is(':checked')) {
          el.find('input[type=checkbox]').click();
        }
      });
      $('.filter_item select').each(function(){
        $(this).removeClass('bl');
        $(this).parent().find('span').removeClass('white');
      });
      currentFilters = {};
      delayFilter();
      return false;
    });

    $('#checkbox1').click(function() {
      if($(this).is(':checked')) {
        currentFilters['sale'] = true;
      }
      else {
        currentFilters['sale'] = null;
      }
      delayFilter();
    });

    $('#checkbox2').click(function() {
      if($(this).is(':checked')) {
        currentFilters['newFlag'] = true;
      }
      else {
        currentFilters['newFlag'] = null;
      }
      delayFilter();
    });

    $('.filter .filter_item').on('change', 'select', function() {
      var self = $(this);
      var filterId = $(this).data('filter');
      if (self.val() == ''){
      for (var key in currentFilters) {
    if (currentFilters[key] == filterId) {
        currentFilters.splice(key, 1);
    }
}
      delete currentFilters[filterId];
      } else {
        currentFilters[filterId] = self.val();
      }
      delayFilter();
    });

  });
  $(document).ready(function(){
    /*for filter*/
    $('#js-filtertogg').unbind('click').click(function(){
      $('#js-filter').slideToggle(600);
      return false;
    });
    $('.filter_item select').change(function(){
      if($(this).val()!=''){
        $(this).addClass('bl');
        $(this).parent().find('span').addClass('white');
      }
      else{
        $(this).removeClass('bl');
        $(this).parent().find('span').removeClass('white');
      }
    });
    /*for accordion*/
    $('.linkdown').click(function(){
      if($(this).hasClass('act')){
        $(this).removeClass('act');
        $(this).next().slideToggle(600);
      }
      else{
        $('.linkdown').removeClass('act');
        $('.accordion_wrap p').each(function(){
          if($(this).is(':visible')) { $(this).slideToggle(600); }
        });
        $(this).next().slideToggle(600);
        $(this).toggleClass('act');
      }
      return false;
    });
  });
JS;

\yii\web\JqueryAsset::register($this);
$this->registerJs($js);

/** @var CatalogRu $catInfo */
$catInfo = CatalogRu::find()->where(['ficatalog_id' => $subcat])->andWhere(['fivisible' => 1])->one();



$this->title = $catInfo->fsname;
$this->seoFields = $catInfo->fsseo;

$podcats = Yii::$app->db->createCommand('SELECT * FROM catalog_ru WHERE fiparent_catalog_id = :parentid ORDER BY fiorder', ['parentid' => $subcat])->queryAll();
$podcatsIds = ArrayHelper::map($podcats, 'ficatalog_id', 'fsname');

$items = Yii::$app->db->createCommand('SELECT
                                            i.*, s.fssale_name
                                        FROM
                                            items i
                                        LEFT JOIN sales s ON i.fisale_id = s.fisale_id
                                        /*LEFT JOIN params_values pv ON pv.fiitem_id = i.fiitem_id
                                        LEFT JOIN params_cat pc ON pv.fiparam_id = pc.fiparam_id*/
                                        WHERE
                                            i.fiitem_id IN(
                                                SELECT
                                                    fiitem_id
                                                FROM
                                                    catalog_items
                                                WHERE
                                                    ficatalog_id IN (' . implode(',', array_merge([(int)$subcat], array_keys($podcatsIds))) . ')
                                            )
                                        ORDER BY fiorder')->queryAll();

$itemsIds = ArrayHelper::map($items, 'fiitem_id', 'fsitem_name');

$itemsValues = (new yii\db\Query())
    ->select(['pv.fiitem_id', 'pv.fiparam_id', 'pc.fsparam_name', 'pv.fsparam_value'])
    ->from('params_values pv')
    ->leftJoin('params_cat pc', 'pc.fiparam_id = pv.fiparam_id')
    ->where(['pv.fiitem_id' => array_keys($itemsIds)])
    ->orderBy('pv.fiorder')
    ->all(Yii::$app->db);

$itemsValues = ArrayHelper::map($itemsValues, 'fiparam_id', function ($el) {
    return [$el['fsparam_name'] => $el['fsparam_value']];
}, 'fiitem_id');

$podItems = [];
$searchAr = [0];

$filtersId = $catInfo->filters;
if (empty($filtersId)) {
    $filtersId = [0];
} else {
    $filtersId = json_decode($filtersId, true);
}

if (empty($filtersId)) {
    $filtersId = [0];
}

$catsIds = CatalogRu::find()->where(['fiparent_catalog_id' => (int)$catInfo->ficatalog_id])->all();
$catsIds = \yii\helpers\ArrayHelper::map($catsIds, 'ficatalog_id', 'fsname');
$catsIds = array_merge(
    [(int)$catInfo->ficatalog_id],
    array_keys($catsIds)
);

$filters = Yii::$app->db->createCommand('SELECT DISTINCT
    pc.fiparam_id as `param_id`, pc.fsparam_name AS `name`, pc.fsparam_name_en AS `name_en`
FROM
    params_values pv
LEFT JOIN params_cat pc ON pv.fiparam_id = pc.fiparam_id
WHERE
    pv.fiitem_id IN(
        SELECT
            fiitem_id
        FROM
            catalog_items
        WHERE
            ficatalog_id IN (' . implode(',', $catsIds) . ')
    )
    AND
    pc.fiparam_id IN (' . implode(', ', $filtersId) . ')
ORDER BY
    (
        CASE
        WHEN pv.fiorder IS NULL THEN
            99999
        ELSE
            pv.fiorder
        END
    )')->queryAll();

$values = Yii::$app->db->createCommand('SELECT DISTINCT
    pv.fiparam_value_id `value_id`, pv.fiparam_id as `param_id`, pv.fsparam_value as `value`
FROM
    params_values pv
LEFT JOIN params_cat pc ON pv.fiparam_id = pc.fiparam_id
WHERE
    pv.fiitem_id IN(
        SELECT
            fiitem_id
        FROM
            catalog_items
        WHERE
            ficatalog_id IN (' . implode(',', $catsIds) . ')
    )
    GROUP BY pv.fiparam_id,fsparam_value
ORDER BY
    (
        CASE
        WHEN pv.fiorder IS NULL THEN
            99999
        ELSE
            pv.fiorder
        END
    )')->queryAll();

$values = ArrayHelper::map($values, 'value_id', 'value', 'param_id');
$filters = ArrayHelper::map($filters, 'param_id', Yii::$app->language != 'en' ? 'name' : 'name_en');


foreach ($podcats as $pod) {
    $podItems[$pod['ficatalog_id']] = [
        'id' => $pod['ficatalog_id'],
        'name' => $pod['fsname'],
        'elements' => [],
    ];
    $searchAr[] = (int)$pod['ficatalog_id'];
}

$itemsPodcats = Yii::$app->db->createCommand('SELECT DISTINCT
                                            i.*, ci.ficatalog_id as cat
                                        FROM
                                            items i
                                        /*LEFT JOIN params_values pv ON pv.fiitem_id = i.fiitem_id*/
                                        INNER JOIN catalog_items ci ON i.fiitem_id = ci.fiitem_id
                                        WHERE
                                            i.fiitem_id IN(
                                                SELECT
                                                    fiitem_id
                                                FROM
                                                    catalog_items
                                                WHERE
                                                    ficatalog_id IN (' . implode(',', $searchAr) . ')
                                            )
                                        ORDER BY fiorder')->queryAll();

foreach ($itemsPodcats as $item) {
    $podItems[$item['cat']]['elements'][] = $item;
}

?>
<?php echo $this->render('/catalog/breadcrumb', ['breadcrumb' => $breadCrumbs]) ?>
<div class="page_ttl">
    <div class="inner_width">
        <h1><?php echo $catInfo->fsname ?></h1>
    </div>
</div>
<div class="inner_width ovfl">
    <?php if (strlen($catInfo->fsdestination) || strlen($catInfo->fswork) || strlen($catInfo->fsuse)): ?>
        <div class="accordion_wrap">
            <?php if (strlen($catInfo->fsdestination)): ?>
                <a href="#" class="linkdown"><?=Yii::t('app', 'fsdestination')?></a>
                <p><?php echo $catInfo->fsdestination; ?></p>
            <?php endif; ?>
            <?php if (strlen($catInfo->fswork)): ?>
                <a href="#" class="linkdown"><?=Yii::t('app', 'fswork')?></a>
                <p><?php echo $catInfo->fswork ?></p>
            <?php endif; ?>
            <?php if (strlen($catInfo->fsuse)): ?>
                <a href="#" class="linkdown"><?=Yii::t('app', 'fsuse')?></a>
                <p><?php echo $catInfo->fsuse; ?></p>
            <?php endif; ?>
        </div>
    <?php endif; ?>

    <br>


    <div class="filterwrap">
        <?=$this->render('_test-form')?>
        <form class="filter">
            <a href="#" class="reserfilter hide visible1040" id="js-filtertogg">Показать/Скрыть фильтр</a>

            <div id='js-filter'>
                <?php foreach ($filters as $id => $filter): ?>
                    <?php
                    if (empty($values[$id])) {
                        continue;
                    } else {
                        //$values[$id] = ['' => 'Не выбрано'] + $values[$id];
                        $options = $values[$id];
                        asort($options, SORT_NUMERIC);
                        $values[$id] = ['' => Yii::t('app', 'app.list.empty')] + $options;
                    }
                    ?>
                    <div class="filter_item">
                        <span><?php echo $filter; ?></span>

                        <div class="sel mod">
                            <span class="ar"></span>
                            <select data-filter="<?= $id ?>" data-active="<?= @array_keys(@$values[$id])[0] ?>">
                                <option value="">
                                        <?php echo Yii::t('app', 'app.list.empty'); ?>
                                    </option>
                                <?php foreach (@$values[$id] as $key => $val): ?>
                                    <?php if (!empty($key)): ?>
                                    <option
                                        value="<?php echo $key == '' ? '' : $val ?>"><?php echo $val; ?>
                                    </option>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <?php ?>
                <?php endforeach; ?>
                <!--
                <div class="filter_item">
                    <input type="checkbox" class="checkbox" id="checkbox1"/>
                    <label for="checkbox1"><?=Yii::t('app', 'discount.items')?></label>
                </div>
                -->
                <div class="filter_item">
                    <input type="checkbox" class="checkbox" id="checkbox2"/>
                    <label for="checkbox2"><?=Yii::t('app', 'finew')?></label>
                </div>

                <a href="#" class="reserfilter"><?=Yii::t('app', 'reset.filters')?></a>
            </div>
        </form>
        <div class="subscribe hidden1040">
            <?=Yii::t('app', 'learn.about.discounts')?>
            <span class="subscribe_text"></span>
            <form class="subscr_form">
                <input type="text" name="mail" placeholder="<?=Yii::t('app', 'footer.enter_email')?>">
                <input type="submit" value="OK">
            </form>
        </div>
        <div class="barinfo hidden1040">
            <?=Yii::t('app', 'if.you.have.questions')?>
            <a href="mailto:support@fif.by" class="lnk">support@fif.by</a><br>
        </div>
    </div>
    <div class="catalog">
        <?php $items = array_map("unserialize", array_unique(array_map("serialize", $items))); ?>
        <div class="catalog_panel">
            <div class="num"><?php echo count($items); ?> <?=Yii::t('app', 'товаров')?></div>
            <div class="catalog_loader" id="loader">
                <img src="/images/loader.svg" />
            </div>
        </div>
        <div class="catalog_error" style="display: none;">
            <p>Не найдено продукции</p>
            <p>соответствующей вашим критериям</p>
            <a class="reserfilter" href="#"><?=Yii::t('app', 'reset.filters')?></a>
        </div>
        <?php foreach ($items as $item): ?>
            <?php echo $this->render('item-new', ['item' => $item, 'prices' => $prices, 'itemsValues' => $itemsValues, 'breadCrumbs' => $breadCrumbs]) ?>
        <?php endforeach; ?>
        <div class="col-xs-12 padding-none">
          <?php echo $catInfo->fscatalog_bottom_text?>
        </div>
    </div>
</div>
