<?php

use yii\db\Schema;
use yii\db\Migration;

class m150319_184820_add_town extends Migration
{
    public function up()
    {
        $this->addColumn('distributors_point', 'fitown_id', 'int(10)');
        // $this->createIndex('distrib_point_town', 'distributors_point', 'fitown_id');
        $this->addForeignKey('fk_distrib_point_town', 'distributors_point', 'fitown_id', 'town', 'fitown_id');
        $this->alterColumn('distributors','fitown_id','int(10)');
        $this->dropForeignKey('fk_distrib_town', 'distributors');
        $this->dropIndex('distrib_town', 'distributors');
    }

    public function down()
    {
        $this->dropForeignKey('fk_distrib_point_town', 'distributors_point');
        //$this->dropIndex('distrib_point_town', 'distributors_point');
        $this->dropColumn('distributors_point', 'fitown_id');
    }
}
