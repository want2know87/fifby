<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "partners".
 *
 * @property integer $fipartner_id
 * @property string $fspartner_name
 * @property string $fspartner_image
 * @property string $fspartner_link
 */
class Partners extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'partners';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fspartner_name'], 'required'],
            [['fspartner_name', 'fspartner_image', 'fspartner_link'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fipartner_id' => Yii::t('app', 'Fipartner ID'),
            'fspartner_name' => Yii::t('app', 'Fspartner Name'),
            'fspartner_image' => Yii::t('app', 'Fspartner Image'),
            'fspartner_link' => Yii::t('app', 'Fspartner Link'),
        ];
    }
}
