<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Publications;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PublicationsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Обновить тип публикации: '.Publications::$types[$model->key];
$this->params['admin'] = [
    'page_title' => $this->title,
    'main_title' => 'Управление типами публикаций',
    'main_url' => 'publication-type/admin'
];
?>

<div class="publications-update">

    <div class="publications-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="form-group">
    <label class="control-label">Тип</label>
    <?= Html::textInput('type_name', Publications::$types[$model->key], ['class' => 'form-control', 'readonly' => true]) ?>
    </div>

    <div class="well"><span class="glyphicon glyphicon-info-sign"></span> Чем больше приоритет, тем выше элемент в списке.</div>
    <?= $form->field($model, 'position')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    </div>

</div>
