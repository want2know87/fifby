<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CatalogSearchRu */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="catalog-ru-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ficatalog_id') ?>

    <?= $form->field($model, 'fsname') ?>

    <?= $form->field($model, 'fiparent_catalog_id') ?>

    <?= $form->field($model, 'fscatalog_img') ?>

    <?= $form->field($model, 'fscatalog_text') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
