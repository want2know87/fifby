<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SourceMessage */

$this->title = Yii::t('app', 'Создать текст');
$this->params['admin'] = [
    'page_title' => $this->title,
    'main_title' => 'Управление текстами',
    'main_url' => 'source-message/create'
];
?>
<div class="source-message-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
