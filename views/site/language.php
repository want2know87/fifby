<?php

/* @var $this yii\web\View */

$class = str_replace('app\\models\\', '', $class);

$js = <<<JS

$(function() {
  var arr = $.parseJSON($('#translates').val());

  $('.container').on('click', '.flag-panel > div', function() {

    var prevId = $('input[name=language]').val();
    for(var i in arr.values[prevId]) {
      arr.values[prevId][i] = $('[name="%modelName%[' + i + ']"]').val();
    }

    $('.flag-panel > div.active').removeClass('active');
    $(this).addClass('active');
    var id = $(this).data('value');
    $('input[name=language]').val(id);

    for(i in arr.values[id]) {
      $('[name="%modelName%[' + i + ']"]').val(arr.values[id][i]).triggerHandler('change');
      /*if($('#cke_catalogru-fscatalog_text'))*/
    }

    if (typeof CKEDITOR !='undefined') {
      for(i in CKEDITOR.instances) {
        CKEDITOR.instances[i].updateElement();
      }
    }

    $('#translates').val($.toJSON(arr));

  });

$('form').on('submit', function() {
  $('#translates').val(JSON.stringify(arr));
});

});

JS;

$this->registerJs(str_replace('%modelName%', $class, $js));

?>

<textarea id="translates" name="translates" style="display: none;"><?php echo $config ?></textarea>

<input type="hidden" name="language" value="ru" />
<div class="col-xs-12 padding-none flag-panel">
  <div class="col-xs-1 padding-none active" data-value="ru">
    RUS<br/>
    <span class="flag ru"></span>
  </div>
  <div class="col-xs-1 padding-none" data-value="ge">
    GEO<br/>
    <span class="flag geo"></span>
  </div>
  <div class="col-xs-1 padding-none" data-value="en">
    ENG<br/>
    <span class="flag eng"></span>
  </div>
  <div class="col-xs-1 padding-none" data-value="ab">
    ABH<br/>
    <span class="flag abh"></span>
  </div>
</div>