<?php

namespace app\controllers;

use yii\web\UploadedFile;
use app\models\ItemsPictures;
use yii\filters\AccessControl;

class UploadController extends \yii\web\Controller
{

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'delete'],
                'rules' => [
                    [
                        'actions' => ['index', 'delete', 'image'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
      $this->enableCsrfValidation = false;
      $this->layout = 'admin';
      return parent::beforeAction($action);
      // $this->actionIndex();
    }

    public function actionImage()
    {
      if (\Yii::$app->request->isPost) {
        $name = time() . '_' .$_FILES["upload"]["name"];
        if (file_exists(dirname(__FILE__) . '/../web/image/art/' . $name)) {
          echo $_FILES["upload"]["name"] . " существует, необходимо переименовать файл. ";
        } else {
          move_uploaded_file($_FILES["upload"]["tmp_name"],
            dirname(__FILE__) . '/../web/image/art/' . $name);
          $funcNum = \Yii::$app->request->get('CKEditorFuncNum');
          echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($funcNum, '" . \Yii::$app->request->baseUrl . "/image/art/{$name}', 'Успешно загружено');</script>";
        }
      }
    }

    public function actionDelete($id,$item,$page) {
        \Yii::$app->db->createCommand('DELETE FROM items_pictures WHERE fiitem_pic_id = :picid', ['picid' => (int)$id])->execute();
        $this->redirect(['items/update', 'id' => (int)$item, 'page' => (int)$page]);
    }

    public function actionIndex()
    {

        if(\Yii::$app->request->isPost) {
            $file = UploadedFile::getInstancesByName('file');
            $file = current($file);
            $params = \Yii::$app->request->queryParams;
            $name = 'data/files/item_' . (int)$params['item'] . '_' . time() . '.' . $file->extension;
            $file->saveAs(dirname(__FILE__).'/../web/' . $name );
            $itemPic = new ItemsPictures();
            $itemPic->fiitem_id = (int)$params['item'];
            $itemPic->fsimage = $name;
            $itemPic->fsname = \Yii::$app->request->post('name');
            $itemPic->save();
            $this->redirect(['items/update', 'id' => (int)$params['item'], 'page' => (int)$params['page']]);
        }

        return $this->render('index');
    }

}
