<?php

use yii\db\Schema;
use yii\db\Migration;

class m150117_093919_nice_url extends Migration
{
  public function up()
  {
    $this->createTable('chpu', [
      'fsyii_link' => 'varchar(255) not null',
      'fsurl' => 'varchar(255) not null',
    ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');
    $this->createIndex('un_url', 'chpu', ['fsyii_link', 'fsurl'], true);
  }

  public function down()
  {
    $this->dropTable('chpu');
  }
}
