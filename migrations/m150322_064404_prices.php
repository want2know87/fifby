<?php

use yii\db\Schema;
use yii\db\Migration;

class m150322_064404_prices extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {

        $this->addColumn('items', 'fsarticle', 'varchar(30)');
        $this->createIndex('i_item_article', 'items', 'fsarticle');
        $this->addColumn('items', 'fsprice_rur', 'varchar(30)');
        $this->addColumn('items', 'fsprice_usd', 'varchar(30)');
        $this->addColumn('items', 'fsprice_byn', 'varchar(30)');

        $excel = PHPExcel_IOFactory::load(__DIR__ . DIRECTORY_SEPARATOR . 'price.xls');
        $articleCol = -1;
        $nameCol    = -1;
        $startRow   = -1;
        $priceRows = [
            'usd' => [
                'value' => '',
                'ind' => 'usd',
                'col' => -1,
                'search' => ['usd', '$'],
            ],
            'rur' => [
                'value' => '',
                'ind' => 'rur',
                'col' => -1,
                'search' => ['rur'],
            ],
            'byn' => [
                'value' => '',
                'ind' => 'byn',
                'col' => -1,
                'search' => ['byn'],
            ],
        ];
        foreach($excel->getWorksheetIterator() as $worksheet) {
            for($row = 0; $row < $worksheet->getHighestRow(); $row ++) {
                for($col = 0; $col < PHPExcel_Cell::columnIndexFromString($worksheet->getHighestColumn()); $col++) {
                    $cell = $worksheet->getCellByColumnAndRow($col, $row);
                    if('' == trim($cell->getValue())) {
                        continue;
                    }
                    if('Наименование' == $cell->getValue()) {
                        $startRow = $row + 1;
                        $nameCol  = $col;
                    }
                    if('Артикул' == $cell->getValue()) {
                        $articleCol = $col;
                    }
                    foreach($priceRows as $priceKey => $priceVal) {
                        foreach($priceVal['search'] as $val => $search) {
                            //echo "Compare ", mb_strtolower($cell->getValue()), ":", $search;
                            if(strpos(mb_strtolower($cell->getValue()), $search) !== false) {
                                //echo " finded";
                                $priceRows[$priceKey]['col'] = $col;
                                break;
                            }
                            //echo "\n";
                        }
                    }
                }
                if($articleCol >= 0 && $nameCol >= 0 && $startRow >=0) {
                    break;
                }
            }

            if($articleCol >= 0 && $nameCol >= 0 && $startRow >=0) {
                for($row = $startRow; $row < $worksheet->getHighestRow(); $row++) {
                    $prices = [];
                    foreach($priceRows as $priceRow) {
                        if($priceRow['col'] > 0) {
                            $prices['fsprice_' . $priceRow['ind']] = $worksheet->getCellByColumnAndRow($priceRow['col'], $row)->getValue();
                        }
                    }
                    $name = trim($worksheet->getCellByColumnAndRow($nameCol, $row)->getValue());
                    $article = trim($worksheet->getCellByColumnAndRow($articleCol, $row)->getValue());
                    if('' != trim($name)) {
                        $finded = $this->db->createCommand('SELECT * FROM items where fsitem_name = :name', ['name' => $name])->queryOne();
                        if(!empty($finded)) {
                            $strPrice = '';
                            foreach($prices as $key => $value) {
                                $strPrice .= ' ' . $key . ' = ' . '"' . $value . '", ';
                            }
                            $this->db->createCommand('UPDATE items SET ' . $strPrice . ' fsarticle = :article WHERE fiitem_id = :itemid', ['article' => $article, 'itemid' => $finded['fiitem_id']])->execute();
                        }
                        /*echo "Finded name: {$name}, article: {$article}, prices: ", json_encode($prices), "\n";*/
                    }

                }
            }

            break;
        }
        //var_dump($excel);
        //return false;
    }
    
    public function safeDown()
    {
        $this->dropIndex('i_item_article', 'items');
        $this->dropColumn('items', 'fsarticle');
        $this->dropColumn('items', 'fsprice_rur');
        $this->dropColumn('items', 'fsprice_usd');
        $this->dropColumn('items', 'fsprice_byn');
    }

}
