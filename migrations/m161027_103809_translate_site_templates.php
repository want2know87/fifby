<?php

use yii\db\Schema;
use yii\db\Migration;

class m161027_103809_translate_site_templates extends Migration
{
    public function up()
    {
        $this->addColumn('site_templates', 'fstitle_en', Schema::TYPE_STRING);
        $this->addColumn('site_templates', 'fstemplate_code_en', Schema::TYPE_TEXT);
    }

    public function down()
    {
        $this->dropColumn('site_templates', 'fstitle_en');
        $this->dropColumn('site_templates', 'fstemplate_code_en');
    }
}
