<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

$this->title = 'Технические характеристики';
$page = (int)Yii::$app->request->get('page');
$this->params['admin'] = [
    'page_title' => $this->title,
];
?>

<div class="items-index">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'fsparam_name',
            'fsparam_name_en',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}',
                'urlCreator' => function($action, $model, $key, $index) use($page) {
                    $params = is_array($key) ? $key : ['id' => (string) $key];
                    $params[0] = $action;
                    $params += ['page' => $page];

                    return Url::toRoute($params);
                },
            ],
        ],
    ]) ?>
</div>
