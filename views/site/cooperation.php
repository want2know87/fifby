<?php
use app\models\SiteTemplates;
use yii\bootstrap\Modal;

/** @var SiteTemplates $template */
$template = SiteTemplates::findOne(['fscode' => 'site/cooperation']);
$this->title = $template->fstitle;
$this->seoFields = $template->fstemplate_seo;
if (false) {
    eval("?>" . $template->fstemplate_code . "<?php ");
    return;
}
?>

<?php
use yii\helpers\Html;
use app\models\Sertificats;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var bool $result */

$this->title = Yii::t('app', 'cooperation.title');

$js = <<< JS

$(function() {

    $('.tovar_navtab span').click(function(){
      var a = $(this).attr('data-tab');
      $('.tovar_navtab span').removeClass('act');
      $('.tovar_navtab i').removeClass('left').removeClass('center').removeClass('right');
      $(this).toggleClass('act');
      if(a==0){ $('.tovar_navtab i').addClass('left');}
      if(a==1){ $('.tovar_navtab i').addClass('center');}
      if(a==2){ $('.tovar_navtab i').addClass('right');}
      $('#coop-form input[name=type]').val(a==0 ? 'diller' : 'installer');
      $('#coop-form .form-title span').removeClass('active');
      $( '#coop-form .form-title span.'+(a==0 ? 'diller' : 'installer') ).addClass('active');
      $('.tovar_tab.act').hide().removeClass('act');
      $('.tovar_tab').each(function(){
        if($(this).attr('data-tab')==a){
          $(this).slideToggle(600).addClass('act');
        }
      });
    });

    $('.select-type').on('click', '.coop-button', function() {
        $('.select-type span.active').removeClass('active');
        $(this).addClass('active');
        $('.form input[name=type]').val($(this).hasClass('diller') ? 'diller' : 'installer');
        $('.form p.form-title .active').removeClass('active');
        $('.form p.form-title').find('.' + $('.form input[name=type]').val()).addClass('active');
    });

    var coopFormSubmitted = false;
    $('#coop-form').on('submit', function(e) {
        e.preventDefault();
        if (!coopFormSubmitted) {
            $.post($(location).attr('href'), $('#coop-form').serializeArray())
                .success(function() {
                    coopFormSubmitted = true;
                    alert('Запрос отправлен, ожидайте с вами свяжуться сотрудники компании');
                    $('.modal').modal('hide');
                    location.reload();
                })
                .error(function() {
                    alert('Проверьте введенные данные');
                });
        }
    });

});

JS;

$this->registerJs($js);

?>

<!-- new -->
<div class="breadcrumbs">
    <div class="inner_width">
        <a href="/"><i class="ichome"></i>Главная</a>
        <span class="curr"><?php echo Yii::t('app', 'cooperation.title') ?></span>
    </div>
</div>
<div class="page_ttl hide">
    <div class="inner_width">
        <h1><?php echo Yii::t('app', 'cooperation.title') ?></h1>
    </div>
</div>
<div class="inner_width">
    <ul class="aboutlist">
        <li class="act"><a href="#"><?php echo Yii::t('app', 'cooperation.title') ?></a></li>
    </ul>
</div>
<div class="text cooperation-text-inner">
    <div class="inner_width">
        <div class="col-xs-12 title">
            <?php echo Yii::t('app', 'cooperation.text') ?>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="tovar_navtab coop-button-custom">
                  <i></i>
                  <span data-tab='0' class="act"><?php echo Yii::t('app', 'cooperation.diller') ?></span>
                  <span data-tab='2' class=''><?php echo Yii::t('app', 'cooperation.installer') ?></span>
                </div>
                <div class='tovar_tab act' data-tab='0'>
                    <div class="row">
                        <div class="col-xs-10 col-xs-offset-1">
                            <?php echo Yii::t('app', 'cooperation.diller.text') ?>
                        </div>
                    </div>
                </div>
                <div class='tovar_tab' data-tab='2'>
                    <div class="row">
                        <div class="col-xs-10 col-xs-offset-1">
                            <?php echo Yii::t('app', 'cooperation.installer.text') ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="text-align: center; margin-top: 25px;">
        <?php
            Modal::begin ( [
                'size' => Modal::SIZE_LARGE,
                'header' => '',
                'toggleButton' => [
                    'tag' => 'button',
                    'class' => 'btn submit coop-form-btn',
                    'label' => Yii::t('app', 'cooperation.ticket'),
                ]
            ] );

            //$form = ActiveForm::begin(['id' => 'coop-form', 'options' => ['class' => 'form-horizontal']]);
        ?>
            <form action="" method="post" id="coop-form" class="form-horizontal">
            <input type="hidden" name="type" value="diller" />

            <div class="row">
                <div class="col-xs-12">
                    <p class="form-title">
                        <span class="diller active"><?php echo Yii::t('app', 'cooperation.form.diller') ?></span>
                        <span class="installer"><?php echo Yii::t('app', 'cooperation.form.installer') ?></span>
                    </p>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label"><?php echo Yii::t('app', 'cooperation.form.name') ?></label>
                <div class="col-sm-8">
                    <input type="text" required class="form-control" placeholder="<?php echo Yii::t('app', 'cooperation.form.name') ?>" name="name">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label"><?php echo Yii::t('app', 'cooperation.form.country') ?></label>
                <div class="col-sm-8">
                    <input type="text" required class="form-control" placeholder="<?php echo Yii::t('app', 'cooperation.form.country') ?>" name="country">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label"><?php echo Yii::t('app', 'cooperation.form.phone') ?></label>
                <div class="col-sm-8">
                    <input type="text" required class="form-control" placeholder="<?php echo Yii::t('app', 'cooperation.form.phone') ?>" name="phone">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label"><?php echo Yii::t('app', 'cooperation.form.town') ?></label>
                <div class="col-sm-8">
                    <input type="text" required class="form-control" placeholder="<?php echo Yii::t('app', 'cooperation.form.town') ?>" name="town">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Email</label>
                <div class="col-sm-8">
                    <input type="email" required class="form-control" placeholder="Email" name="mail">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label"><?php echo Yii::t('app', 'cooperation.form.comment') ?></label>
                <div class="col-sm-8">
                    <textarea rows="3" name="comment" placeholder="<?php echo Yii::t('app', 'cooperation.form.comment') ?>" class="form-control"></textarea>
                </div>
            </div>

            <div class="form-group">
                <div class="center-block">
                    <button class="btn submit coop-form-btn"><?php echo Yii::t('app', 'cooperation.ticket') ?></button>
                </div>
            </div>
        </form>
        <?php //$form->end(); ?>
        <?php Modal::end();?>
        </div>
    </div>
</div>
<!-- /new -->

<?php if (false): ?>
<div class="distributors-index">
    <div class="padding-none col-xs-12 where-buy">
        <?php echo Yii::t('app', 'cooperation.title') ?>
    </div>
    <div class="col-xs-12 padding-none gray-buy">
        <?php echo Yii::t('app', 'cooperation.text') ?>
    </div>
    <br/>
    <div class="col-xs-12 padding-none cooperation center-block">
        <?php if($result): ?>
            <div class="col-xs-12 title" style="text-align: center;">
                <?php echo Yii::t('app', 'cooperation.success') ?>
            </div>
        <?php else: ?>
            <div class="col-xs-12 form">
                <div class="col-xs-12 title">
                    <?php echo Yii::t('app', 'cooperation.select.type') ?>
                </div>
                <div class="col-xs-12 select-type">
                    <div class="col-xs-6 padding-none"><span class="diller active coop-button"><?php echo Yii::t('app', 'cooperation.diller') ?></span></div>
                    <div class="col-xs-6 padding-none"><span class="installer coop-button"><?php echo Yii::t('app', 'cooperation.installer') ?></span></div>
                </div>
                <div class="col-xs-1"></div>
                <div class="col-xs-10 note packet">
                    <br/>
                    <span class="diller active"><?php echo Yii::t('app', 'cooperation.diller.text') ?></span>
                    <span class="installer"><?php echo Yii::t('app', 'cooperation.installer.text') ?></span>
                </div>
                <div class="col-xs-1"></div>

                <?php

                Modal::begin ( [
                    'header' => '',
                    'toggleButton' => [
                        'tag' => 'button',
                        'class' => 'btn submit',
                        'label' => Yii::t('app', 'cooperation.ticket'),
                    ]
                ] );

                $form = ActiveForm::begin([
                    'options' => ['class' => 'form-inline']
                ]) ?>
                <input type="hidden" name="type" value="diller" />
                <p class="form-title">
                    <span class="diller active"><?php echo Yii::t('app', 'cooperation.form.diller') ?></span>
                    <span class="installer"><?php echo Yii::t('app', 'cooperation.form.installer') ?></span>
                </p><br/>
                <div class="form-group">
                    <label class="col-sm-4 control-label padding-none centered"><?php echo Yii::t('app', 'cooperation.form.name') ?></label>
                    <div class="col-sm-8">
                        <input type="email" class="form-control" placeholder="<?php echo Yii::t('app', 'cooperation.form.name') ?>" name="name">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label padding-none centered"><?php echo Yii::t('app', 'cooperation.form.country') ?></label>
                    <div class="col-sm-8">
                        <input type="email" class="form-control" placeholder="<?php echo Yii::t('app', 'cooperation.form.country') ?>" name="country">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label padding-none"><?php echo Yii::t('app', 'cooperation.form.phone') ?></label>
                    <div class="col-sm-8">
                        <input type="email" class="form-control" placeholder="<?php echo Yii::t('app', 'cooperation.form.phone') ?>" name="phone">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label padding-none centered"><?php echo Yii::t('app', 'cooperation.form.town') ?></label>
                    <div class="col-sm-8">
                        <input type="email" class="form-control" placeholder="<?php echo Yii::t('app', 'cooperation.form.town') ?>" name="town">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label padding-none centered">Email</label>
                    <div class="col-sm-8">
                        <input type="email" class="form-control" placeholder="Email" name="mail">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label padding-none"><?php echo Yii::t('app', 'cooperation.form.comment') ?></label>
                    <div class="col-sm-8">
                        <textarea name="comment" placeholder="<?php echo Yii::t('app', 'cooperation.form.comment') ?>" class="form-control"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="center-block">
                        <button class="btn submit"><?php echo Yii::t('app', 'cooperation.ticket') ?></button>
                    </div>
                </div>

                <?php $form->end(); ?>


                <?php Modal::end();?>
            </div>
        <?php endif; ?>
    </div>
</div>
<?php endif; ?>
