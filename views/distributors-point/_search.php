<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DistributorsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="distributors-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'fidistr_id') ?>

    <?= $form->field($model, 'fitown_id') ?>

    <?= $form->field($model, 'ficountry_id') ?>

    <?= $form->field($model, 'fsname') ?>

    <?php // echo $form->field($model, 'fsaddress') ?>

    <?php // echo $form->field($model, 'maps') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
