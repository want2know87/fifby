﻿<?php
use yii\helpers\Html;
use app\models\Sertificats;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var bool $result */

$this->title = 'Сотрудничество';

$js = <<< JS

$(function() {

    $('.select-type').on('click', '.coop-button', function() {
        if($('form').is(':visible')) {
            $('form').slideUp();
        }
        $('.select-type span.active').removeClass('active');
        $(this).addClass('active');
        $('.form input[name=type]').val($(this).hasClass('diller') ? 'diller' : 'installer');
        $('.form .form-title span:visible').hide();
        $('.form .form-title span.' + ($(this).hasClass('diller') ? 'diller' : 'installer')).show();
        $('form').slideDown();
    });

});

JS;

$this->registerJs($js);

?>

<div class="distributors-index">
    <div class="padding-none col-xs-12 where-buy">
        Сотрудничество
    </div>
    <div class="col-xs-12 padding-none gray-buy">
        Компания «Евроавтоматика ФиФ производит широкий ассортимент релейной защиты и автоматики и реализует его через дилерскую сеть. Мы приглашаем к сотрудничеству торговые и инжиниринговые компании.
    </div>
    <br/>
    <div class="col-xs-12 padding-none cooperation center-block">
        <div class="col-xs-1"></div>
        <?php if($result): ?>
            <div class="col-xs-10 title" style="text-align: center;">
                Заявка сформирована и отправлена на рассмотрение.
            </div>
        <?php else: ?>
        <div class="col-xs-10 form">
            <div class="col-xs-12 title">
                Выберите интересующий Вас тип сотрудничества и заполните заявку:
            </div>
            <div class="col-xs-12 select-type">
                <span class="diller coop-button">Дилер, дистрибьютор компании Евроавтоматика ФиФ</span>
                или
                <span class="installer coop-button">Авторизованный инсталлятор систем <a href="http://fhome.by/" target="_blank">F&Home</a></span>
            </div>
            <div class="col-xs-12 note packet">
                <br/>Продажа продукции торговой марки «F&F» Условия сотрудничества можно обсудить с заместителем начальника отдела продаж компании после заполнения Заявки
            </div>
            <?php $form = ActiveForm::begin([
                'options' => [
                    'style' => 'display:none;',
                ]
            ]) ?>
            <input type="hidden" name="type" value="diller" />
            <div class="col-xs-12 form-title">
                <span class="diller">Заявка на дилера</span>
                <span class="installer" style="display: none;">Заявка на инсталлятора</span>
            </div>
            <div class="col-xs-6">
                <input type="text" class="form-control" placeholder="Ваше имя*" name="name">
            </div>
            <div class="col-xs-6">
                <input type="text" class="form-control"  placeholder="Телефон с кодом страны" name="phone">
            </div>
            <div class="col-xs-6">
                <input type="text"  class="form-control"  placeholder="Страна" name="country">
            </div>
            <div class="col-xs-6">
                <input type="text"  class="form-control"  placeholder="Город" name="town">
            </div>
            <div class="col-xs-12">
                <input type="text" class="form-control" placeholder="Email" name="mail">
            </div>
            <div class="col-xs-12">
                <textarea name="comment" class="form-control" placeholder="Комментарий, если необходимо"></textarea>
            </div>
            <div class="col-xs-12">
                <button class="btn submit">Отправить заявку</button>
            </div>
            <?php $form->end(); ?>
        </div>
        <?php endif; ?>
        <div class="col-xs-1"></div>
    </div>
</div>