<?php

use yii\db\Schema;
use yii\db\Migration;

class m150310_185609_replace_desc extends Migration
{
    public function up()
    {
        $info = $this->db->createCommand('select items.fiitem_id, items.fsitem_small_desc from items')->queryAll();
        foreach($info as $inf) {
            $id = $inf['fiitem_id'];
            $desc = strip_tags($inf['fsitem_small_desc']);
            Yii::$app->db->createCommand('update items set fsitem_small_desc = :desc where fiitem_id = :itemid', ['desc' => $desc, 'itemid' => $id])->execute();
        }
    }

    public function down()
    {
        echo "m150310_185609_replace_desc cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
