<div class="page_ttl hide">
    <div class="inner_width">
        <h1>ЕВРОАВТОМАТИКА</h1>
    </div>
</div>

<?php
$catalog = \app\models\CatalogRu::find()
    ->where('fiparent_catalog_id is NULL')
    ->orderBy('fiorder ASC')
    ->all();
?>

<div class="owl-carousel">
    <div class="item">
        <img src="images/bann2.jpg" alt=""/>
        <div class="carousel_item">
            <div class="inner_width">
                <h3>Производим электротехническую продукцию более 10 лет</h3>
                <a href="#" class="catalog_item_more big">Узнать подробнее</a>
            </div>
        </div>
    </div>
    <div class="item">
        <img src="images/bann2.jpg" alt=""/>
        <div class="carousel_item">
            <div class="inner_width">
                <h3>Производим электротехническую продукцию более 10 лет2222</h3>
                <a href="#" class="catalog_item_more big">Узнать подробнее</a>
            </div>
        </div>
    </div>
    <div class="item">
        <img src="images/bann2.jpg" alt=""/>
        <div class="carousel_item">
            <div class="inner_width">
                <h3>Производим электротехническую продукцию более 10 33333</h3>
                <a href="#" class="catalog_item_more big">Узнать подробнее</a>
            </div>
        </div>
    </div>
</div>
<div class="inner_width ">
    <div class="servicettl line"><h5>Наши направления деятельности</h5></div>
    <?php foreach ($catalog as $cat): ?>
        <div class="service_item">
            <div class="service_item_pic">
                <?php if(strlen($cat['fscatalog_img'])): ?>
                    <?php echo \yii\helpers\Html::img(Yii::$app->request->getBaseUrl() . '/' . $cat['fscatalog_img'], ['style' => 'max-width:160px;max-height:165px;']) ?>
                <?php endif; ?>
            </div>
            <div class="service_item_inf">
                <a class="catalogcol_item_ttl" href="<?php echo \yii\helpers\Url::to(['catalog/index', 'cat' => $cat['ficatalog_id']]) ?>"><?php echo htmlspecialchars($cat['fsname']) ?> <!--<img src="images/lg1.png" alt="" />--></a>
                <p>Самый широкий ассортимент релейной автоматики в СНГ. Более 400 наименований по 30 функциональным группам продуктов бытового и промышленного назначения. От классических фотореле до программируемых блоков АВР и ограничителей мощности.</p>
                <?php
                    $subcatalog = \app\models\CatalogRu::find()
                    ->where('fiparent_catalog_id = ' . (int) $cat['ficatalog_id'])
                    ->orderBy('fiorder ASC')
                    ->all();
                ?>
                <?php foreach ($subcatalog as $sub): ?>
                    <a href="<?php echo \yii\helpers\Url::to(['catalog/index', 'cat' => $sub['ficatalog_id']]) ?>" class="lnk"><?php echo htmlspecialchars($sub['fsname']) ?></a>
                <?php endforeach; ?>
            </div>
            <div class="service_item_r">
                <a class="catalog_item_more big gr" href="rza.html">Узнать подробнее</a>
            </div>
        </div>
    <?php endforeach; ?>
</div>
<div class="choose">
    <div class="inner_width center">
        <div class="servicettl line"><h5>Подберите продукцию по функционалу</h5></div>
        <ul class="choosetabs">
            <li data-num='0' class="act"><span>1</span> Выберите интересующую вас группу</li>
            <li data-num='1' ><span>2</span> Выберите нужную характеристику</li>
            <li data-num='2'><span>3</span> Ваш выбор</li>
        </ul>
    </div>
    <div class="choosewrap">
        <div class="inner_width ">
            <form class="choose_form">
                <ul class="choose_form_tab show" data-tab='0'>
                    <li>
                        <input id="c1" type="checkbox" name=""  hidden />
                        <label for="c1">Управление освещением</label>
                    </li>
                    <li>
                        <input id="c2" type="checkbox" name="" hidden />
                        <label for="c2">Защита электродвигателей и потребителей</label>
                    </li>
                    <li>
                        <input id="c3" type="checkbox" name="" hidden  />
                        <label for="c3">Защита электродвигателей и потребителей</label>
                    </li>
                    <li>
                        <input id="c4" type="checkbox" name="" hidden   />
                        <label for="c4"> Защита электродвигателей и потребителей</label>
                    </li>
                    <li>
                        <input id="c5" type="checkbox" name=""  hidden />
                        <label for="c5">Управление освещением</label>
                    </li>
                    <li>
                        <input id="c6" type="checkbox" name="" hidden />
                        <label for="c6">Защита электродвигателей и потребителей</label>
                    </li>
                    <li>
                        <input id="c7" type="checkbox" name="" hidden  />
                        <label for="c7">Защита электродвигателей и потребителей</label>
                    </li>
                    <li>
                        <input id="c8" type="checkbox" name="" hidden   />
                        <label for="c8"> Защита электродвигателей и потребителей</label>
                    </li>
                </ul>
                <ul class="choose_form_tab " data-tab='1'>
                    <li>
                        <input id="c9" type="checkbox" name="" hidden />
                        <label for="c9">Защита электродвигателей и потребителей</label>
                    </li>
                    <li>
                        <input id="c10" type="checkbox" name="" hidden  />
                        <label for="c10">Защита электродвигателей и потребителей</label>
                    </li>
                    <li>
                        <input id="c11" type="checkbox" name="" hidden   />
                        <label for="c11"> Защита электродвигателей и потребителей</label>
                    </li>
                </ul>
                <ul class="choose_form_tab" data-tab='2'>
                    <li>
                        <input id="c12" type="checkbox" name="" hidden />
                        <label for="c12">Защита электродвигателей и потребителей</label>
                    </li>
                    <li>
                        <input id="c13" type="checkbox" name="" hidden  />
                        <label for="c13">Защита электродвигателей и потребителей</label>
                    </li>
                </ul>
            </form>
        </div>
    </div>
    <div class="inner_width center">
        <div class="servicettl line"> <h5>Компания Eвроавтоматика сегодня</h5></div>
        <ul class="listnow">
            <li>13 лет на рынке</li>
            <li>ПОСТАВКИ НА ВЕСЬ РЫНОК СНГ</li>
            <li>СВЫШЕ 400 изделий</li>
            <li>ОБОРОТЫ ПРОДАЖ больше 1 млн</li>
        </ul>
        <p class="pnow">Мы - производственная компания СООО «Евроавтоматика ФиФ». Обеспечиваем средствами релейной защиты и автоматики рынки стран СНГ уже на протяжении 13 лет. В свою очередь история берёт с 1992 года, когда отец с сыном решили создать на базе торгово-коммерческой организации фирму «F&F»- на сегодняшний день занимает лидирующие позиции в странах ЕС и США по обеспечению промышленной и бытовой автоматикой. С первых дней основания мы стараемся думать о завтрашнем дне. Наш основной приоритет — внимание к тенденциям в области энергетики и бережное отношение к клиенту.</p>
        <div class="now_item">
            <div class="now_item_ttl">Высокое качество изделий</div>
            <p>Собственное производство, многоступенчатый технологический контроль, использование комплектующих ведущих мировых производителей (Omron, Schrack, Atmel и др.), профессиональная сертификация производства и продукции гарантируют высокое качество изделий;</p>
        </div>
        <div class="now_item mod1">
            <div class="now_item_ttl">Инновационные разработки</div>
            <p>Постоянно обновляемая линейка продуктов, профессиональный подход к проектированию (импульсные блоки питания, гальваническая развязка питания датчиков и др.), инновационные разработки обеспечивают компании лидирующие позиции на рынке СНГ;</p>
        </div>
        <div class="now_item mod2">
            <div class="now_item_ttl">Надежный партнер</div>
            <p>Комплекс маркетинговых услуг, гибкая ценовая политика, экспертная техническая поддержка, гарантийное и послегарантийное обслуживание обеспечивают комфортное сотрудничество для партнёров и потребителей.</p>
        </div>
        <a href="#" class="catalog_item_more modm">Подробнее</a>
    </div>
</div>