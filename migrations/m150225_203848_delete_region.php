<?php

use yii\db\Schema;
use yii\db\Migration;

class m150225_203848_delete_region extends Migration
{
    public function safeUp()
    {

        $this->dropForeignKey('fk_region_country','region');
        $this->dropForeignKey('fk_distrib_region','distributors');
        $this->dropForeignKey('fk_town_region','town');

        //$this->dropIndex('u_town_name', 'town');
        //$this->createIndex('u_town_name_n', 'town', ['ficountry_id','fstown_name'], true);
        //$this->dropIndex('dest_id','distributors');
        $this->dropTable('region');
        $this->dropColumn('distributors', 'firegion_id');
        $this->dropColumn('town', 'firegion_id');
    }

    public function down()
    {
        echo "m150225_203848_delete_region cannot be reverted.\n";

        return false;
    }
}
