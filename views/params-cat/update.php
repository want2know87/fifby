<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Items */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Техническую характеристику',
]) . ' ' . $model->fsparam_name;

$this->params['admin'] = [
    'page_title' => $this->title,
    'main_title' => 'Технические характеристики',
    'main_url' => 'param/admin'
];
?>
<div class="items-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
