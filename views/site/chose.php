<?php

use app\models\Selects;
use app\models\SelectsItems;

$selectsGeneral = Selects::find()->where('fiparent_id = 0 or fiparent_id is null')->all();
$selectsOther = Selects::find()->where('fiparent_id <> 0 AND fiparent_id is not null')->all();

$js = <<<EOF
    $(function () {
       window.setTimeout(function () {
           $('.choose_form').unbind('change');
           
           $('.choose_form li label').click(function() {
               $('#loader').show(0);
               var self = $(this);
               var index = $('.choosetabs li.act').attr('data-num');
               $('.choose_form_tab').removeClass('show');
               $('.choosetabs li').removeClass('act');
               if(index == 0) {
                    //var active = $('input[name=general]:checked').attr('value');
                    var active = self.closest('li').find('input').attr('value');
                    $('.choose_form_tab[data-tab="1"] li').hide();
                    $('.choose_form_tab[data-tab="1"] li[data-parent=' + active + ']').show();
                    $('.choose_form_tab[data-tab="1"]').addClass("show");
                    $('.choosetabs li[data-num="1"]').addClass('act');
                    $('#loader').hide(0);
               }
               if(index == 1) {
                    //var active = $('input[name=podcat]:checked').attr('value');
                    var active = self.closest('li').find('input').attr('value');
                    $.post('%url%', {cat : active})
                        .always(function() {
                            $('.choose_form_tab[data-tab="2"]').addClass("show");
                            $('.choosetabs li[data-num="2"]').addClass('act');
                            $('#loader').hide(0);
                        })
                        .done(function(data) {
                           $('.choose_form_tab[data-tab="2"]').html(data);
                        });
               }
           });
           
           
       });
       
    });
EOF;

$js = str_replace('%url%', \yii\helpers\Url::to(['site/getcats']), $js);

$this->registerJs($js);

?>

<div class="choosewrap">
    <div class="inner_width ">
        <div class="general_loader" id="loader">
            <img src="/images/loader.svg" />
        </div>
        <form class="choose_form">
            <ul class="choose_form_tab show" data-tab='0'>
                <?php foreach ($selectsGeneral as $sg): ?>
                    <li>
                        <input id="c<?php echo $sg->fiselect_id ?>" type="radio" value="<?php echo $sg->fiselect_id ?>"
                               name="general" hidden/>
                        <label for="c<?php echo $sg->fiselect_id ?>"><?php echo $sg->fsselect_name; ?></label>
                    </li>
                <?php endforeach; ?>
            </ul>
            <ul class="choose_form_tab " data-tab='1'>
                <?php foreach ($selectsOther as $sg): ?>
                    <li data-parent="<?php echo $sg->fiparent_id ?>">
                        <input id="c<?php echo $sg->fiselect_id ?>" type="radio" value="<?php echo $sg->fiselect_id ?>"
                               name="podcat" hidden/>
                        <label for="c<?php echo $sg->fiselect_id ?>"><?php echo $sg->fsselect_name; ?></label>
                    </li>
                <?php endforeach; ?>
            </ul>
            <ul class="choose_form_tab last" data-tab='2'>

            </ul>
        </form>
    </div>
</div>