<?php

namespace app\models;

use Yii;
use app\models\traits\MultiLanguageTrait;

/**
 * This is the model class for table "params_cat".
 *
 * @property string $fiparam_id
 * @property string $fsparam_name
 * @property integer $fiparam_show_filter
 * @property integer $fiparam_order
 *
 * @property ParamsValues[] $paramsValues
 */
class ParamsCat extends \yii\db\ActiveRecord
{
    use MultiLanguageTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'params_cat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fiparam_show_filter', 'fiparam_order'], 'integer'],
            [['fsparam_name'], 'string', 'max' => 255],

            ['fsparam_name_en', 'trim'],
            ['fsparam_name_en', 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fiparam_id' => Yii::t('app', 'Внутренний номер параметра продукта'),
            'fsparam_name' => Yii::t('app', 'Название параметра продукта'),
            'fsparam_name_en' => Yii::t('app', 'Название параметра продукта') . ' En',
            'fiparam_show_filter' => Yii::t('app', 'Отображать ли в фильтрах'),
            'fiparam_order' => Yii::t('app', 'Приоритет сортировки'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParamsValues()
    {
        return $this->hasMany(ParamsValues::className(), ['fiparam_id' => 'fiparam_id']);
    }
}
