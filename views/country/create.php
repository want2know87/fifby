<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Country */

$this->params['admin'] = [
    'page_title' => $this->title,
    'main_title' => 'Создать страну',
    'main_url' => 'country/create'
];
?>
<div class="country-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
