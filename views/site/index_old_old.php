<?php
use app\components\SiteView;
use app\models\CatalogRu;
/* @var $this SiteView */
$this->setShowBanner(true);
$this->title = 'Евроавтоматика';

$catsSearch = Yii::$app->db->createCommand("SELECT
                                                *
                                            FROM
                                                catalog_ru
                                            WHERE
                                                fiparent_catalog_id IS NULL
                                                AND fivisible = 1
                                            OR fiparent_catalog_id = ''
                                            OR fiparent_catalog_id IN(
                                                SELECT
                                                    ficatalog_id
                                                FROM
                                                    catalog_ru
                                                WHERE
                                                    fiparent_catalog_id IS NULL
                                                OR fiparent_catalog_id = ''
                                            )
                                            ORDER BY fiparent_catalog_id is NULL desc, fiorder")
    ->queryAll();

$cats = [];
$catsLeft = [];
foreach($catsSearch as $catalog) {
    $translates = json_decode($catalog['translates'], true);
    $cats[$catalog['ficatalog_id']]['name'] = $translates['values'][Yii::$app->language]['fsname'];
    $cats[$catalog['ficatalog_id']]['id'] = $catalog['ficatalog_id'];
    $cats[$catalog['ficatalog_id']]['level'] = $catalog['fiparent_catalog_id'] ? 2 : 1;
    $cats[$catalog['ficatalog_id']]['text'] = $translates['values'][Yii::$app->language]['fscatalog_text'];
    $cats[$catalog['ficatalog_id']]['img'] = $catalog['fscatalog_img'];
    $cats[$catalog['ficatalog_id']]['show_text'] = $catalog['fishow_general_desc'];

    $parentEl = $catalog['fiparent_catalog_id'] ? $catalog['fiparent_catalog_id'] : 0;

    $el = [
        'name' => $translates['values'][Yii::$app->language]['fsname'],
        'id' => $catalog['ficatalog_id'],
        'level' => $catalog['fiparent_catalog_id'] ? 2 : 1,
        'text' => $translates['values'][Yii::$app->language]['fscatalog_text'],
        'show_text' => $catalog['fishow_general_desc'],
    ];

    $catsLeft[$parentEl][] = $el;
}

$catsSearch = CatalogRu::find()->where(['fivisible' => 1])->orderBy('fiorder')->all();
foreach($cats as &$catalog) {
    $catalog['items'] = [];
    /** @var CatalogRu $cat */
    foreach($catsSearch as $cat) {
        $translates = json_decode($cat->translates, true);
        if($cat->fiparent_catalog_id == $catalog['id']) {
            $catalog['items'][] = [
                'id' => $cat->ficatalog_id,
                'name' => $translates['values'][Yii::$app->language]['fsname'],
                'text' => $translates['values'][Yii::$app->language]['fscatalog_text'],
                'show_text' => $cat->fishow_general_desc,
            ];
        }
    }
}

$js = <<< JS
$(function() {
    $(".general-cats").on('mousemove mouseover', '> div', function () {
        $(".general-cats").find('> .active').removeClass('active');
        $(this).addClass('active');
        $('.general-elements > div').each(function() {
            $(this).hide();
        });
        $('.general-elements > div[data-parent-id=' + $(this).data('id') + ']').show();
    });
    $('.general-cats > div:eq(1)').trigger('mouseover');
});
JS;

$this->registerJs($js);

?>
<div class="site-index">
    <br/>
    <div class="col-xs-12 padding-none">
        <h1 class="general-title"><?php echo Yii::t('app', 'catalog'); ?></h1>
    </div>
    <div class="col-xs-12 padding-none general-catalog">
        <div class="col-xs-4 general-cats padding-none">
        <?php $b = true; foreach($cats as $index => $element): ?>
            <?php if((int)$element['level'] >= 2) continue; ?>
            <div class="col-xs-12 padding-none <?php if($b) { $b = false; echo "active"; } ?> cat-level-<?php echo (int)$element['level'] ?>" data-id="<?php echo $element['id'] ?>">
                <?php /*echo htmlspecialchars($element['name']) */?>
                <?php echo \yii\helpers\Html::a(htmlspecialchars($element['name']), ['catalog/index','cat' => $element['id']]) ?>
            </div>
            <?php if(empty($catsLeft[(int)$element['id']])) continue; ?>
            <?php foreach($catsLeft[(int)$element['id']] as $ind => $el): ?>
                <div class="col-xs-12 padding-none cat-level-<?php echo (int)$el['level'] ?>" data-id="<?php echo $el['id'] ?>">
                    <?php /*echo htmlspecialchars($el['name']) */?>
                    <?php echo \yii\helpers\Html::a(htmlspecialchars(htmlspecialchars($el['name'])), ['catalog/index','cat' => $el['id']]) ?>
                </div>
            <?php endforeach; ?>
        <?php endforeach; ?>
        <?php /*$b = true; foreach($cats as $index => $element): */?><!--
            <div class="col-xs-12 padding-none <?php /*if($b) { $b = false; echo "active"; } */?> cat-level-<?php /*echo (int)$element['level'] */?>" data-id="<?php /*echo $element['id'] */?>">
                <?php /*echo htmlspecialchars($element['name']) */?>
            </div>
        --><?php /*endforeach; */?>
        </div>
        <div class="col-xs-8 general-elements">
        <?php $b = true; foreach($cats as $index => $element): ?>
            <div class="col-xs-12" data-parent-id="<?php echo $element['id'] ?>" <?php if(!$b) {echo "style=\"display: none;\"";} else {$b = false;} ?>>
                <div class="col-xs-12 padding-none text-name">
                    <?php echo $element['name']; ?>
                </div>
                <?php if(strlen($element['text']) && $element['show_text']): ?>
                <div class="col-xs-12 padding-none text-desc">
                    <?php echo $element['text'] ?>
                </div>
                <?php endif; ?>
                <?php if(empty($element['items'])): ?>
                    <div class="col-xs-12 padding-none text-desc">
                        <?php echo \yii\helpers\Html::a('Перейти в раздел &rarr;', ['catalog/index','cat' => $element['id']]) ?>
                    </div>
                <?php endif; ?>
                <div class="col-xs-8 element padding-none" data-parent-id="<?php echo $element['id'] ?>">
                    <?php $ind = 1; ?>
                    <?php if($element['level'] == 2): ?>
                        <?php foreach($element['items'] as $item): ?>
                            <div class="col-xs-12 padding-none element" data-id="<?php echo $item['id'] ?>" data-parent-id="<?php echo $element['id'] ?>">
                                <div class="col-xs-12 padding-none item-title">
                                    <div class="romb"><?php echo $ind++ ?></div>
                                    <div class="name"><a href="<?php echo \yii\helpers\Url::to(['catalog/index','cat' => (int)$item['id'] ]) ?>"><?php echo htmlspecialchars($item['name']) ?></a></div>
                                </div>
                                <?php if(strlen($item['text']) && $item['show_text']): ?>
                                <div class="col-xs-12 padding-none item-text">
                                    <?php echo $item['text'] ?>
                                </div>
                                <?php endif; ?>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
                <div class="col-xs-4 element image" data-parent-id="<?php echo $element['id'] ?>">
                    <?php if(strlen($element['img'])): ?>
                        <?php echo \yii\helpers\Html::img(Yii::$app->request->getBaseUrl() . '/' . $element['img']) ?>
                    <?php endif; ?>
                </div>
            </div>
        <?php endforeach; ?>
        </div>
    </div>
</div>
