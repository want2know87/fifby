<?php
use yii\helpers\Html;
use app\models\Sertificats;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Tabs;
use yii\data\ActiveDataProvider;
use app\models\PublicationFiles;
use yii\widgets\ListView;
use app\models\Publications;

/* @var $this yii\web\View */
/* @var bool $result */

$this->title = Yii::t('app', 'down.title');

$js = <<< JS

$(function() {

});

JS;

$this->registerJs($js);

?>

<?php

use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PublicationsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Publications');
?>

<div class="breadcrumbs">
    <div class="inner_width">
        <a href="<?php echo \yii\helpers\Url::to(['site/index']); ?>"><i class="ichome"></i>Главная</a>
        <span class="curr">Медиа</span>
    </div>
</div>
<div class="page_ttl">
    <div class="inner_width">
        <h1>Медиа</h1>
    </div>
</div>
<div class="inner_width clrmar mediacenter">
    <?php foreach (\app\models\Publications::$types as $index => $type): ?>
        <div class="blocklink media">
            <a href="/downloads?type=<?php echo $index; ?>">
                <div class="media_item ">
                    <img src="images/icons/media/i<?php echo $index+2; ?>.png" alt=""/>
                    <span><?php echo $type ?></span>
                </div>
            </a>
        </div>
    <?php endforeach; ?>
</div>
<div class="inner_width ">
    <div class="description">
        <div class="separator"></div>
        Тут должен быть сео текст для продвижения, если оне влезет в верху. Возможно изготовление (под заказ) автоматов на напряжение 24 В AC/DC, а также с другим диапазоном регулировки порога включения по освещённости или временными Монтаж
        Реле установить в месте, открытом для доступа дневного света, при изменении которого будет включаться/отключаться освещение. При монтаже необходимо учесть, чтобы свет от включаемого освещения не попадал на фотодатчик. Регулировку порога включения необходимо производить медленно и аккуратно.
    </div>
</div>
