<?php

use yii\db\Schema;
use yii\db\Migration;
use app\models\Distributors;
use app\models\DistributorsMail;
use app\models\DistributorsPhones;
use app\models\DistributorsPointMail;
use app\models\DistributorsPointPhones;

class m150315_160649_migrate_distrib extends Migration
{
    public function up()
    {
        $distribs = \app\models\Distributors::find()->groupBy('fsname')->all();
        foreach($distribs as $distr) {
            /** @var Distributors $distr */
            /** @var Distributors $points */
            $points = \app\models\Distributors::find()->where(['fsname' => $distr->fsname])->orderBy('fidistr_id')->all();
            $first = true;
            $firstPoint = false;
            foreach($points as $point) {
                /** @var Distributors $point */
                if($first) {
                    $firstPoint = $point;
                    $first = false;
                }
                $disribPoint = new \app\models\DistributorsPoint();
                $disribPoint->fidistr_id = $firstPoint->fidistr_id;
                $disribPoint->fsaddress = $point->fsaddress;
                $disribPoint->fssite = $point->fssite;
                $disribPoint->maps = $point->maps;
                $disribPoint->save();
                /** @var DistributorsPhones $phones */
                $phones = DistributorsPhones::find()->where(['fidistr_id' => $point->fidistr_id])->all();
                foreach($phones as $phone) {
                    /** @var DistributorsPhones $phone */
                    $pointPhone = new DistributorsPointPhones();
                    $pointPhone->fsphone = $phone->fsphone;
                    $pointPhone->fidistr_point_id = $disribPoint->fidistr_point_id;
                    $pointPhone->save();
                    $phone->delete();
                }
                /** @var DistributorsMail $phones */
                $mails = DistributorsMail::find()->where(['fidistr_id' => $point->fidistr_id])->all();
                foreach($mails as $mail) {
                    /** @var DistributorsMail $mail */
                    $pointMail = new DistributorsPointMail();
                    $pointMail->fsmail = $mail->fsmail;
                    $pointMail->fidistr_point_id = $disribPoint->fidistr_point_id;
                    $pointMail->save();
                    $mail->delete();
                }
                if($firstPoint != $point) {
                    $point->delete();
                }
            }
        }
        $this->createIndex('un_distrib_name', 'distributors', 'fsname', true);
        $this->addForeignKey('fk_distrib_point', 'distributors_point', 'fidistr_id', 'distributors', 'fidistr_id');
    }

    public function down()
    {
        foreach([
                    'distributors_point_phones',
                    'distributors_point_mail',
                    'distributors_point',
                ] as $tName) {
            $this->truncateTable($tName);
        }
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
