<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sales".
 *
 * @property string $fisale_id
 * @property string $fssale_name
 * @property string $fiprice_percent
 *
 * @property Items[] $items
 */
class Sales extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sales';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fssale_name', 'fiprice_percent'], 'required'],
            [['fiprice_percent'], 'integer'],
            [['fssale_name'], 'string', 'max' => 255],
            [['fssale_name'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fisale_id' => Yii::t('app', 'Fisale ID'),
            'fssale_name' => Yii::t('app', 'Fssale Name'),
            'fiprice_percent' => Yii::t('app', 'Fiprice Percent'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(Items::className(), ['fisale_id' => 'fisale_id']);
    }
}
