<?php
use yii\helpers\Html;
use app\models\Sertificats;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Tabs;
use yii\data\ActiveDataProvider;
use app\models\PublicationFiles;
use yii\widgets\ListView;
use app\models\Publications;

/* @var $this yii\web\View */
/* @var bool $result */

$this->title = Yii::t('app', 'down.title');
/* @var $this yii\web\View */
?>

<div class="breadcrumbs">
    <div class="inner_width">
        <a href="<?php echo \yii\helpers\Url::to(['site/index']); ?>"><i class="ichome"></i><?= Yii::t('app', 'navigation.general') ?></a>
        <a href="<?php echo \yii\helpers\Url::to(['site/downloads']) ?>"><?= Yii::t('app', 'navigation.media') ?></a>
        <span class="curr"><?php echo Yii::t('app', Publications::$types[$type]) ?></span>
    </div>
</div>
<div class="page_ttl">
    <div class="inner_width">
        <h1><?php echo Yii::t('app', Publications::$types[$type]) ?></h1>
    </div>
</div>

<div class="inner_width clrmar">
<?php
    $fileQuery = PublicationFiles::find()
        ->joinWith(['fipublic'])
        ->andWhere(['publications.fitype' => (int) $type])
        ->andWhere(['publications.lang' => Yii::$app->language])
        ->orderBy('publication_files.priority DESC, publication_files.fsname ASC')
    ;
    $files = $fileQuery->all();
    foreach ($files as $file): ?>
    <?php echo \yii\helpers\Html::a('<span>' . $file['fsname'] . '</span>' .
        (file_exists(Yii::$app->basePath . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . $file['fsfile']) ? ' (' .
        round(@filesize(Yii::$app->basePath . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . $file['fsfile'])/1024, 0) . ' Kb)' : '') , \yii\helpers\Url::to(['publication-files/get-item', 'id' => $file['fipublic_file_id']]), ['target' => '_blank', 'class' => 'pricelist_item type'.$type]) ?>
<?php endforeach; ?>
</div>
