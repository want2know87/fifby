<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="items-form">

    <?php $form = ActiveForm::begin(); ?>

    <input type="hidden" name="page" value="<?php echo Yii::$app->request->get('page'); ?>" />

    <?= $form->field($model, 'fsparam_name')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'fsparam_name_en')->textInput(['maxlength' => 255]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
