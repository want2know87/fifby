<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\News */

$this->title = Yii::t('app', 'Create News');

$this->params['admin'] = [
    'page_title' => $this->title,
    'main_title' => 'Новости',
    'main_url' => 'news/admin'
];
?>
<div class="news-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
