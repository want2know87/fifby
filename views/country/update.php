<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Country */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app','Country'),
]) . ' ' . $model->fscountry_name;
$this->params['admin'] = [
    'page_title' => $this->title,
    'main_title' => 'Обновить страну',
    'main_url' => 'country/update'
];
?>
<div class="country-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
