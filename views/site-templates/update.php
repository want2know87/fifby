<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SiteTemplates */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Site Templates'),
]) . ' ' . $model->fstitle;
$this->params['admin'] = [
    'page_title' => $this->title,
    'main_title' => 'Шаблоны сайта',
    'main_url' => 'site-templates/admin'
];
?>
<div class="site-templates-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
