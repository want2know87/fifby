<?php

use yii\helpers\Html;
use app\models\CatalogRu;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CatalogSearchRu */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $subcat int */

/** @var CatalogRu $catInfo */
$catInfo = CatalogRu::find()->where(['ficatalog_id' => $subcat])->andWhere(['fivisible' => 1])->one();

$translates = json_decode($catInfo->translates, true);

$this->title = $translates['values'][Yii::$app->language]['fsname'];

// print_r($items);

$js = <<< JS

$(function() {

});

JS;

$js = str_replace('%URL%', \yii\helpers\Url::to(['catalog/get-item']), $js);

$this->registerJs($js);

$podcats = CatalogRu::find()->where(['fiparent_catalog_id' => $catInfo->ficatalog_id])->andWhere(['fivisible' => 1])->orderBy('fiorder')->all();

?>
<?php echo $this->render('/catalog/breadcrumb', ['breadcrumb' => $breadCrumbs]) ?>

<div class="page_ttl">
    <div class="inner_width">
        <h1><?php echo $this->title ?></h1>
    </div>
</div>

<div class="inner_width clrmar center">
    <?php foreach ($podcats as $pc): ?>
    <div class="blocklink">
        <a href="<?php echo \yii\helpers\Url::to(['catalog/index', 'cat' => $pc->ficatalog_id]) ?>">
            <div class="blocklink_ttl">
                <div class="pic"><?php if(strlen($pc->fscatalog_img)): ?>
                        <?php echo Html::img(Yii::$app->request->baseUrl . '/' . $pc->fscatalog_img, ['style' => 'max-width:50px;max-height:75px;']); ?>
                    <?php endif; ?></div>
                        <span class="catalogcol_item_ttl"><?php echo $pc->fsname; ?></span>
            </div>
            <p>
                <?php echo strip_tags($pc->fscatalog_text); ?>
            </p>
        </a>
    </div>
    <?php endforeach; ?>
</div>

<div class="inner_width ">
    <div class="description">
        <div class="separator"></div>
        <?php echo strip_tags($catInfo['fsdestination']) ?>
    </div>
</div>