<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\News;

/**
 * NewsSearch represents the model behind the search form about `app\models\News`.
 */
class NewsSearch extends News
{
    public $q;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['finewsid'], 'integer'],
            [['fstitle', 'fstext', 'fdcreate_date','q', 'lang'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = News::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if(!isset($params['sort'])) {
            $dataProvider->setSort(['defaultOrder' => ['finewsid' => SORT_DESC]]);
        }

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'finewsid' => $this->finewsid,
            'fdcreate_date' => $this->fdcreate_date,
            'lang' => $this->lang,
        ]);

        $query->andFilterWhere(['like', 'fstitle', $this->fstitle])
            ->andFilterWhere(['like', 'fstext', $this->fstext]);

        $query->orFilterWhere(['like', 'fstitle', $this->q])
            ->orFilterWhere(['like', 'fstext', $this->q])
            ->orFilterWhere(['like', 'fsanons', $this->q])
            ->orFilterWhere(['like', 'fsanons_general', $this->q]);

        return $dataProvider;
    }
}
