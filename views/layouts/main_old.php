<?php
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\components\SiteView;
use yii\helpers\Url;
use yii\bootstrap\ButtonDropdown;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

/* @var $this SiteView */
/* @var $content string */

AppAsset::register($this);

$js = <<< JS

$(function() {
    $('.container > .main-menu').on('click', '> div', function() {
        window.location.href = $(this).find('a').attr('href');
        return false;
    });

    $(window).scroll(function() {
        if($(this).scrollTop() != 0) {
            $('#toTop').fadeIn();
        }
        else {
            $('#toTop').fadeOut();
        }
    });

    $('#toTop').click(function() {
        $('body,html').animate({scrollTop:0},800);
    });

});

JS;

$this->registerJs($js);

?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="shortcut icon" href="/favicon.ico">
        <link rel="icon" href="/favicon.ico">
        <link href="/css/fonts.css" type="text/css" rel="stylesheet" title="style" media="all" />
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
        <link href="/css/reset.css" type="text/css" rel="stylesheet" title="style" media="all" />
        <link href="/css/style.css" type="text/css" rel="stylesheet" title="style" media="all" />
        <link href="/css/style1.css" type="text/css" rel="stylesheet" title="style" media="all" />
        <link href="/libs/owlcarousel2/owl.carousel.css" type="text/css" rel="stylesheet" title="style" media="all" />
        <link href="/libs/fancybox/jquery.fancybox.css" type="text/css" rel="stylesheet" title="style" media="all" />
        <!--[if lt IE 9]>
        <script src="/libs/html5shiv/es5-shim.min.js"></script>
        <script src="/libs/html5shiv/html5shiv.min.js"></script>
        <![endif]-->
        <?php echo @$this->seoFields; ?>
        <?php if(strpos($this->seoFields,'<title>') === false): ?>
            <title><?= Html::encode($this->title) ?></title>
        <?php endif; ?>
    </head>
    <body>

    <?php $this->beginBody() ?>

    <header class="head inner_width">
        <div class="head_logo">
            <a href="<?= Url::to(['/']) ?>"><img src="/images/logo.jpg" alt=""></a>
            <div class="head_ttl">Производство электротехнической продукции,
                релейной защиты и автоматики</div>
        </div>
        <ul class="head_list">
            <li><a href="<?php echo Url::to(['news/index']) ?>">Новости</a></li>
            <li><a href="<?php echo Url::to(['site/contact']) ?>">Контакты</a></li>
            <li><a href="<?php echo Url::to(['forum/index']) ?>">Форум</a></li>
            <li><a href="#"><i class="icyou"></i></a></li>
        </ul>
        <div class="munubtn">
            <span></span>
            <span></span>
            <span></span>
        </div>
        <div class="searchbtn hide visible1180"><img src="images/icons/search2.png" alt=""/></div>
        <form class="menu_search modtog" id="js-search">
            <input type="text" placeholder="" name=""/>
            <input type="submit" value="" name=""/>
        </form>
        <div class="head_linkwrap">
            <a href="#js-subscribe" class="head_link bl jsfancy">Подписаться на новости</a>
            <div class="head_link red" id="js-support">
                Поддержка
                <div class="support">
                    <a href="#" id="js-supportclose" class="support_close">Close</a>
                    <b>Головной офис компании</b>
                    <a href="tel:+375293194373">+375 29 319 43 73</a>
                    <a href="tel:+375154554740">+375 15 455 47 40</a>
                    <a href="tel:+375298695606">+375 29 869 56 06</a>
                    <b>Бесплатный по России</b>
                    <a href="tel:8(800)7079949">8 (800) 707 99 49</a>
                    <b>Skype</b>
                    <a href="skype:support-2.euroautomatika">support-2.euroautomatika</a>
                    <b>E-mail:</b>
                    <a href="mailto:support@fif.by">support@fif.by</a>
                </div>
            </div>
        </div>
    </header>
    
    <?php
    $catalog = \app\models\CatalogRu::find()
        ->where('fiparent_catalog_id is NULL')
        ->orderBy('fiorder ASC')
        ->all();
    ?>

    <div class="menuline">
        <div class="inner_width">
            <ul class="menuline_wr">
                <li  class="menu2">
                    <a href="<?php echo Url::to(['catalog/index']) ?>" id="js-menu">Продукция <i class="icmenu"></i></a>
                    <ul class="level2">
                        <?php foreach ($catalog as $cat): ?>
                            <li><a href="<?php echo Url::to(['catalog/index','cat' => (int)$cat['ficatalog_id'] ]); ?>"><?php echo htmlspecialchars($cat['fsname']) ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                </li>
                <li><a href="<?php echo Url::to(['site/about']) ?>">О Компании</a></li>
                <li><a href="<?php echo Url::to(['site/downloads']) ?>">Материалы</a></li>
                <li><a href="<?php echo Url::to(['distributors/index']) ?>">Где купить</a></li>
                <li class="hide visible790"><a href="<?php echo Url::to(['news/index']) ?>">Новости</a></li>
                <li class="hide visible790"><a href="<?php echo Url::to(['site/contacts']) ?>">Контакты</a></li>
                <li class="hide visible790"><a href="#">Форум</a></li>
            </ul>
            <div class="menuline_right">
                <form class="menu_search hidden1180">
                    <input type="text" placeholder="" name=""/>
                    <input type="submit" value="" name=""/>
                </form>
                <ul class="menuline_wr">
                    <li><a href="#" class="ffm">Подбор аналогов</a></li>
                    <li><a href="#" class="ffm"><i class="icru"></i>Дистрибуция в РФ</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="main">
        <?= $content ?>
    </div>

    <footer class="foot mod">
        <div class="inner_width ovfl">
            <div class="foot_half">
                <div class="foot_item map">
                    <a href="#"><img src="images/logofoot.png" alt=""></a>
                </div>
                <div class="foot_item">
                    <div class="foot_listhalf">
                        <a href="#">Продукция</a>
                        <a href="#">Материлы</a>
                        <a href="#">О компании</a>
                        <a href="news.html">Новости</a>
                        <a href="contacts.html">Контакты</a>
                        <a href="#">Где купить</a>
                    </div>
                    <div class="foot_listhalf">
                        <a href="#">Форум</a>
                        <a href="#">Подбор Аналога</a>
                    </div>
                </div>
            </div>
            <div class="foot_half right">
                <div class="foot_item">
                    <div class="foot_ttl">Контакты:</div>
                    <p>Республика Беларусь, 231300,<br>
                        г. Лида, ул. Минская, 18А <br>
                        <a href="#" class="foot_link">Cмотреть на карте</a></p>
                    <div class="foot_ttl mod">+375 (154) 56 05 29</div>
                </div>
                <div class="foot_item modr">
                    <div class="foot_ttl">Cледите за нами:</div>
                    <form class="foot_form">
                        <input type="text" placeholder="Введите ваш email"/>
                        <input type="submit" value="OK"/>
                    </form>
                    <div class="foot_social">
                        <a href="#"><img src="images/icons/you.png" alt=""/></a>
                        <a href="#"><img src="images/icons/fb2.png" alt=""/></a>
                    </div>
                </div></div>
            <div class="separator modf"></div>
            <div class="foot_copy">
                &copy; 2011–2016. Все права защищены. СООО «Евроавтоматика ФиФ»                 <span>Разработка сайта <a href="http://www.hi-lead.by/" class="lnk" target="_blank">hi-lead</a></span>
            </div>
        </div>
    </footer>


    <div class="hide">
        <div id='js-subscribe' class="subscribemain">
            <div class="subscribemain_top">
                <b>Подписка на новости</b>
                <p>Будьте первым кто узнает о акциях, выставках и интересных новостях <br>из мира элеткротехнической продукции</p>
            </div>
            <form class="subscribemain_fr">
                <input type="text" placeholder="Введите email"/>
                <input type="submit" class='catalog_item_more' value="Подписаться">
            </form>
            <a href="javascript:parent.$.fancybox.close();" class="subscribemain_thx" >Cпасибо, я уже c вами</a>
        </div>
    </div>
    <script type="text/javascript" src="/libs/jquery/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="/libs/fancybox/jquery.fancybox.js"></script>
    <script type="text/javascript" src="/libs/owlcarousel2/owl.carousel.min.js"></script>
    <script src="/js/main.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.owl-carousel').owlCarousel({
                loop:true,
                nav:false,
                items:1
            });
            /*for choose*/
            $('.choose_form').change(function(){
                var a = $('.choosetabs li.act').attr('data-num');
                $('.choose_form_tab').removeClass('show');
                $('.choosetabs li').removeClass('act');
                if( a==0){
                    $('.choose_form_tab[data-tab="1"]').addClass("show");
                    $('.choosetabs li[data-num="1"]').addClass('act');
                }
                if( a==1){
                    $('.choose_form_tab[data-tab="2"]').addClass("show");
                    $('.choosetabs li[data-num="2"]').addClass('act');
                }
            });
            $('.choosetabs li').click(function(){
                $('.choose_form_tab').removeClass('show');
                $('.choosetabs li').removeClass('act');
                $(this).addClass('act');
                var a = $('.choosetabs li.act').attr('data-num');
                $('.choose_form_tab').each(function(){
                    if($(this).attr('data-tab')==a){
                        $(this).addClass('show');
                    }
                });
            });
        });
    </script>

    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>