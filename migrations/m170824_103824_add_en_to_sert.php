<?php

use yii\db\Migration;

class m170824_103824_add_en_to_sert extends Migration
{
    public function safeUp()
    {
        $this->addColumn('sertificats', 'lang', $this->string(2)->defaultValue('ru'));
    }

    public function safeDown()
    {
        $this->dropColumn('sertificats', 'lang');
    }
}
