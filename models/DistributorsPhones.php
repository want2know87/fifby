<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "distributors_phones".
 *
 * @property integer $fidistrib_mail_id
 * @property integer $fidistr_id
 * @property string $fsphone
 *
 * @property Distributors $fidistr
 */
class DistributorsPhones extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'distributors_phones';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fidistr_id'], 'required'],
            [['fidistr_id'], 'integer'],
            [['fsphone'], 'string', 'max' => 255],
            [['fidistr_id', 'fsphone'], 'unique', 'targetAttribute' => ['fidistr_id', 'fsphone'], 'message' => 'The combination of Fidistr ID and Fsphone has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fidistrib_mail_id' => Yii::t('app', 'Fidistrib Mail ID'),
            'fidistr_id' => Yii::t('app', 'Fidistr ID'),
            'fsphone' => Yii::t('app', 'Fsphone'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFidistr()
    {
        return $this->hasOne(Distributors::className(), ['fidistr_id' => 'fidistr_id']);
    }
}
