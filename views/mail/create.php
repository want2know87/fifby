<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Mail */

$this->title = 'Создать подписку';
$this->params['admin'] = [
    'page_title' => $this->title,
    'main_title' => 'Управление адресами',
    'main_url' => '/mail/create'
];
?>
<div class="mail-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
