<?php

use yii\db\Schema;
use yii\db\Migration;

class m161027_172904_add_eng_fields_to_catalogru extends Migration
{
    public function up()
    {
        $this->addColumn('catalog_ru', 'fsname_en', Schema::TYPE_STRING);
        $this->addColumn('catalog_ru', 'fscatalog_text_en', Schema::TYPE_TEXT);
    }

    public function down()
    {
        $this->dropColumn('catalog_ru', 'fsname_en');
        $this->dropColumn('catalog_ru', 'fscatalog_text_en');
    }
}
