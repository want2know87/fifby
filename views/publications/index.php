<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PublicationsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Publications');
$this->params['admin'] = [
    'page_title' => $this->title,
    'main_title' => 'Загрузки',
    'main_url' => 'publications/admin'
];
?>
<div class="publications-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Publications'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'lang',
                'filterInputOptions' => ['prompt' => '', 'class'=> 'form-control'],
                'filter' => ['ru' => 'RU', 'en' => 'EN'],
            ],

            'fipublic_id',
            'fsname',
            'fsdesc:ntext',
            'fitype' => [
                'attribute' => 'fitype',
                'class' => \yii\grid\DataColumn::className(),
                'format' => 'raw',
                'value' => function($model){
                    /** @var \app\models\Publications $model */
                    return $model->fitype == 0 ? 'Каталог' : 'Публикация';
                },
                'filter' => false,
            ],
            'fsimage' => [
                'attribute' => 'fsimage',
                'class' => \yii\grid\DataColumn::className(),
                'format' => 'raw',
                'value' => function($model){
                    /** @var \app\models\Publications $model */
                    return Html::img(Yii::$app->request->baseUrl . '/' . $model->fsimage, ['style' => 'max-width:200px;']);
                    /*return isset($parent[$model->fiparent_catalog_id]) ? htmlspecialchars($parent[$model->fiparent_catalog_id]) : '';*/
                },
                'filter' => false,
            ],

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update}{delete}'],
        ],
    ]); ?>

</div>
