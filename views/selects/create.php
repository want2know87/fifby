<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Selects */

$this->title = 'Создать подбор';
$this->params['admin'] = [
    'page_title' => $this->title,
    'main_title' => 'Создать',
    'main_url' => 'selects/create'
];
?>
<div class="selects-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
