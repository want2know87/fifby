<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sertificats".
 *
 * @property integer $fisert_id
 * @property string $fssert_name
 * @property string $fssert_text
 * @property string $fssert_img
 * @property integer $priority
 * @property integer $lang
 */
class Sertificats extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sertificats';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fssert_text'], 'string'],
            [['fssert_name', 'fssert_img'], 'string', 'max' => 255],
            ['priority', 'integer'],
            ['lang', 'required'],
            ['lang', 'string', 'max' => 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fisert_id' => Yii::t('app', 'Fisert ID'),
            'fssert_name' => Yii::t('app', 'Fssert Name'),
            'fssert_text' => Yii::t('app', 'Fssert Text'),
            'fssert_img' => Yii::t('app', 'Fssert Img'),
            'priority' => 'Приоритет',
            'lang' => 'Язык',
        ];
    }
}
