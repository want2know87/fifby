<?php

use yii\db\Schema;
use yii\db\Migration;

class m150315_152614_distrib_address extends Migration
{
    public function safeUp()
    {
        $this->createTable('distributors_point', [
            'fidistr_point_id' => 'pk',
            'fidistr_id' => 'int(10) not null',
            'fsaddress' => 'varchar(512)',
            'maps' => 'varchar(255)',
            'fssite' => 'varchar(255)',
        ]);

        $this->createTable('distributors_point_mail', [
            'fidistrib_point_mail_id' => 'pk',
            'fidistr_point_id' => 'int(10) not null',
            'fsmail' => 'varchar(255)',
        ]);

        $this->addForeignKey('fk_distrib_point_mail', 'distributors_point_mail', 'fidistr_point_id', 'distributors_point', 'fidistr_point_id');

        $this->createIndex('ixdistr_point_mail', 'distributors_point_mail', 'fidistrib_point_mail_id');
        $this->createIndex('ixdistr_point_mail_name', 'distributors_point_mail', ['fidistr_point_id','fsmail'], true);

        $this->createTable('distributors_point_phones', [
            'fidistrib_point_mail_id' => 'pk',
            'fidistr_point_id' => 'int(10) not null',
            'fsphone' => 'varchar(255)',
        ]);

        $this->addForeignKey('fk_distrib_point_phone', 'distributors_point_phones', 'fidistr_point_id', 'distributors_point', 'fidistr_point_id');

        $this->createIndex('distr_point_phones', 'distributors_point_phones', 'fidistr_point_id');
        $this->createIndex('distr_point_phone_name', 'distributors_point_phones', ['fidistr_point_id','fsphone'], true);
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_distrib_point_phone','distributors_point_phones');
        $this->dropForeignKey('fk_distrib_point_mail','distributors_point_mail');
        $this->dropIndex('ixdistr_point_mail', 'distributors_point_mail');
        $this->dropIndex('ixdistr_point_mail_name', 'distributors_point_mail');
        $this->dropIndex('distr_point_phones', 'distributors_point_phones');
        $this->dropIndex('distr_point_phone_name', 'distributors_point_phones');
        foreach([
                    'distributors_point_phones',
                    'distributors_point_mail',
                    'distributors_point',
                ] as $tName) {
            $this->truncateTable($tName);
            $this->dropTable($tName);
        }

    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
