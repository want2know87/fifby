<?php
use yii\helpers\Html;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use \app\models\CatalogItems;
use \app\models\CatalogRu;

$catSearch = [];
$general = false;
if(isset($page) && $page == 'general') {
    $general = true;
    $catalogs = array_map(function($el) { return $el['items']; }, $cats);
    foreach($catalogs as $cat) {
        $catSearch = array_merge($catSearch, $cat);
    }
    $catalogs = ArrayHelper::map($catSearch, 'id', 'name');
    //$catslogs =
}

/** var array $item */
?>
<style>

span.where a {
    background: #ffe900;
    color: #000;
    text-decoration: none;
    transition: all 0.3s;
    border-radius: 3px;
    font-size: 17px;
    border: 1px solid #ffe900;
    padding: 12px 6px;
    position: relative;
    top: 2px;
}
span.where a:hover {
    background: none;
}
.show-more-wrap a.description{
    margin-left: 10px;
    width: 132px;
    height: 46px;
    border: 1px solid #02ad60;
    border-radius: 3px;
    background: #02ad60;
    text-align: center;
    font: 300 15px/45px 'roboto',sans-serif;
    text-decoration: none;
    color: #fff;
    transition: all 0.3s;
    cursor: pointer;
    padding: 5px 10px;
}
.show-more-wrap a.description:hover {
    color: #fff;
    background: #02ad60;
    box-shadow: 2px 2px 13px -2px rgba(0,0,0,0.75);
}
span.price-name{
    font-size: 15px;
    color:#a2a2a2;
}
span.number {
    font-size: 18px;
}
.item-one span.new {
    float: right;
    position: relative;
    top: 10px;
    font-family: Roboto,"Helvetica Neue",Helvetica,Arial,sans-serif;
    font-size: 14px;
    color: #0d0d0d;
    background-color: #ffe900;
    padding: 2px 5px;
}
.btn-lg, .btn-group-lg > .btn {
    padding: 10px 10px;
}
</style>
<div class="item col-md-3 col-xs-11 item-one padding-10 <?php if($item['fisale_id']) echo "sale"?>" data-id="<?php echo $item['fiitem_id'] ?>">
    <div class="title col-xs-8 padding-none">
        <?php echo $item->fsitem_name ?>
    </div>
    <div class="new col-xs-4 padding-none">
      <?php if((int)$item['finew']): ?>
        <span class="new">Новинка</span>
      <?php endif; ?>
    </div>
    <?php if($general): ?>
      <div class="col-xs-12 padding-none cat-link">
          <?php
          $catItem = CatalogItems::find()->where(['fiitem_id' => $item['fiitem_id']])->one();
          if(!isset($catalogs[$catItem['ficatalog_id']])) {
              $catalog = CatalogRu::find()->where(['ficatalog_id' => $catItem['ficatalog_id']])->one();
              $catItem = CatalogRu::find()->where(['ficatalog_id' => $catalog['fiparent_catalog_id']])->one();
          }
          ?>
          <?php echo Html::a(@$catalogs[$catItem['ficatalog_id']], Url::to(['catalog/index','cat' => $catItem['ficatalog_id']])); ?>
      </div>
    <?php endif; ?>
    <div class="col-xs-12 padding-none">
        <div class="img col-xs-5 padding-none"><a href="<?php echo \yii\helpers\Url::to(['catalog/get-item', 'id' => $item['fiitem_id']]) ?>">
            <?php $image = file_exists(Yii::$app->basePath . '/web/' . $item['fsitem_picture'] . '.small.jpg') ?
                    Yii::$app->request->baseUrl . '/' . $item['fsitem_picture'] . '.small.jpg' :
                    Yii::$app->request->baseUrl . '/' . $item['fsitem_picture'];
                    ?>
            <?php echo Html::img($image, ['style' => 'max-width:110px;max-height:160px;']) ?>
            <?php // echo Html::img(Yii::$app->request->baseUrl . '/' . $item['fsitem_picture'], ['style' => 'max-width:110px;max-height:160px;']) ?>
        </a>
        </div>
        <div class="desc col-xs-7 padding-none shadow-text" data-toggle="tooltip" data-placement="top" title="<?php echo $item->fsitem_small_desc ?>">
            <div class="description-item-similar">
                <?php echo StringHelper::truncate($item->fsitem_small_desc, 130) ?>
            </div>
        </div>
    </div>
    <div class="price col-xs-11 padding-none">
        <div class="col-xs-12 padding-none price-cell">
            <?php
            $itemPrice = [];
            foreach($prices as $name) {
                if(!empty($item['fsprice_' . strtolower($name)]) && strlen($item['fsprice_' . strtolower($name)])) {
                    $itemPrice[$name] = $item['fsprice_' . strtolower($name)];
                }
            }
            if(!empty($itemPrice)) {
                echo '<span class="price-name">'.Yii::t('app', 'Цена').': </span>';
                $priceNameDef = '';
                foreach($itemPrice as $name => $pr) {
                    echo '<span class="number" style="margin-right: 5px;">',$pr,'</span>';
                    $priceNameDef = $name;
                    break;
                }
                $items = [];
                foreach($itemPrice as $name => $pr) {
                    $items[] = [
                        'label' => $name,
                        'options' => [
                            'data-price' => $pr,
                        ],
                    ];
                }
                echo ButtonDropdown::widget ( [
                    'label' => $priceNameDef,
                    'options' => [
                        'class' => 'btn-lg btn-default',
                    ],
                    'dropdown' => [
                        'items' => $items,
                    ],
                ] );

            }
            ?>
        <span class="where"><a href="<?php echo \yii\helpers\Url::to(['distributors/index']) ?>"><?=Yii::t('app', 'Где купить')?></a></span>
        </div>
    </div>
    <div class="show-more-wrap">
        <div class="show-more padding-10 col-xs-11 padding-none">
            <a class="description" href="<?php echo \yii\helpers\Url::to(['catalog/get-item', 'id' => $item['fiitem_id']]) ?>">
                <?=Yii::t('app', 'Подробное техническое описание')?>
            </a>
        </div>
    </div>
</div>
