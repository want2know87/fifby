<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Town;
use app\models\DestributorsPoint;
use yii\widgets\ActiveField;
use dosamigos\google\maps\Map;
use dosamigos\google\maps\LatLng;

/* @var $this yii\web\View */
/* @var $model app\models\DistributorsPoint */
/* @var $form yii\widgets\ActiveForm */

$list = [
    '' => Yii::t('app', 'app.list.empty'),
];
$towns = Town::getAll();
$towns = $list + $towns;

$js = <<< JS
$('#search-address').bind("keypress", function(e) {
  if (e.keyCode == 13) {
    e.preventDefault();
    return false;
  }
});
JS;
$this->registerJs($js);

$js = <<< JS

// Create the search box and link it to the UI element.
var input = /** @type {HTMLInputElement} */(
  document.getElementById('search-address'));
// map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

var searchBox = new google.maps.places.SearchBox(/** @type {HTMLInputElement} */(input));

google.maps.event.addListener(searchBox, 'places_changed', function() {
    var places = searchBox.getPlaces();

    if (places.length == 0) {
      return;
    }

    var bounds = new google.maps.LatLngBounds();

    for (var i = 0, place; place = places[i]; i++) {
        bounds.extend(place.geometry.location);
    }

    %mapName%.fitBounds(bounds);

  });


var marker;

google.maps.event.addListener(%mapName%, 'click', function(event) {
   placeMarker(event.latLng);
});

function placeMarker(location) {
    if(typeof marker != "undefined") {
        marker.setMap(null);
    }
    $('#distributorspoint-maps').val(location);
    marker = new google.maps.Marker({
        position: location,
        map: %mapName%
    });
}
JS;

if(!$model->isNewRecord) {
    $temp = $model->maps;
    $temp = explode(',', trim($temp, '()'));
    if(count($temp) == 2) {
        $js .= <<< JS
            function getLatLngFromString(ll) {
                var latlng = ll.split(',')
                return new google.maps.LatLng(parseFloat(latlng[0]), parseFloat(latlng[1]));
            }
            marker = new google.maps.Marker({
            position: getLatLngFromString('%location%'),
            map: %mapName%
        });
JS;
        $js = str_replace('%location%', trim($model->maps,'()'), $js);
    }
}

// $this->registerJs($js);

?>

<div class="distributors-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'fitown_id')->dropDownList($towns) ?>

    <?php //echo $form->field($model, 'dist_type')->dropDownList(\app\models\DistributorsPoint::$DIS_TYPES_MAP) ?>

    <?= Html::hiddenInput('distr_id', (int)Yii::$app->request->get('distr_id')) ?>

    <?= $form->field($model, 'fssite')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'fsaddress')->textInput(['maxlength' => 512]) ?>

    <?= $form->field($model, 'fsaddress_en')->textInput(['maxlength' => 512]) ?>

    <div class="form-group field-distributors-distributorsphones">
        <label class="control-label" for="distributors-distributorsphones"><?php echo Yii::t('app', 'Distributors Phones'); ?></label>
        <?php echo Html::textarea('Distributors[distributorsPhones][]', implode("\r\n", array_map(function ($model) {return $model->fsphone; } , $model->distributorsPointPhones)), ['class' => 'form-control', 'id' => 'distributors-distributorsphones', 'row' => 4]) ?>
    </div>

    <div class="form-group field-distributors-distributorsmails">
        <label class="control-label" for="distributors-distributorsmails"><?php echo Yii::t('app', 'Distributors Mails'); ?></label>
        <?php echo Html::textarea('Distributors[distributorsMails][]', implode("\r\n", array_map(function ($model) {return $model->fsmail; } , $model->distributorsPointMails)), ['class' => 'form-control', 'id' => 'distributors-distributorsmails', 'row' => 4]) ?>
    </div>

	<?php echo $form->field($model, 'distribution_type_retail')->checkbox(); ?>

    <?php echo $form->field($model, 'distribution_type_wholesale')->checkbox(); ?>

    <?= $form->field($model, 'maps')->textInput(['maxlength' => 255, 'style' => 'display:none;']) ?>
    <label for="search-address">помощь в поиске по карте</label>
    <?php

    $coord = new LatLng(['lat' => 55.7522200, 'lng' => 37.6155600]);
    if(!$model->isNewRecord) {
        $temp = $model->maps;
        $temp = explode(',', trim($temp, '()'));
        if(count($temp) == 2) {
            $coord = new LatLng(['lat' => $temp[0], 'lng' => $temp[1]]);
        }
    }
    $map = new Map([
        'center' => $coord,
        'zoom' => 10,
    ]);

    echo Html::textInput('search-address','',['id' => 'search-address', 'class' => 'form-control']), "<br/>";
    $map->appendScript(str_replace('%mapName%', $map->getName(), $js));
    echo $map->display();

    ?>

    <br/>
    <br/>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'app.create') : Yii::t('app', 'app.update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
