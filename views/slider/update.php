<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Slider */

$this->title = 'Обновить слайд: ' . $model->fsslide_title;
$this->params['admin'] = [
    'page_title' => $this->title,
    'main_title' => 'Слайдер',
    'main_url' => 'slider/index'
];
?>
<div class="slider-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
