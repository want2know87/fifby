<?php

namespace app\controllers;

use app\models\Country;
use app\models\DistributorsMail;
use app\models\DistributorsPhones;
use app\models\DistributorsPoint;
use app\models\Town;
use Yii;
use app\models\Distributors;
use app\models\DistributorsSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\DistributorsPointMail;
use app\models\DistributorsPointPhones;
use yii\filters\AccessControl;

/**
 * DistributorsController implements the CRUD actions for Distributors model.
 */
class DistributorsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'select-country', 'select-town', 'select-distrib', 'view', 'get-distrib', 'admin', 'create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['index', 'select-country', 'select-town', 'select-distrib', 'view', 'get-distrib'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionGetDistrib($letters, $country, $town) {
        $return = [];
        $distribSelect = Distributors::find()
            ->where('fsname like :likefield', ['likefield' => '%' . $letters . '%']);
        if(!empty($country)) {
            $distribSelect->andWhere(['ficountry_id' => (int)$country]);
        }
        if(!empty($town)) {
            $distribSelect->andWhere($distribSelect->where,['fitown_id' => (int)$town]);
        }
        $distrib = $distribSelect->limit(10)->all();
        /** @var Distributors $distr */
        foreach($distrib as $distr) {
            $return[$distr->fidistr_id] = $distr->fsname;
        }
        return json_encode($return);
    }

    /**
     * Lists all Distributors models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->post('tel')){
            foreach (Yii::$app->request->post('mail') as $key => $value) {
                Yii::$app->mailer->compose()
                    ->setTo([$value,'2258769@tde-fif.ru','region1@fif.by'])
                    ->setFrom('region1@fif.by')
                    ->setSubject('Заказ обратного звонка')
                    ->setHtmlBody('Добрый день.<br>Письмо сгенерировано автоматически сайтом www.fif.by, www.tde-fif.ru<br>Клиент просит перезвонить ему на номер: '.Yii::$app->request->post('tel').'<br>При возникновении вопросов напишите нам: region1@fif.by<br><br>_____ <br><br>С уважением, команда СООО "Евроавтоматика ФиФ"')
                    ->send();
            }
            echo '{"send":"yes"}';
        } else {
            $searchModel = new DistributorsSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            if(Yii::$app->request->isAjax) {
                return $this->renderAjax('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                ]);
            }

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    public function actionSelectCountry($country, $town, $distrib) {
        $queryTown = Town::find()->orderBy(['fstown_name' => SORT_ASC]);
        $distribQuery = Distributors::find()->groupBy('fsname')->orderBy(['fsname' => SORT_ASC]);
        if(!empty($country)) {
            $queryTown->where(['ficountry_id' => (int)$country]);
            $distribQuery
                ->innerJoin('distributors_point', 'distributors.fidistr_id = distributors_point.fidistr_id')
                ->innerJoin('town', 'distributors_point.fitown_id = town.fitown_id')
                ->where(['town.ficountry_id' => (int)$country]);
        }
        if(!empty($town)) {
          $distribQuery
            ->where(['town.fitown_id' => (int)$town]);
        }
        if(!empty($distrib)) {
          $queryTown
            ->innerJoin('distributors_point', 'town.fitown_id = distributors_point.fitown_id')
            ->where(['distributors_point.fidistr_id' => (int) $distrib]);
        }
        $queryTown->orderBy('case when town.`fitown_id` in (1, 7) then -1 else `fstown_name` end, `fstown_name`');
        $towns = ArrayHelper::map($queryTown->all(), 'fitown_id', 'fstown_name');
        $distrib = ArrayHelper::map($distribQuery->all(), 'fidistr_id', 'fsname');
        return json_encode([
            'towns' => $towns,
            'distribs' => $distrib,
        ]);
    }

    public function actionSelectTown($town, $country, $distrib) {
        $queryCountry = Country::find()->orderBy(['fscountry_name' => SORT_ASC]);
        $distribQuery = Distributors::find()->groupBy('fsname')->orderBy(['fsname' => SORT_ASC]);
        $townJoined = false;
        if(!empty($town)) {
            $queryCountry
                ->innerJoin('town', 'town.ficountry_id = country.ficountry_id')
                ->where(['town.fitown_id' => (int)$town]);
            $townJoined = true;
            $distribQuery
                ->innerJoin('distributors_point', 'distributors.fidistr_id = distributors_point.fidistr_id')
                ->where(['distributors_point.fitown_id' => (int)$town])
                ->groupBy('fsname');
        }
        if(!empty($country)) {
          if(!$townJoined) {
            $distribQuery
              ->innerJoin('distributors_point', 'distributors.fidistr_id = distributors_point.fidistr_id')
              ->innerJoin('town', 'distributors_point.fitown_id = town.fitown_id')
              ->where(['town.ficountry_id' => (int)$country]);
          }
          else {
            $distribQuery
              ->innerJoin('town', 'distributors_point.fitown_id = town.fitown_id')
              ->where(['town.ficountry_id' => (int)$country])
              ->where(['distributors_point.fitown_id' => (int)$town]);
          }
        }
        if(!empty($distrib)) {
          if(!$townJoined) {
            $queryCountry
              ->innerJoin('town', 'town.ficountry_id = country.ficountry_id')
              ->innerJoin('distributors_point', 'distributors_point.fitown_id = town.fitown_id')
              ->where(['distributors_point.fidistr_id' => (int)$distrib]);
          }
          else {
              $queryCountry->innerJoin('distributors_point', 'distributors_point.fitown_id = town.fitown_id')
              ->where(['distributors_point.fidistr_id' => (int)$distrib]);
          }
        }
        $country = ArrayHelper::map($queryCountry->all(), 'ficountry_id', 'fscountry_name');
        $distrib = ArrayHelper::map($distribQuery->all(), 'fidistr_id', 'fsname');
        return json_encode([
            'countrys' => $country,
            'distribs' => $distrib,
        ]);
    }

    public function actionSelectDistrib($distrib, $country, $town) {
        $queryCountry = Country::find()->orderBy(['fscountry_name' => SORT_ASC]);
        $queryTown = Town::find()->orderBy(['fstown_name' => SORT_ASC]);
        $townFiltered = false;
        if(!empty($distrib)) {
            $townFiltered = true;
            $queryCountry
                ->innerJoin('town', 'town.ficountry_id = country.ficountry_id')
                ->innerJoin('distributors_point', 'town.fitown_id = distributors_point.fitown_id')
                ->where(['distributors_point.fidistr_id' => (int)$distrib])
                ->groupBy('fscountry_name');
            $queryTown
                ->innerJoin('distributors_point', 'town.fitown_id = distributors_point.fitown_id')
                ->where(['distributors_point.fidistr_id' => (int)$distrib])
                ->groupBy('fstown_name');
        }
        if(!empty($country)) {
          $queryTown
            ->innerJoin('country', 'country.ficountry_id = town.ficountry_id')
            ->where(['country.ficountry_id' => (int) $country]);
        }
        if(!empty($town)) {
          if(!$townFiltered) {
            $queryCountry
              ->innerJoin('town', 'town.ficountry_id = country.ficountry_id')
              ->where(['town.fitown_id' => (int)$town]);
          }
          else {
            $queryCountry->where(['town.fitown_id' => (int)$town]);
          }
        }
        $queryTown->orderBy('case when town.`fitown_id` in (1, 7) then -1 else `fstown_name` end, `fstown_name`');
        $country = ArrayHelper::map($queryCountry->all(), 'ficountry_id', 'fscountry_name');
        $towns = ArrayHelper::map($queryTown->all(), 'fitown_id', 'fstown_name');
        return json_encode([
            'countrys' => $country,
            'towns' => $towns,
        ]);
    }

    /**
     * Lists all Distributors models.
     * @return mixed
     */
    public function actionAdmin()
    {
        $this->layout = 'admin';
        $searchModel = new DistributorsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('admin', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Distributors model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Distributors model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->layout = 'admin';

        $model = new Distributors();

        $model->load(Yii::$app->request->post());

        $town = Town::find()->where(['fitown_id' => (int) $model->fitown_id])->all();
        if(!empty($town)) {
            $model->ficountry_id = $town[0]->ficountry_id;
        }

        if ($model->save()) {

            $additional = Yii::$app->request->post('Distributors');
            /*DistributorsPhones::deleteAll('fidistr_id = :distr', [':distr' => $model->fidistr_id]);
            DistributorsMail::deleteAll('fidistr_id = :distr', [':distr' => $model->fidistr_id]);*/
            foreach(['Phones', 'Mails'] as $field) {
                if(isset($additional['distributors' . $field]) && !empty($additional['distributors' . $field])) {
                    $value = $additional['distributors' . $field];
                    $values = array_map('trim',explode("\n", current($value)));
                    switch($field) {
                        case 'Phones':
                            foreach($values as $val) {
                                $phone = new DistributorsPhones();
                                $phone->fidistr_id = $model->fidistr_id;
                                $phone->fsphone = $val;
                                $phone->save();
                            }
                            break;
                        case 'Mails':
                            foreach($values as $val) {
                                $mail = new DistributorsMail();
                                $mail->fidistr_id = $model->fidistr_id;
                                $mail->fsmail = $val;
                                $mail->save();
                            }
                            break;
                        default:
                            break;
                    }
                }
            }

            return $this->redirect(['admin']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Distributors model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $this->layout = 'admin';

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save() && !(int) Yii::$app->request->post('filter')) {

            $town = Town::find()->where(['fitown_id' => (int) $model->fitown_id])->all();
            if(!empty($town)) {
                $model->ficountry_id = $town[0]->ficountry_id;
            }

            $model->save();

            $additional = Yii::$app->request->post('Distributors');
            /*DistributorsPhones::deleteAll('fidistr_id = :distr', [':distr' => $model->fidistr_id]);
            DistributorsMail::deleteAll('fidistr_id = :distr', [':distr' => $model->fidistr_id]);*/
            foreach(['Phones', 'Mails'] as $field) {
                if(isset($additional['distributors' . $field]) && !empty($additional['distributors' . $field])) {
                    $value = $additional['distributors' . $field];
                    $values = array_map('trim',explode("\n", current($value)));
                    switch($field) {
                        case 'Phones':
                            foreach($values as $val) {
                                $phone = new DistributorsPhones();
                                $phone->fidistr_id = $model->fidistr_id;
                                $phone->fsphone = $val;
                                $phone->save();
                            }
                            break;
                        case 'Mails':
                            foreach($values as $val) {
                                $mail = new DistributorsMail();
                                $mail->fidistr_id = $model->fidistr_id;
                                $mail->fsmail = $val;
                                $mail->save();
                            }
                            break;
                        default:
                            break;
                    }
                }
            }

            return $this->redirect(['admin']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Distributors model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        $points = DistributorsPoint::findAll(['fidistr_id' => $model->fidistr_id]);
        foreach($points as $point) {
            DistributorsPointPhones::deleteAll('fidistr_point_id = :distr', [':distr' => $point->fidistr_point_id]);
            DistributorsPointMail::deleteAll('fidistr_point_id = :distr', [':distr' => $point->fidistr_point_id]);
            $point->delete();
        }

        DistributorsPhones::deleteAll('fidistr_id = :distr', [':distr' => $model->fidistr_id]);
        DistributorsMail::deleteAll('fidistr_id = :distr', [':distr' => $model->fidistr_id]);
        $model->delete();

        return $this->redirect(['admin']);
    }

    /**
     * Finds the Distributors model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Distributors the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Distributors::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
