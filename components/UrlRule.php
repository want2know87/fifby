<?php
namespace app\components;

use Yii;
use yii\web\UrlRuleInterface;
use yii\base\Object;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

class UrlRule extends Object implements UrlRuleInterface
{
    public function createUrl($manager, $route, $params)
    {

        return false;
    }

    public function parseRequest($manager, $request)
    {
        @$path=explode('/', (parse_url(Yii::$app->request->url)['path']) )[1];
        if (in_array($path, ['index.php','index.html'])){
            Yii::$app->getResponse()->redirect(Url::to(['/']), 301)->send();
            exit;
        }

        return false;
    }
}