<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Distributors */

$this->title = Yii::t('app', 'app.internal.create-point-distrib');
$this->params['admin'] = [
    'page_title' => $this->title,
    'main_title' => 'Дистрибьютор',
    'main_url' => 'distributors/update?id='.$_GET['distr_id']
];
?>
<div class="distributors-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
