<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use dosamigos\google\maps\LatLng;
use dosamigos\google\maps\Map;

/* @var $this yii\web\View */
/* @var $model app\models\Distributors */

?>
<div class="distributors-view">

    <div class="col-xs-12 padding-none">
        <div class="col-xs-12 padding-none fix-height">
            <div class="col-xs-12 title"><?php echo $model->fsname ?></div>
            <div class="col-xs-3 info marker"><?php echo $model->fsaddress ?></div>
            <div class="col-xs-3 info phone"><?php
                foreach($model->distributorsPhones as $phone) {
                    echo $phone->fsphone, "<br/>";
                }
                ?></div>
            <div class="col-xs-3 info site"><a href="http://<?php echo str_replace('http://','',htmlspecialchars($model->fssite)) ?>"><?php echo str_replace('http://','',htmlspecialchars($model->fssite)) ?></a></div>
            <div class="col-xs-3 info mail"><?php
                foreach($model->distributorsMails as $mail) {
                    echo '<a href="mailto:',htmlspecialchars($mail->fsmail),'">',$mail->fsmail, "</a><br/>";
                }
                ?>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="col-xs-3 padding-none center-block show-close">
                <?php if('' != trim($model->maps)): ?>
                    <span class="show-map"><?php echo Yii::t('app', 'show.map') ?></span>
                    <span class="show-map" style="display: none;"><?php echo Yii::t('app', 'hide.map') ?></span>
                <?php endif; ?>
                <br/>
            </div>
        </div>
        <?php
        if('' != trim($model->maps)) {
            $temp = $model->maps;
            $temp = explode(',', trim($temp, '()'));
            if(count($temp) == 2) {
                $coord = new LatLng(['lat' => $temp[0], 'lng' => $temp[1]]);
                $map = new Map([
                    'center' => $coord,
                    'zoom' => 16,
                ]);
                $map->width = '100%';
                $map->height = '210px';
                $js = <<< JS
                                function getLatLngFromString(ll) {
                                    var latlng = ll.split(',')
                                    return new google.maps.LatLng(parseFloat(latlng[0]), parseFloat(latlng[1]));
                                }
                                marker = new google.maps.Marker({
                                position: getLatLngFromString('%location%'),
                                map: %mapName%
                            });
                            $(window).on('rerender-map', function() {
                                var center = %mapName%.getCenter();
                                google.maps.event.trigger(%mapName%, 'resize');
                                %mapName%.setCenter(center);
                            });
JS;
                $js = str_replace(array('%location%', '%mapName%'), array(trim($model->maps,'()'), $map->getName()), $js);
                //$map->appendScript($js);
                //echo $map->display();
            }
        }
        ?>
        <div class="col-xs-12 padding-none google-map-block" data-map="<?php echo trim($model->maps,'()') ?>" style="display: none;">
            <div class="gmap-map-canvas"></div>
            <?php
            if(isset($map)) {
                //echo $map->display();
            }
            ?>
        </div>
    </div>
    <?php /*echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            'fidistr_id',
            'fitown_id',
            'ficountry_id',
            'firegion_id',
            'fsname',
            'fsaddress',
            'maps',
        ],
    ]) */?>

</div>
