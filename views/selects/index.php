<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Selects;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Подбор на главной';
$this->params['breadcrumbs'][] = $this->title;
$this->params['admin'] = [
    'page_title' => $this->title,
    'main_title' => 'Подбор на главной',
    'main_url' => 'selects/index'
];
?>
<div class="selects-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать главную категорию', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php

    $parents = Selects::find()->where('fiparent_id = 0 OR fiparent_id IS NULL')->all();

    foreach ($parents as $parent) {
    ?>
        <div class="level-0">
            <?php echo Html::a($parent->fsselect_name, \yii\helpers\Url::to(['selects/update', 'id' => $parent->fiselect_id])) ?>
            <form method="POST" style="display: inline;" action="<?php echo \yii\helpers\Url::to(['selects/delete', 'id' => $parent->fiselect_id]) ?>">
                <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
                <input type="hidden" name="id" value="<?php echo $parent->fiselect_id ?>" />
                <button type="submit">-</button>
            </form>
            <form method="POST" style="display: inline;" action="<?php echo \yii\helpers\Url::to(['selects/create']) ?>">
                <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
                <input type="hidden" name="parent" value="<?php echo $parent->fiselect_id ?>" />
                <button type="submit">+</button>
            </form>
            <div class="elements-0">
            <?php
            $podcats = Selects::find()->where('fiparent_id = ' . (int)$parent->fiselect_id)->all();
            foreach ($podcats as $podcat) { ?>
                <div class="level-1">
                    <?php echo Html::a($podcat->fsselect_name, \yii\helpers\Url::to(['selects/update', 'id' => $podcat->fiselect_id])) ?>
                    <form method="POST" style="display: inline;" action="<?php echo \yii\helpers\Url::to(['selects/delete', 'id' => $podcat->fiselect_id]) ?>">
                        <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
                        <input type="hidden" name="id" value="<?php echo $podcat->fiselect_id ?>" />
                        <button type="submit">-</button>
                    </form>
                </div>
            <?php }
            ?>
            </div>
        </div>

    <?php } ?>

    <?/*= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'fiselect_id',
            'fsselect_name',
            'fiparent_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); */?>

</div>
