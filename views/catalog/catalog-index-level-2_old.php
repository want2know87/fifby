<?php

use yii\helpers\Html;
use app\models\CatalogRu;
use yii\bootstrap\ButtonDropdown;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CatalogSearchRu */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $subcat int */

$prices = ['BYR', 'RUR', 'USD'];

/** @var CatalogRu $catInfo */
$catInfo = CatalogRu::find()->where(['ficatalog_id' => $subcat])->andWhere(['fivisible' => 1])->one();

$this->title = $catInfo->fsname;

$items = Yii::$app->db->createCommand('SELECT
                                            i.*
                                        FROM
                                            items i
                                        /*LEFT JOIN params_values pv ON pv.fiitem_id = i.fiitem_id
                                        LEFT JOIN params_cat pc ON pv.fiparam_id = pc.fiparam_id*/
                                        WHERE
                                            i.fiitem_id IN(
                                                SELECT
                                                    fiitem_id
                                                FROM
                                                    catalog_items
                                                WHERE
                                                    ficatalog_id = :ficatalogid
                                            )
                                        ORDER BY fiorder',['ficatalogid' => $subcat])->queryAll();

$podcats = Yii::$app->db->createCommand('SELECT * FROM catalog_ru WHERE fiparent_catalog_id = :parentid', ['parentid' => $subcat])->queryAll();

$podItems = [];
$searchAr = [0];

foreach($podcats as $pod) {
    $podItems[$pod['ficatalog_id']] = [
        'id'       => $pod['ficatalog_id'],
        'name'     => $pod['fsname'],
        'elements' => [],
    ];
    $searchAr[] = (int)$pod['ficatalog_id'];
}

$itemsPodcats = Yii::$app->db->createCommand('SELECT
                                            i.*, ci.ficatalog_id as cat
                                        FROM
                                            items i
                                        /*LEFT JOIN params_values pv ON pv.fiitem_id = i.fiitem_id*/
                                        INNER JOIN catalog_items ci ON i.fiitem_id = ci.fiitem_id
                                        WHERE
                                            i.fiitem_id IN(
                                                SELECT
                                                    fiitem_id
                                                FROM
                                                    catalog_items
                                                WHERE
                                                    ficatalog_id IN (' . implode(',', $searchAr) . ')
                                            )
                                        ORDER BY fiorder')->queryAll();

foreach($itemsPodcats as $item) {
    $podItems[$item['cat']]['elements'][] = $item;
}

// print_r($items);

$js = <<< JS

$(function() {

var current = null;

$('#show-info').on('show.bs.modal', function() {

    $('#show-info .modal-body').css({'min-height' : $('#show-info .properties').height() + $('.modal-body .pictures').height() + 35});
    $('#show-info .modal-body .properties .image').css({'padding-top': ($('#show-info .properties').height()-$('#show-info .properties .image').height())/2 });

    $('#show-info .properties .image').hide();
    window.setTimeout(function() {
        $('#show-info .modal-body').css({'min-height' : $('#show-info .properties').height() + $('.modal-body .pictures').height() + 35});
        $('#show-info .modal-body .properties .image').css({'padding-top': ($('#show-info .properties').height()-$('#show-info .properties .image').height())/2 });
        $('#show-info').modal('handleUpdate');
    }, 250);
    window.setTimeout(function() {
        $('#show-info .modal-body').css({'min-height' : $('#show-info .properties').height() + $('.modal-body .pictures').height() + 35});
        $('#show-info').modal('handleUpdate');
    }, 500);
    window.setTimeout(function() {
        $('#show-info .modal-body').css({'min-height' : $('#show-info .properties').height() + $('.modal-body .pictures').height() + 35});
        $('#show-info').modal('handleUpdate');
    }, 1000);
    $('#show-info .properties .image').show();
});

$('#show-info').on('click', 'div.left', function() {
    var self = $('.item-one[data-id=' + current + ']');
    self = self.prev('.item-one');
    if(!self.length) {
        self = $('.item-one:last');
    }
    self.find('.show-more').click();
});

$('#show-info').on('click', 'div.right', function() {
    var self = $('.item-one[data-id=' + current + ']');
    self = self.next('.item-one');
    if(!self.length) {
        self = $('.item-one:first');
    }
    self.find('.show-more').click();
});

$('.catalogs-items .item-one').on('click', '.show-more', function() {
    current = $(this).closest('.item-one').data('id');
    /*console.log($(this).closest('.item-one').data('id'))*/
    $.post('%URL%', {id : $(this).closest('.item-one').data('id')})
        .done(function(data) {
            $('#show-info .modal-content .modal-body').html(data);
            var item = $.parseJSON($('#show-info .dialog-data').html());
            $('#show-info .modal-header span.title')
                .remove();
            $('<span/>')
                .addClass('title')
                .text(item['fsitem_name'])
                .prependTo($('#show-info .modal-header'));
            /*console.log(item);*/
            $('#show-info .properties .image').hide();
            $('#show-info').modal();
        });
});

$('.catalogs-items .item-one').on('click', '.description a', function() {
    /*console.log($(this).closest('.item-one').data('id'))*/
    if(!$(this).data('id')) {
        return false;
    }
    current = $(this).closest('.item-one').data('id');
    $.post('%URL%', {id : $(this).data('id')})
        .done(function(data) {
            $('#show-info .modal-content .modal-body').html(data);
            var item = $.parseJSON($('#show-info .dialog-data').html());
            $('#show-info .modal-header span.title')
                .remove();
            $('<span/>')
                .addClass('title')
                .text(item['fsitem_name'])
                .prependTo($('#show-info .modal-header'));
            /*console.log(item);*/
            $('#show-info .properties .image').hide();
            $('#show-info').modal();
        });
});

$('.catalogs-items .item-one').on('click', '.btn-group .dropdown-menu li', function() {
    var price = $(this).data('price');
    var name = $(this).text();
    $(this).closest('.btn-group').find('.btn').html(name + ' <span class="caret"></span>');
    $(this).closest('.price-cell').find('.number').text(price);
});

$('[data-toggle="tooltip"]').tooltip();

});

JS;

$js = str_replace('%URL%', \yii\helpers\Url::to(['catalog/get-item']), $js);

$this->registerJs($js);

?>
<?php echo $this->render('/catalog/breadcrumb', ['breadcrumb' => $breadCrumbs]) ?>
<?php
    $modal = \yii\bootstrap\Modal::begin(['id' => 'show-info']);
    $modal->end();
?>
<div class="catalog-subcats">
    <div class="col-xs-12 padding-none general-titles center-block">
        <h2><?php echo $this->title ?></h2>
    </div>
    <?php if(strlen($catInfo->fsdestination) || strlen($catInfo->fswork) || strlen($catInfo->fsuse)): ?>
    <div class="col-xs-12 padding-none cat-desc">
        <?php if(strlen($catInfo->fsdestination)): ?>
        <div class="col-xs-12 padding-none">
            <div class="col-xs-12 padding-none title">Назначение</div>
            <div class="col-xs-12 padding-none desc"><?php echo $catInfo->fsdestination ?></div>
        </div>
        <?php endif; ?>
        <?php if(strlen($catInfo->fswork)): ?>
        <div class="col-xs-12 padding-none">
            <div class="col-xs-12 padding-none title">Принцип работы</div>
            <div class="col-xs-12 padding-none desc"><?php echo $catInfo->fswork ?></div>
        </div>
        <?php endif; ?>
        <?php if($catInfo->fsuse): ?>
        <div class="col-xs-12 padding-none">
            <div class="col-xs-12 padding-none title">Применение</div>
            <div class="col-xs-12 padding-none desc"><?php echo $catInfo->fsuse ?></div>
        </div>
        <?php endif; ?>
    </div>
    <?php endif; ?>
    <div class="col-xs-12 padding-none catalogs-items">
        <?php foreach($podItems as $podcats): ?>
            <div class="col-xs-12 padding-none podcats-element">
                <div class="elem">
                    <span class="title"><?php echo $podcats['name'] ?></span>
                    <div class="line"><div class="inner"></div></div>
                </div>
            </div>
            <div class="col-xs-12 padding-none">
            <?php foreach($podcats['elements'] as $item): ?>
                <?php if(!empty($item['fsitem_before_text'])): ?>
                    <div class="col-xs-12 padding-none sub-item-desc">
                        <?php echo $item['fsitem_before_text'] ?>
                    </div>
                <?php endif; ?>
                <?php echo $this->render('item', ['item' => $item, 'prices' => $prices]) ?>
            <?php endforeach; ?>
            </div>
        <?php endforeach; ?>

        <?php foreach($items as $item): ?>
            <?php if(!empty($item['fsitem_before_text'])): ?>
                <div class="col-xs-12 padding-none sub-item-desc">
                    <?php echo $item['fsitem_before_text'] ?>
                </div>
            <?php endif; ?>
            <?php echo $this->render('item', ['item' => $item, 'prices' => $prices]) ?>
        <?php endforeach; ?>
    </div>
</div>
