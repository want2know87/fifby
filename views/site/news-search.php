<?php
use yii\helpers\Html;
use yii\bootstrap\ButtonDropdown;

/** var array $item */

?>
<div class="col-xs-12 item-new-one padding-none " data-id="<?php echo $item['finewsid'] ?>">

    <?php
        $itemValues = @$itemsValues[$item['finewsid']];
        if(empty($itemValues)) {
            $itemValues = [];
        }
        $itemValues = array_map(
          function($el) {
            return @array_values($el)[0];
        }, $itemValues);
    ?>

    <div class="hidden-info" style="display: none;"><?php echo json_encode($itemValues) ?></div>
    <div class="col-xs-12 padding-none">
        <div class="desc col-xs-12 padding-none" data-toggle="tooltip" data-placement="top" title="<?php echo $item['fsanons'] ?>">
            <div class="description">
                <p class="title"><?php echo Html::a($item['fstitle'], \yii\helpers\Url::to(['news/view', 'id' => $item['finewsid']])) ?></p>
                <?php echo $item['fsanons'] ?>
                    <?php /*if(!empty($itemsValues[$item['finewsid']])) {
                        $index = 0;
                        foreach(@$itemsValues[$item['finewsid']] as $id => $params) {
                            echo '<div class="col-xs-9 param-name padding-none"><div class="dots"></div><div class="name">',array_keys($params)[0], '</div></div><div class="col-xs-3 param-value padding-none">', array_values($params)[0], '</div>';
                            $index++;
                            if($index>=5) {
                                break;
                            }
                        }
                    }*/
                ?>
            </div>
        </div>
    </div>
</div>