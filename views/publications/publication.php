<?php

use app\models\Publications;
use app\models\PublicationFiles;
use yii\helpers\Html;

/** @var Publications $model  */

$files = PublicationFiles::find()->where(['fipublic_id' => $model->fipublic_id])->all();

?>

<div class="col-xs-12 padding-none publication">
    <div class="col-md-2 padding-none image">
        <?php echo Html::img(Yii::$app->request->baseUrl . '/' . $model->fsimage) ?>
    </div>
    <div class="col-md-10 padding-none">
        <div class="col-xs-12 padding-none title">
            <?php echo $model->fsname ?>
        </div>
        <div class="col-xs-12 padding-none description">
            <?php echo $model->fsdesc ?>
        </div>
        <?php foreach($files as $file): ?>
            <?php /** @var PublicationFiles $file */ ?>
            <div class="col-xs-12 padding-none public">
                <i class="item"></i><?php echo \yii\helpers\Html::a($file->fsname . '<span class="gray"> (' . (file_exists(Yii::$app->basePath . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . $file->fsfile) ? round(filesize(Yii::$app->basePath . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . $file->fsfile)/1024, 0) : '') . ' Кб)</span>', Yii::$app->request->baseUrl . '/' . $file->fsfile, ['target' => '_blank']); ?>
            </div>
        <?php endforeach; ?>
    </div>
</div>