<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CatalogRu */

$this->title = Yii::t('app', 'Create {modelClass}', [
    'modelClass' => Yii::t('app','Catalog Ru'),
]);
$this->params['admin'] = [
    'page_title' => $this->title,
    'main_title' => 'Каталог',
    'main_url' => 'catalog/admin'
];
?>
<div class="catalog-ru-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
