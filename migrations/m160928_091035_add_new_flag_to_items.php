<?php

use yii\db\Schema;
use yii\db\Migration;

class m160928_091035_add_new_flag_to_items extends Migration
{
    public function up()
    {
        $this->addColumn('items', 'new_flag', Schema::TYPE_BOOLEAN . ' not null default 0');
    }

    public function down()
    {
        $this->dropColumn('items', 'new_flag');
    }
}
