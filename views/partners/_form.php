<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Partners */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="partners-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype'=>'multipart/form-data']
    ]); ?>

    <?= $form->field($model, 'fspartner_name')->textInput(['maxlength' => 255]) ?>

    <?php if(strlen($model->fspartner_image)): ?>
        <div class="form-group">
            <?php echo Html::img(Yii::$app->request->baseUrl . '/' . $model->fspartner_image) ?>
        </div>
    <?php endif; ?>

    <?= $form->field($model, 'fspartner_image')->fileInput() ?>

    <?= $form->field($model, 'fspartner_link')->textInput(['maxlength' => 255]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
