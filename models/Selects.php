<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "selects".
 *
 * @property string $fiselect_id
 * @property string $fsselect_name
 * @property integer $fiparent_id
 */
class Selects extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'selects';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fsselect_name'], 'required'],
            [['fiparent_id'], 'integer'],
            [['fsselect_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fiselect_id' => 'Внутренний номер',
            'fsselect_name' => 'Название выборки',
            'fiparent_id' => 'Родительская выборка',
        ];
    }
}
