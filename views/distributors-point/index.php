<?php

use yii\helpers\Html;
use app\models\Town;
use app\models\Country;
use yii\data\ActiveDataProvider;
use app\models\Distributors;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DistributorsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'app.internal.distribs');

$list = [
    '' => Yii::t('app', 'app.list.empty'),
];

$towns = $list + Town::getAll();
$country = $list + Country::getAll();

$this->registerJsFile('https://maps.googleapis.com/maps/api/js?key=&libraries=places&language=ru');
/*$this->registerJsFile('https://maps.gstatic.com/maps-api-v3/api/js/20/1/intl/ru_ALL/main.js');
$this->registerJsFile('https://maps.gstatic.com/maps-api-v3/api/js/20/1/intl/ru_ALL/places.js');*/

$js = <<< JS
$(function() {
    $('.filters').on('change', 'select', function(context, data) {
        if($(this).val() != '') {
            $(this).addClass('green');
        }
        else {
            $(this).removeClass('green');
        }
        if(typeof data == 'undefined') {
            var filters = {};
            $(this).parents('.filters').find('input,select').each(function() {
                filters[$(this).attr('name')] = $(this).val();
            });
            $.get('%url%', filters)
            .done(function(data) {
                $('.list-view').replaceWith($(data).find('.list-view'));
            });
        }
    });
    $('.filters select').each(function() {
        $(this).trigger('change', {internal : true});
    });
});
JS;

$js = str_replace('%url%', \yii\helpers\Url::to(['distributors/index']),$js);

$this->registerJs($js);

$js = <<< JS
    $(function() {
        function getLatLngFromString(ll) {
            var latlng = ll.split(',');
            return new google.maps.LatLng(parseFloat(latlng[0]), parseFloat(latlng[1]));
        }
        $('.distributors-index').on('click', '.show-close span', function() {
            if($(this).parents('.distributors-view').find('.google-map-block').is(':hidden')) {
                $(this).parents('.distributors-view').find('.google-map-block').show();
                $(this).parents('.distributors-view').find('.show-close span:first').hide();
                $(this).parents('.distributors-view').find('.show-close span:last').show();
                var center = $(this).parents('.distributors-view').find('.google-map-block').data('map');
                var mapOptions = {"center" : getLatLngFromString(center),"zoom":16};
                var container =  $(this).parents('.distributors-view').find('.gmap-map-canvas')[0];
                container.style.width = '100%';
                container.style.height = '210px';
                var gmap0 = new google.maps.Map(container, mapOptions);
                var marker = new google.maps.Marker({
                    position: getLatLngFromString(center),
                    map: gmap0
                });
                $(window).on('rerender-map', function() {
                    var center = gmap0.getCenter();
                    google.maps.event.trigger(gmap0, 'resize');
                    gmap0.setCenter(center);
                });

                $(window).trigger('rerender-map');
            }
            else {
                $(this).parents('.distributors-view').find('.google-map-block').hide();
                $(this).parents('.distributors-view').find('.show-close span:first').show();
                $(this).parents('.distributors-view').find('.show-close span:last').hide();
            }
        });
    })
JS;

$this->registerJs($js);

?>
<div class="distributors-index">

    <div class="col-xs-12 where-buy">
        <?php echo Yii::t('app', 'where.buy') ?>
    </div>

    <br/>
    <br/>

    <div class="col-xs-12 padding-none">
        <?php echo Yii::t('app', 'where.buy-filters') ?>
    </div>

    <br/>
    <br/>

    <div class="col-xs-12 padding-none filters">
        <div class="col-xs-4 padding-none">
            <label for="select-country"><?php echo Yii::t('app', 'select.country') ?></label>
            <?php echo Html::dropDownList('country', 'null', $country, ['id' => 'select-country', 'class' => 'form-control']); ?>
        </div>
        <div class="col-xs-4">
            <label for="select-town"><?php echo Yii::t('app', 'select.town') ?></label>
            <?php echo Html::dropDownList('town', 'null', $towns, ['id' => 'select-town', 'class' => 'form-control']); ?>
        </div>
        <!--<div class="col-xs-3">
            <label for="select-region"><?php /*echo Yii::t('app', 'select.region') */?></label>
            <?php /*echo Html::dropDownList('region', 'null', $regions, ['id' => 'select-region', 'class' => 'form-control']); */?>
        </div>-->
        <div class="col-xs-4">
            <label for="select-distrib"><?php echo Yii::t('app', 'select.distrib') ?></label>
            <?php echo \yii\jui\AutoComplete::widget([
                'id' => 'select-distrib',
                'name' => 'distrib',
                'options' => [
                    'class' => 'form-control',
                ],
                'clientEvents' => [
                    'select' => 'function () { console.log(arguments) }',
                ],
                'clientOptions' => [
                    'minLength' => 3,
                    'source'=>new JsExpression("function(request, response) {
                        $.getJSON('". \yii\helpers\Url::to(['distributors/get-distrib']) ."', {
                            letters: $(\"#select-distrib\").val(),
                            country: $('#select-country').val(),
                            town: $('#select-town').val()
                        }, response);
                    }"),
                ],
            ]); ?>
        </div>
        <!--<div class="col-xs-4">
            <label for="select-distrib"><?php /*echo Yii::t('app', 'select.distrib') */?></label>
            <?php /*echo Html::textInput('distrib', '', ['id' => 'find-distrib', 'class' => 'form-control']); */?>
        </div>-->
    </div>

    <br/>
    <br/>
    <br/>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>



    <?php

    $query = Distributors::find();
    if(Yii::$app->request->get('country')) {
        $query->andWhere(['ficountry_id' => (int)Yii::$app->request->get('country')]);
    }

    if(Yii::$app->request->get('town')) {
        $query->andWhere(['fitown_id' => (int)Yii::$app->request->get('town')]);
    }

    if(Yii::$app->request->get('distrib')) {
        $query->andWhere(['fsname' => Yii::$app->request->get('distrib')]);
    }

    /*if(Yii::$app->request->get('region')) {
        $query->where(['firegion_id' => (int)Yii::$app->request->get('region')]);
    }*/


    $provider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => [
            'pageSize' => 99999,
        ],
    ]);

    ?>

    <div>
    <?php

    echo \yii\widgets\ListView::widget([
        'dataProvider' => $provider,
        'itemView' => '/distributors/view',
        'options' => ['class' => 'list-view'],
        'itemOptions' => [
            'class' => 'block-item',
        ],
        'summaryOptions' => [
            'style' => 'display:none;'
        ],
    ]);

    ?>

    </div>


    <?php /*echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'fitown_id' => [
                'class' => \yii\grid\DataColumn::className(),
                'attribute' => 'fitown_id',
                'format' => 'html',
                'value' => function($model) use ($towns) {
                    return $towns[$model->fitown_id];
                }
            ],
            'ficountry_id' => [
                'class' => \yii\grid\DataColumn::className(),
                'attribute' => 'ficountry_id',
                'format' => 'html',
                'value' => function($model) use ($country) {
                    return $country[$model->ficountry_id];
                }
            ],
            'firegion_id' => [
                'class' => \yii\grid\DataColumn::className(),
                'attribute' => 'firegion_id',
                'format' => 'html',
                'value' => function($model) use ($regions) {
                    return $regions[$model->firegion_id];
                }
            ],
            'fsname',
            // 'fsaddress',
            // 'maps',

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {delete}'],
        ],
    ]); */?>

</div>
