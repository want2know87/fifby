<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DistributorsPoint */

$this->title = Yii::t('app', 'Update Distributors: ') . ' ' . $model->fsaddress;
$this->params['admin'] = [
    'page_title' => $this->title,
    'main_title' => 'Дистрибьютор',
    'main_url' => 'distributors/update?id='.$_GET['distr_id']
];
?>
<div class="distributors-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
