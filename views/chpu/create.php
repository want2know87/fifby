<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Chpu */

$this->title = Yii::t('app', 'Создать ЧПУ');
$this->params['admin'] = [
    'page_title' => $this->title,
    'main_title' => 'ЧПУ',
    'main_url' => 'chpu/index'
];
?>
<div class="chpu-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
