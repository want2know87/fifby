<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Town;

/**
 * TownSearch represents the model behind the search form about `app\models\Town`.
 */
class TownSearch extends Town
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fitown_id', 'ficountry_id'], 'integer'],
            [['fstown_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Town::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'fitown_id' => $this->fitown_id,
            'ficountry_id' => $this->ficountry_id,
        ]);

        $query->andFilterWhere(['like', 'fstown_name', $this->fstown_name]);

        return $dataProvider;
    }
}
