<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Publications */

$this->title = Yii::t('app', 'Create Publications');
$this->params['admin'] = [
    'page_title' => $this->title,
    'main_title' => 'Создать загрузку',
    'main_url' => 'publications/create'
];
?>
<div class="publications-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
