<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Mail */

$this->title = 'Обновить подписку: ' . ' ' . $model->fsmail;
$this->params['admin'] = [
    'page_title' => $this->title,
    'main_title' => 'Обновить подписку',
    'main_url' => '/mail/update'
];
?>
<div class="mail-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
