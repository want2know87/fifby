<?php

namespace app\models;

use Yii;
use app\models\traits\MultiLanguageTrait;

/**
 * This is the model class for table "distributors_point".
 *
 * @property integer $fidistr_point_id
 * @property integer $fidistr_id
 * @property string $fsaddress
 * @property string $maps
 * @property string $fssite
 * @property integer $fitown_id
 *
 * @property DistributorsPointMail[] $distributorsPointMails
 * @property DistributorsPointPhones[] $distributorsPointPhones
 */
class DistributorsPoint extends \yii\db\ActiveRecord
{
    use MultiLanguageTrait;

    /**
     * @inheritdoc
     */
    public static $DIS_TYPES_MAP = [
        0 => 'Опт/Розница',
        1 => 'Опт',
        2 => 'Розница',
    ];

    public static function getDistTypeName($type) {
        return self::$DIS_TYPES_MAP[$type];
    }

    public static function tableName()
    {
        return 'distributors_point';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fidistr_id'], 'required'],
            [['fidistr_id','fitown_id'], 'integer'],
            [['fsaddress'], 'string', 'max' => 512],
            [['maps', 'fssite'], 'string', 'max' => 255],
            [['distribution_type_retail', 'distribution_type_wholesale'], 'boolean'],
            [['fsaddress_en'], 'string', 'max' => 512],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fidistr_point_id' => Yii::t('app', 'Fidistr Point ID'),
            'fidistr_id' => Yii::t('app', 'Fidistr ID'),
            'fsaddress' => Yii::t('app', 'Fsaddress'),
            'fsaddress_en' => Yii::t('app', 'Fsaddress') . ' En',
            'maps' => Yii::t('app', ''),
            'fssite' => Yii::t('app', 'Fssite'),
            'fitown_id' => Yii::t('app', 'app.internal.town'),
            'dist_type' => Yii::t('app', 'app.internal.type'),
            'distribution_type_wholesale' => 'Опт',
            'distribution_type_retail' => 'Розница',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistributorsPointMails()
    {
        return $this->hasMany(DistributorsPointMail::className(), ['fidistr_point_id' => 'fidistr_point_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistributorsPointPhones()
    {
        return $this->hasMany(DistributorsPointPhones::className(), ['fidistr_point_id' => 'fidistr_point_id']);
    }
}
