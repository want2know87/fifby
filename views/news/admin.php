<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'News');
$this->params['admin'] = [
    'page_title' => $this->title
];
?>
<div class="news-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create News'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'lang',
                'filterInputOptions' => ['prompt' => '', 'class'=> 'form-control'],
                'filter' => ['ru' => 'RU', 'en' => 'EN'],
            ],
            'finewsid',
            'fstitle',
            'fdcreate_date',

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {delete}' ],
        ],
    ]); ?>

</div>
