<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/fonts.css',
    	'https://fonts.googleapis.com/css?family=Roboto:400,300,500&subset=latin,cyrillic',
    	'css/reset.css',
        'css/style.css',
    	'css/style1.css',
    	'libs/owlcarousel2/owl.carousel.css',
    	'libs/fancybox/jquery.fancybox.css',
    ];
    public $js = [
        'js/jquery-ui.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
