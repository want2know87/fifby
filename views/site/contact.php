<?php
//require_once 'about_old.php'; return;
use app\models\SiteTemplates;

/** @var SiteTemplates $template */
$template = SiteTemplates::findOne(['fscode' => '/site/contacts']);
$this->title = $template->fstitle;
$this->seoFields = $template->fstemplate_seo;
eval("?>" . $template->fstemplate_code . "<?php ");
return;

?>
<?php

$js = <<< JS
    $(function(){
        $(".contactslist li").on("click", function(){
            var tabs = $(".contactslist li"),
                cont = $(".contactstabwrap .contactstab");
            tabs.removeClass("act");
            cont.removeClass("act");
            $(this).addClass("act");
            cont.eq($(".contactslist li").index(this)).addClass("act");

            return false;
        });
    });
JS;

$this->registerJs($js);

?>

<div class="breadcrumbs">
    <div class="inner_width">
        <a href="#"><i class="ichome"></i>Главная</a>
        <span class="curr">Контакты</span>
    </div>
</div>
<div class="page_ttl">
    <div class="inner_width">
        <h1>Контакты</h1>
    </div>
</div>
<div class="inner_width ovfl">
    <ul class="contactslist">
        <li class="act"><a href="#">Отдел продаж</a></li>
        <li><a href="#" class="line2">Служба технической поддержки</a></li>
        <li><a href="#">Отдел развития</a></li>
        <li><a href="#">Главный инженер</a></li>
        <li><a href="#">Приёмная</a></li>
    </ul>
    <div class="contactstabwrap">
        <div class="contactstab act">
            <div class="contactstab_info">
                <div class="contactstab_ttl">Отдел продаж</div>
                231300, Республика Беларусь, г. Лида, ул. Минская 18А <br>
                <a href="tel:+375 154 55 47 40">+375 154 55 47 40</a>  <br>
                <a href="mailto:support@fif.by">support@fif.by</a>
            </div>
            <div class="contactstab_map">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2351.469841740677!2d25.273298115325858!3d53.88785254171363!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46de8a99cfba4df7%3A0x6dc359fe507fceaf!2z0JXQstGA0L7QsNCy0YLQvtC80LDRgtC40LrQsCDQpNC40KQ!5e0!3m2!1sru!2sru!4v1469520230018" width="400" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
        <div class="contactstab">
            <div class="contactstab_info">
                <div class="contactstab_ttl">Служба технической поддержки:</div>
                231300, Республика Беларусь, г. Лида, ул. Минская 18А <br>
                <a href="tel:+375 154 55 47 40">+375 154 55 47 40</a>  <br>
                <a href="tel:+375 154 60 03 80">+375 154 60 03 80</a><br>
                <a href="tel:+375 29 869 56 06">+375 29 869 56 06</a> (07:00 - 18:00)<br>
                <a href="tel:+375 29 319 43 73">+375 29 319 43 73</a><br>
                <a href="tel:+8 (800) 707-99-49">+8 (800) 707-99-49</a> - для Российской Федерации <br>(бесплатный)<br>
                <a href="mailto:support@fif.by">support@fif.by</a>
            </div>
            <div class="contactstab_map">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2351.469841740677!2d25.273298115325858!3d53.88785254171363!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46de8a99cfba4df7%3A0x6dc359fe507fceaf!2z0JXQstGA0L7QsNCy0YLQvtC80LDRgtC40LrQsCDQpNC40KQ!5e0!3m2!1sru!2sru!4v1469520230018" width="400" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
        <div class="contactstab">
            <div class="contactstab_info">
                <div class="contactstab_ttl">Отдел развития</div>
                231300, Республика Беларусь, г. Лида, ул. Минская 18А <br>
                <a href="tel:+375 154 55 47 40">+375 154 55 47 40</a>  <br>
                <a href="mailto:support@fif.by">support@fif.by</a>
            </div>
            <div class="contactstab_map">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2351.469841740677!2d25.273298115325858!3d53.88785254171363!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46de8a99cfba4df7%3A0x6dc359fe507fceaf!2z0JXQstGA0L7QsNCy0YLQvtC80LDRgtC40LrQsCDQpNC40KQ!5e0!3m2!1sru!2sru!4v1469520230018" width="400" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
        <div class="contactstab">
            <div class="contactstab_info">
                <div class="contactstab_ttl">Главный инженер</div>
                231300, Республика Беларусь, г. Лида, ул. Минская 18А <br>
                <a href="tel:+375 154 55 47 40">+375 154 55 47 40</a>  <br>
                <a href="mailto:support@fif.by">support@fif.by</a>
            </div>
            <div class="contactstab_map">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2351.469841740677!2d25.273298115325858!3d53.88785254171363!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46de8a99cfba4df7%3A0x6dc359fe507fceaf!2z0JXQstGA0L7QsNCy0YLQvtC80LDRgtC40LrQsCDQpNC40KQ!5e0!3m2!1sru!2sru!4v1469520230018" width="400" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
        <div class="contactstab">
            <div class="contactstab_info">
                <div class="contactstab_ttl">Приёмная</div>
                231300, Республика Беларусь, г. Лида, ул. Минская 18А <br>
                <a href="tel:+375 154 55 47 40">+375 154 55 47 40</a>  <br>
                <a href="mailto:support@fif.by">support@fif.by</a>
            </div>
            <div class="contactstab_map">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2351.469841740677!2d25.273298115325858!3d53.88785254171363!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46de8a99cfba4df7%3A0x6dc359fe507fceaf!2z0JXQstGA0L7QsNCy0YLQvtC80LDRgtC40LrQsCDQpNC40KQ!5e0!3m2!1sru!2sru!4v1469520230018" width="400" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</div>