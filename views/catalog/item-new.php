<?php
use yii\helpers\Html;
use yii\bootstrap\ButtonDropdown;

/** var array $item */
$itemSmallDescField = Yii::$app->language == 'en' ? 'fsitem_small_desc_en' : 'fsitem_small_desc';

?>

<?php
$itemValues = @$itemsValues[$item['fiitem_id']];
if(empty($itemValues)) {
    $itemValues = [];
}
$itemValues = array_map(
    function($el) {
        return @array_values($el)[0];
    }, $itemValues);
?>

<div class="catalog_item <?php if($item['new_flag']) echo "newFlag"?>" data-id="<?php echo $item['fiitem_id'] ?>">
<div class="hidden-info" style="display: none;"><?php echo json_encode($itemValues) ?></div>
    <div class="catalog_itemhalf">
        <?php $image = file_exists(Yii::$app->basePath . '/web/' . $item['fsitem_picture'] . '.small.jpg') ?
            Yii::$app->request->baseUrl . '/' . $item['fsitem_picture'] . '.small.jpg' :
            Yii::$app->request->baseUrl . '/' . $item['fsitem_picture'];
        ?>
        <div class="catalog_item_pic"><div class="helper"></div><a href="<?php echo \yii\helpers\Url::to(['catalog/items', 'id' => $item['fiitem_id']]); ?>"><img height="124.5" src="<?php echo $image ?>" alt=""></a></div>
        <div class="catalog_item_inf">
            <a href="<?php echo \yii\helpers\Url::to(['catalog/items', 'id' => $item['fiitem_id']]); ?>"
                style="display: inline-block;"
            >
                <?php echo $item['fsitem_name'] ?>
            </a>
            <?php if ($item['new_flag']): ?>
            <sup style="display: inline-block; font-weight: normal; font-size: 15px; margin-left: 15px; color: #e26060">NEW!</sup>
            <?php endif; ?>
            <ul>
                <?php $i=0; foreach (explode("\n", trim($item[$itemSmallDescField])) as $ite): $i++; if($i>3) break; ?>
                    <?php echo "<li>", $ite,"</li>"; ?>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
    <div class="catalog_itemhalf mod">
        <div class="tovar_price mod">
            <?php
            $itemPrice = [];
            foreach($prices as $name) {
                if(!empty($item['fsprice_' . strtolower($name)]) && strlen($item['fsprice_' . strtolower($name)])) {
                    $itemPrice[$name] = $item['fsprice_' . strtolower($name)];
                }
            }
            if(!empty($itemPrice)) {
                echo '<span>'.Yii::t('app', 'Цена').':</span>';
                $priceNameDef = '';
                foreach ($itemPrice as $name => $pr) {
                    echo '<span class="pr">', $pr, '</span>';
                    $priceNameDef = $name;
                    break;
                }
                echo "<div class=\"sel\"><span class=\"ar\"></span>";
                echo "<select class=\"sel\">";
                foreach ($itemPrice as $name => $pr) {
                    echo "<option data-price=\"", $pr, "\">", $name, "</option>";
                }
                echo "</select> <span style=\"font-size: 14px;color: #a2a2a2;\">с НДС</span></div>";
            }
            ?>
        </div>
        <a href="<?php echo \yii\helpers\Url::to(['catalog/items', 'id' => $item['fiitem_id']]); ?>" class="catalog_item_more"><?= Yii::t('app', 'app.internal.read-more') ?></a>
    </div>
</div>
