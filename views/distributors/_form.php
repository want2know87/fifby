<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Town;
use yii\widgets\ActiveField;
use dosamigos\google\maps\Map;
use dosamigos\google\maps\LatLng;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\Distributors */
/* @var $form yii\widgets\ActiveForm */

$list = [
    '' => Yii::t('app', 'app.list.empty'),
];
$towns = Town::getAll();
$towns = $list + $towns;

\yii\web\JqueryAsset::register($this);

$js = <<< JS

$(function() {

window.setTimeout(function() {
    $(document).off('change.yiiGridView keydown.yiiGridView', '#w1-filters input, #w1-filters select');

    $(document).on('keydown', '#w1-filters input, #w1-filters select', function(e) {
        if (e.keyCode  == 13) {
            var data = {};
            $.each($('#w1-filters input, #w1-filters select').serializeArray(), function () {
                data[this.name] = this.value;
            });
            $.post(window.location.href, data)
                .done(function(html) {
                    $(document).find('.grid-view').replaceWith($(html).find('.grid-view'));
                    for(k in data) {
                        $('input[name="'+ k +'"]').val(data[k]);
                    }
                });
            /*console.log(data);*/
            return false;
        }
    });

});

});

JS;

$this->registerJs($js);

?>

<div class="distributors-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'fsname')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'fsname_en')->textInput(['maxlength' => 255]) ?>

    <div class="well"><span class="glyphicon glyphicon-info-sign"></span> Чем больше приоритет, тем выше элемент в списке.</div>
    <?= $form->field($model, 'priority')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'app.create') : Yii::t('app', 'app.update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

  <?php if(!$model->isNewRecord): ?>
    <?php
    $itemModel = $model;
    $query = \app\models\DistributorsPoint::find()->where(['fidistr_id' => $model->fidistr_id]);
    $params = Yii::$app->request->post('DistributorsPointSearch');
    if(!empty($params['fitown_id'])) {
      $query->leftJoin('town','town.fitown_id = distributors_point.fitown_id');
      $query->andFilterWhere(['like', 'town.fstown_name', $params['fitown_id']]);
    }
    if(!empty($params['fsaddress'])) {
      $query->andFilterWhere(['like', 'fsaddress', $params['fsaddress']]);
    }
    $dataProvider = new \yii\data\ActiveDataProvider([
      'query' => $query,
    ]);
    $dataProvider->pagination->pageParam = 'point-page';
    ?>
    <div class="form-group">
      <?= \yii\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => new \app\models\DistributorsPointSearch(),
        'filterUrl' => ['distributors/update-filter', 'id' => $model->fidistr_id, 'page' => (int) Yii::$app->request->get('page'), 'filter' => 1],
        'columns' => [
          ['class' => 'yii\grid\SerialColumn'],

          'fitown_id' => [
            'attribute' => 'fitown_id',
            'class' => \yii\grid\DataColumn::className(),
            'format' => 'raw',
            'value' => function(\app\models\DistributorsPoint $model) use($towns) {
              return isset($towns[$model->fitown_id]) ? $towns[$model->fitown_id] : '';
            },
            /*'filter' => ArrayHelper::map(Town::find()->asArray()->all(), 'fitown_id', 'fstown_name'),*/
          ],
          'fsaddress',
          'fssite',

          [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update}{delete}',
            'urlCreator'=>function($action, $model, $key, $index) use ($itemModel) {
              return ['distributors-point/' . $action,'id'=>$model->fidistr_id, 'page' => (int)Yii::$app->request->get('page')];
            },
            'buttons' => [
              'update' => function($url, $model) use($itemModel) {
                return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                  ['distributors-point/update', 'id' => $model->fidistr_point_id, 'distr_id' => $itemModel->fidistr_id, 'page' => (int)Yii::$app->request->get('page')],
                  [
                    'title' => 'Изменить',
                    'data-pjax' => '0',
                  ]
                );
              },
              'delete' => function($url, $model) use($itemModel) {
                return Html::a('<span class="glyphicon glyphicon-trash"></span>',
                  ['distributors-point/delete', 'id' => $model->fidistr_point_id, 'distr_id' => $itemModel->fidistr_id, 'page' => (int)Yii::$app->request->get('page')],
                  [
                    'class' => 'remove-btn',
                    'data-pjax' => '0',
                    'data-confirm' => 'Удалить?',
                    'data-method' => 'post',
                  ]
                );
              }
            ]
          ],
        ],
      ]); ?>
      <?= Html::a('Создать точку', \yii\helpers\Url::to(['distributors-point/create', 'distr_id' => $model->fidistr_id, 'page' => Yii::$app->request->get('page', Yii::$app->request->post('page'))])) ?>
    </div>
  <?php endif; ?>

</div>
