<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Items */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app','Item'),
]) . ' ' . $model->fsitem_name;

$this->params['admin'] = [
    'page_title' => $this->title,
    'main_title' => 'Товары',
    'main_url' => 'items/admin'
];
?>
<div class="items-update">


    <?= $this->render('_form', [
        'model' => $model,
        'itemList' => $itemList,
    ]) ?>

</div>
