<?php

use yii\db\Migration;

class m171009_151312_similar_items_init extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%items__similar}}', [
            'item_id' => 'int(10) unsigned not null',
            'similar_item_id' => 'int(10) unsigned not null',
        ]);
        $this->addPrimaryKey(null, '{{%items__similar}}', ['item_id', 'similar_item_id']);
        $this->addForeignKey('fk-items__similar-item_id', '{{%items__similar}}', 'item_id', '{{%items}}', 'fiitem_id', 'CASCADE');
        $this->addForeignKey('fk-items__similar-similar_item_id', '{{%items__similar}}', 'similar_item_id', '{{%items}}', 'fiitem_id', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-items__similar-item_id', '{{%items__similar}}');
        $this->dropForeignKey('fk-items__similar-similar_item_id', '{{%items__similar}}');
        $this->dropTable('{{%items__similar}}');
    }
}
