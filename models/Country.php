<?php

namespace app\models;

use Yii;
use app\models\traits\MultiLanguageTrait;

/**
 * This is the model class for table "country".
 *
 * @property integer $ficountry_id
 * @property string $fscountry_name
 *
 * @property Distributors[] $distributors
 * @property Region[] $regions
 * @property Town[] $towns
 */
class Country extends \yii\db\ActiveRecord
{
    use MultiLanguageTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'country';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fscountry_name'], 'string', 'max' => 55],
            [['fscountry_name'], 'unique'],
            [['fscountry_name_en'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ficountry_id' => Yii::t('app', 'app.internal.number'),
            'fscountry_name' => Yii::t('app', 'app.fscountry.name'),
            'fscountry_name_en' => Yii::t('app', 'app.fscountry.name') . ' En',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistributors()
    {
        return $this->hasMany(Distributors::className(), ['ficountry_id' => 'ficountry_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTowns()
    {
        return $this->hasMany(Town::className(), ['ficountry_id' => 'ficountry_id']);
    }

    /**
     * @return array
     */
    public static function getAll() {
        $array = Country::find()->orderBy('ISNULL(`order`), `order`')->all();
        $list = [];
        /** @var Country $country */
        foreach($array as $country) {
            $list[$country->ficountry_id] = $country->fscountry_name;
        }
        return $list;
    }
}
