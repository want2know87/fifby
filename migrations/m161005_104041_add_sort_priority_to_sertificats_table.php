<?php

use yii\db\Schema;
use yii\db\Migration;

class m161005_104041_add_sort_priority_to_sertificats_table extends Migration
{
    public function up()
    {
        $this->addColumn('sertificats', 'priority', Schema::TYPE_INTEGER . ' not null default 0');
    }

    public function down()
    {
        $this->dropColumn('sertificats', 'priority');
    }
}
