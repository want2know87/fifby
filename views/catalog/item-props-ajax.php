<?php

use app\models\Items;
use app\models\ItemsPictures;
use yii\bootstrap\ButtonDropdown;
/** @var Items $itemInfo */

/*foreach($params as $param) {
    echo $param['name'], $param['value'];
}*/

$i = 0;

$prices = ['BYN', 'RUR', 'USD'];

$pictures = ItemsPictures::find()->where(['fiitem_id' => $itemInfo->fiitem_id])->all();

?>
<script class="dialog-data" type="application/json"><?php echo json_encode($itemInfo->toArray(['fsitem_name'])) ?></script>

<?php if(strlen($itemInfo->fspassport)): ?>
    <div class="item-passport">
      <?php echo \yii\helpers\Html::a('Паспорт изделия <span class="gray">(' . round(@filesize(Yii::$app->basePath . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . $itemInfo->fspassport)/1024, 0) . ' Кб)</span>', Yii::$app->request->baseUrl . '/' . $itemInfo->fspassport,['target' => '_blank']) ?>
    </div>
<?php endif; ?>

<div class="item-prices">
  <div class="price">
    <div class="price-cell">
      <?php
      $itemPrice = [];
      foreach($prices as $name) {
        if(!empty($itemInfo['fsprice_' . strtolower($name)]) && strlen($itemInfo['fsprice_' . strtolower($name)])) {
          $itemPrice[$name] = $itemInfo['fsprice_' . strtolower($name)];
        }
      }
      if(!empty($itemPrice)) {
        echo '<span class="price-name">Цена </span>';
        $priceNameDef = '';
        foreach($itemPrice as $name => $pr) {
          echo '<span class="number">',$pr,'</span>';
          $priceNameDef = $name;
          break;
        }
        $items = [];
        foreach($itemPrice as $name => $pr) {
          $items[] = [
            'label' => $name,
            'options' => [
              'data-price' => $pr,
            ],
          ];
        }
        echo ButtonDropdown::widget ( [
          'label' => $priceNameDef,
          'options' => [
            'class' => 'btn-lg btn-default',
          ],
          'dropdown' => [
            'items' => $items,
          ],
        ] );

      }
      ?>
    </div>
    <div class=""><div class="where"><a href="<?php echo \yii\helpers\Url::to(['distributors/index']) ?>">Где купить</a></div></div>
  </div>
</div>

<div class="col-xs-12 padding-none properties">
    <div class="col-xs-12 padding-none image vertical-align">
        <?php echo \yii\helpers\Html::img(Yii::$app->request->baseUrl . '/' . $itemInfo->fsitem_picture) ?>
    </div>
    <div class="col-xs-12">
        <?php if(!empty($params)): ?>
        <table>
            <tr><td colspan="2">Техническое описание</td></tr>
            <?php foreach($params as $param): ?>
                <tr class="columns">
                    <td class="<?php echo $i%2 == 0 ? 'green' : '' ?>"><?php echo $param['name'] ?></td>
                    <td class="<?php echo $i%2 == 0 ? 'green' : '' ?>"><?php echo $param['value'] ?></td>
                </tr>
            <?php $i++; endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>

<div class="left"></div>
<div class="right"></div>

<div class="col-xs-12 padding-none pictures">
    <?php foreach($pictures as $picture): ?>
        <?php /** @var ItemsPictures $picture */ ?>
        <div class="col-xs-12 picture-title"><?php echo $picture->fsname ?></div>
        <div class="col-xs-12 picture-image"><?php echo \yii\helpers\Html::img(Yii::$app->request->baseUrl . '/' . $picture->fsimage, ['style' => 'max-width:400px;padding-top:10px;']) ?></div>
    <?php endforeach; ?>
</div>