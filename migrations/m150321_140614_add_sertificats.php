<?php

use yii\db\Schema;
use yii\db\Migration;

class m150321_140614_add_sertificats extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->createTable('sertificats', [
            'fisert_id' => 'pk',
            'fssert_name' => 'varchar(255)',
            'fssert_text' => 'text',
            'fssert_img' => 'varchar(255)',
        ]);
    }
    
    public function safeDown()
    {
        $this->dropTable('sertificats');
    }

}
