<?php

use yii\db\Schema;
use yii\db\Migration;

class m150223_175409_add_site extends Migration
{
    public function up()
    {
        $this->addColumn('distributors', 'fssite', 'varchar(255)');
    }

    public function down()
    {
        $this->dropColumn('distributors', 'fssite');
    }
}
