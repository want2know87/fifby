<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Items;

/**
 * ItemsSearch represents the model behind the search form about `app\models\Items`.
 */
class ItemsSearch extends Items
{
    public $q;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fiitem_id', 'fianother_customer', 'fitemplate_id', 'fiitem_catalog_id', 'fiorder'], 'integer'],
            [['fsitem_name', 'fsitem_old_desc', 'fsitem_picture', 'fsitem_small_desc', 'fsparams_array', 'fsitem_customer','q'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Items::find();

        if (isset($params['ItemsSearch']['q'])){
            $query = Items::find()->joinWith('mainCatalog',true,'RIGHT JOIN');
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'fiitem_id' => $this->fiitem_id,
            'fianother_customer' => $this->fianother_customer,
            'fitemplate_id' => $this->fitemplate_id,
            'fiitem_catalog_id' => $this->fiitem_catalog_id,
            'fiorder' => $this->fiorder,
        ]);

        $query->andFilterWhere(['like', 'fsitem_name', $this->fsitem_name])
            ->andFilterWhere(['like', 'fsitem_old_desc', $this->fsitem_old_desc])
            ->andFilterWhere(['like', 'fsitem_picture', $this->fsitem_picture])
            ->andFilterWhere(['like', 'fsitem_small_desc', $this->fsitem_small_desc])
            ->andFilterWhere(['like', 'fsparams_array', $this->fsparams_array])
            ->andFilterWhere(['like', 'fsitem_customer', $this->fsitem_customer])
            ->andFilterWhere(['like', 'fiorder', $this->fiorder]);

        $query->orFilterWhere(['like', 'items.fsitem_name', $this->q])
            ->orFilterWhere(['like', 'items.fsseo', $this->q])
            ->orFilterWhere(['like', 'items.fsitem_bottom_text', $this->q])
            ->orFilterWhere(['like', 'items.fsarticle', $this->q])
            ->orFilterWhere(['like', 'items.fsitem_small_desc', $this->q]);

        return $dataProvider;
    }
}
