<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SourceMessage */
/* @var $form yii\widgets\ActiveForm */

$msg_ru = '';
$msg_en = '';

if(!$model->isNewRecord) {
    $msg_ru = \app\models\Message::findOne(['id' => $model->id, 'language' => 'ru']);
    $msg_ru = $msg_ru->translation;
    $msg_en = \app\models\Message::findOne(['id' => $model->id, 'language' => 'en']);
    if (!$msg_en) {
        $msg_en = new \app\models\Message();
        $msg_en->language = 'en';
        $msg_en->id = $model->id;
        $msg_en->save();
    }
    $msg_en = $msg_en->translation;
}

?>

<div class="source-message-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'category')->hiddenInput(['value' => 'app'])->label(false) ?>

    <?= $form->field($model, 'message')->textInput() ?>

    <div class="form-group">
        <label>Текст RU</label>
    <?= Html::textarea('translate[ru]', $msg_ru, ['rows' => 6, 'class' => 'form-control']) ?>
    </div>

    <div class="form-group">
        <label>Текст EN</label>
    <?= Html::textarea('translate[en]', $msg_en, ['rows' => 6, 'class' => 'form-control']) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
