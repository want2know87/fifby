<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ParamsCat;

class ParamsCatSearch extends ParamsCat
{
    public function rules()
    {
        return [
            [['fsparam_name', 'fsparam_name_en'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = ParamsCat::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'fsparam_name', $this->fsparam_name])
              ->andFilterWhere(['like', 'fsparam_name_en', $this->fsparam_name_en]);

        return $dataProvider;
    }
}
