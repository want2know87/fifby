<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Управление адресами';
$this->params['admin'] = [
    'page_title' => $this->title,
    'main_title' => 'Управление адресами',
    'main_url' => '/mail/index'
];
?>
<div class="mail-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать подписку', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'fdcreate',
            'fsmail',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
