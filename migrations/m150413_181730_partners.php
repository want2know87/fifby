<?php

use yii\db\Schema;
use yii\db\Migration;

class m150413_181730_partners extends Migration
{
    public function up()
    {
        $this->createTable('partners', [
            'fipartner_id' => 'pk',
            'fspartner_name' => 'varchar(255) not null',
            'fspartner_image' => 'varchar(255)',
            'fspartner_link' => 'varchar(255)',
        ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');
    }

    public function down()
    {
        $this->dropTable('partners');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
