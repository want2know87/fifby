<?php

namespace app\controllers;

use Yii;
use app\models\Chpu;
use app\models\ChpuSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * ChpuController implements the CRUD actions for Chpu model.
 */
class ChpuController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
      $this->enableCsrfValidation = false;
      return parent::beforeAction($action);
      // $this->actionIndex();
    }

    /**
     * Lists all Chpu models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->layout = 'admin';
        $searchModel = new ChpuSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Chpu model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->layout = 'admin';
        $model = new Chpu();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Chpu model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $fsyii_link
     * @param string $fsurl
     * @return mixed
     */
    public function actionUpdate($fsyii_link, $fsurl)
    {
        $this->layout = 'admin';
        $model = $this->findModel($fsyii_link, $fsurl);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Chpu model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $fsyii_link
     * @param string $fsurl
     * @return mixed
     */
    public function actionDelete($fsyii_link, $fsurl)
    {
        $this->findModel($fsyii_link, $fsurl)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Chpu model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $fsyii_link
     * @param string $fsurl
     * @return Chpu the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($fsyii_link, $fsurl)
    {
        if (($model = Chpu::findOne(['fsyii_link' => $fsyii_link, 'fsurl' => $fsurl])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
