<?php

use yii\db\Migration;

class m170823_153610_add_en_to_distributors extends Migration
{
    public function safeUp()
    {
        $this->addColumn('distributors', 'fsname_en', $this->string());
        $this->addColumn('distributors_point', 'fsaddress_en', $this->string(512));
    }

    public function safeDown()
    {
        $this->dropColumn('distributors', 'fsname_en');
        $this->dropColumn('distributors_point', 'fsaddress_en');
    }
}
