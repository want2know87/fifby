<?php
use yii\helpers\Html;
use yii\data\ActiveDataProvider;

/* @var $this yii\web\View */
/* @var $model \app\models\News */

$this->title = $model->fstitle;

$years = Yii::$app->db->createCommand('SELECT DISTINCT YEAR(fdcreate_date) year FROM news WHERE lang="' . Yii::$app->language . '" order by 1 desc')->queryAll();

$this->registerCssFile('@web/js/ckeditor/contents.css');
$this->registerJsFile('@web/js/social-likes.min.js', ['depends' => \yii\web\JqueryAsset::className()]);
$this->registerCssFile('@web/js/social-likes_flat.css');

?>

<div class="breadcrumbs">
  <div class="inner_width">
    <a href="<?php echo \yii\helpers\Url::to(['site/index']) ?>"><i class="ichome"></i>Главная</a>
    <a href="<?php echo \yii\helpers\Url::to(['news/index']) ?>"></i>Новости</a>
    <span class="curr"><?php echo $model->fstitle ?></span>
  </div>
</div>
<div class="page_ttl mod">
  <div class="inner_width">
    <h1>Новости</h1>
  </div>
</div>
<div class="inner_width ">
  <ul class="aboutlist">
    <li class="act"><a href="#">Новости компании</a></li>
  </ul>
  <ul class="newsfilter">

    <?php foreach($years as $year): ?>
      <li><a href="<?php echo \yii\helpers\Url::to(['news/index', 'year' => $year['year']]) ?>" class="year <?php if((@$_GET['year'] == $year['year']) || count($years) == 1) echo " act" ?>">
          <?php echo $year['year'] ?>
        </a></li>
    <?php endforeach; ?>
  </ul>

  <div class="newswr">
    <div class="filterwrap mod">
        <div class="barinfo  hidden1040">
            <span class="big">Следите за новостями</span>
            Мы пишем только о том, что проверили на собственном опыте. Подпишитесь на обновления блога, чтобы не упускать полезные знания.
            <span class="subscribe_text"></span>
            <form class="subscr_form">
                <input type="text" name="mail" placeholder="Введите ваш email">
                <input type="submit" value="OK">
            </form>
        </div>
      <!--<div class="barinfo hidden1040">
        Сюда  можно будет
        повесить баннер партнера
        в обмен на баннер на его сайт
        или любой другой блок.
      </div>-->
    </div>

    <div class="catalog mod">
    <article class="news_single">
      <a href="<?php echo \yii\helpers\Url::to(['news/view','id' => $model->finewsid]) ?>" class="catalogcol_item_ttl"><?php echo $model->fstitle ?></a>
      <div class="news_item_date">
        <!--<div class="socialshare">
          <a href="#"><img alt="" src="images/icons/vk.png"></a>
          <a href="#"><img alt="" src="images/icons/fb.png"></a>
          <a href="#"><img alt="" src="images/icons/in.png"></a>
        </div>-->
        <?php echo date('d.m.Y', strtotime($model->fdcreate_date)) ?>
      </div>
      <p>
        <?php echo $model->fstext ?>
      </p>
    </article>

<?php

$another = \app\models\News::find()->andWhere(['lang' => Yii::$app->language])->orderBy('fdcreate_date desc')->limit(5)->all();

?>

    <div class="simiral">
      <b>Похожие статьи:</b>
      <ul class="newsfilter mod">
      <?php foreach($another as $news): ?>
        <li><a href="<?php echo \yii\helpers\Url::to(['news/view','id' => $news['finewsid']]) ?>"><?php echo $news['fstitle'] ?></a></li>
      <?php endforeach; ?>
      </ul>
    </div>
</div>
</div>
