<?php

use yii\db\Schema;
use yii\db\Migration;

class m150226_190739_insert_catalog extends Migration
{
    public function up()
    {
        $query = file_get_contents('migrations/fifby_struct.sql');
        $this->db->createCommand($query)->execute();
        $this->dropTable('users');
        foreach([
            'catalog',
            'catalog_items',
            'items',
            'links',
            'link_items',
            'params',
            'params_cat',
            'params_values',
            'templates',
            'templates_params',
                ] as $table) {
            $this->db->createCommand("ALTER TABLE {$table} ENGINE=InnoDB;")->execute();
        }
    }

    public function down()
    {
        $this->dropTable('catalog');
        $this->dropTable('catalog_items');
        $this->dropTable('items');
        $this->dropTable('links');
        $this->dropTable('link_items');
        $this->dropTable('params');
        $this->dropTable('params_cat');
        $this->dropTable('params_values');
        $this->dropTable('templates');
        $this->dropTable('templates_params');
        $this->dropTable('users');
    }
}
