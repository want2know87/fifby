<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "distributors_point_mail".
 *
 * @property integer $fidistrib_point_mail_id
 * @property integer $fidistr_point_id
 * @property string $fsmail
 *
 * @property DistributorsPoint $fidistrPoint
 */
class DistributorsPointMail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'distributors_point_mail';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fidistr_point_id'], 'required'],
            [['fidistr_point_id'], 'integer'],
            [['fsmail'], 'string', 'max' => 255],
            [['fidistr_point_id', 'fsmail'], 'unique', 'targetAttribute' => ['fidistr_point_id', 'fsmail'], 'message' => 'The combination of Fidistr Point ID and Fsmail has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fidistrib_point_mail_id' => Yii::t('app', 'Fidistrib Point Mail ID'),
            'fidistr_point_id' => Yii::t('app', 'Fidistr Point ID'),
            'fsmail' => Yii::t('app', 'Fsmail'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFidistrPoint()
    {
        return $this->hasOne(DistributorsPoint::className(), ['fidistr_point_id' => 'fidistr_point_id']);
    }
}
