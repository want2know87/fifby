<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "publication_type".
 *
 * @property integer $id
 * @property integer $key
 * @property integer $position
 */
class PublicationType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'publication_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['position'], 'required'],
            [['position', 'key'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'key' => 'Внутренний ключ',
            'position' => 'Приоритет',
        ];
    }
}
