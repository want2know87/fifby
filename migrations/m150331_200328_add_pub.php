<?php

use yii\db\Schema;
use yii\db\Migration;

class m150331_200328_add_pub extends Migration
{
    public function up()
    {
        $this->createTable('publications', [
            'fipublic_id' => 'pk',
            'fsname' => 'varchar(255) not null',
            'fsdesc' => 'text',
            'fitype' => 'int(1) default 0',
            'fsimage' => 'varchar(255)',
        ]);

        $this->createTable('publication_files', [
            'fipublic_file_id' => 'pk',
            'fipublic_id' => 'int(10) not null',
            'fsname' => 'varchar(255) not null',
            'fsfile' => 'varchar(255)',
        ]);

        $this->addForeignKey('fk_public_file', 'publication_files', 'fipublic_id', 'publications', 'fipublic_id');
    }

    public function down()
    {
        $this->dropForeignKey('fk_public_file', 'publication_files');
        $this->dropTable('publication_files');
        $this->dropTable('publications');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
