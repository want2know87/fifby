<?php

use yii\db\Migration;

class m170823_151813_add_en_to_country extends Migration
{
    public function safeUp()
    {
        $this->addColumn('country', 'fscountry_name_en', $this->string());
        $this->addColumn('town', 'fstown_name_en', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn('country', 'fscountry_name_en');
        $this->dropColumn('town', 'fstown_name_en');
    }
}
