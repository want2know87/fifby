<?php

use yii\db\Migration;

class m170824_093457_add_en_to_publications extends Migration
{
    public function safeUp()
    {
        $this->addColumn('publications', 'lang', $this->string(2)->defaultValue('ru'));
    }

    public function safeDown()
    {
        $this->dropColumn('publications', 'lang');
    }
}
