<?php

namespace app\models;

use Yii;
use app\models\traits\MultiLanguageTrait;
use yii2tech\ar\linkmany\LinkManyBehavior;

/**
 * This is the model class for table "items".
 *
 * @property string $fiitem_id
 * @property string $fsitem_name
 * @property string $fsitem_old_desc
 * @property string $fsitem_picture
 * @property string $fsitem_small_desc
 * @property string $fspassport
 * @property string $fsparams_array
 * @property integer $fianother_customer
 * @property string $fsitem_customer
 * @property integer $fitemplate_id
 * @property integer $fiitem_catalog_id
 * @property string $fsarticle
 * @property string $fsprice_rur
 * @property string $fsprice_usd
 * @property string $fsprice_byn
 * @property integer $fiorder
 * @property string $fdcreate_date
 * @property string $finew
 * @property string $fsseo
 * @property boolean $new_flag
 *
 * @property ItemsPictures[] $itemsPictures
 * @property ParamsValues[] $paramsValues
 * @property Items[] $similarItems
 */
class Items extends \yii\db\ActiveRecord
{
    use MultiLanguageTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'items';
    }

    public function behaviors()
    {
        return [
            'linkSimilarItemsBehavior' => [
                'class' => LinkManyBehavior::className(),
                'relation' => 'similarItems', // relation, which will be handled
                'relationReferenceAttribute' => 'similarItemIds', // virtual attribute, which is used for related records specification
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fsitem_name'], 'required'],
            [['fsitem_old_desc', 'fsitem_small_desc', 'fsparams_array', 'fsitem_before_text', 'fsseo', 'fsitem_bottom_text', 'fsitem_passport_text'], 'string'],
            [['fianother_customer', 'fitemplate_id', 'fiitem_catalog_id', 'fiorder', 'finew'], 'integer'],
            [['fsitem_name', 'fspassport', 'fsitem_picture', 'fsitem_customer'], 'string', 'max' => 255],
            [['fsarticle', 'fsprice_rur', 'fsprice_usd', 'fsprice_byn', 'fdcreate_date'], 'string', 'max' => 30],
            [['fsitem_name'], 'unique'],
            [['new_flag'], 'boolean'],
            [['fsitem_bottom_text_en', 'fsitem_small_desc_en','fsseo_en'], 'string'],
            ['similarItemIds', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fiitem_id' => Yii::t('app', 'Внутренний номер товара'),
            'fsitem_name' => Yii::t('app', 'Название товара'),
            'fsitem_old_desc' => Yii::t('app', 'Описание в старом формате'),
            'fsitem_picture' => Yii::t('app', 'Картинка товара'),
            'fsitem_small_desc' => Yii::t('app', 'Краткое описание'),
            'fsparams_array' => Yii::t('app', 'Fsparams Array'),
            'fianother_customer' => Yii::t('app', 'Признак конкурента'),
            'fsitem_customer' => Yii::t('app', 'Производитель'),
            'fitemplate_id' => Yii::t('app', 'Внутренний номер шаблона'),
            'fiitem_catalog_id' => Yii::t('app', 'Fiitem Catalog ID'),
            'fspassport' => Yii::t('app', 'fspassport'),
            'fsarticle' => Yii::t('app', 'Fsarticle'),
            'fsprice_rur' => Yii::t('app', 'Fsprice Rur'),
            'fsprice_usd' => Yii::t('app', 'Fsprice Usd'),
            'fsprice_byn' => Yii::t('app', 'Fsprice Byn'),
            'fiorder' => Yii::t('app', 'Fiorder'),
            'fsitem_before_text' => Yii::t('app', 'fsitem_before_text'),
            'fdcreate_date' => Yii::t('app', 'fdcreate_date'),
            'finew' => Yii::t('app', 'finew'),
            'fsseo' => Yii::t('app', 'fsseo'),
            'fisale_id' => Yii::t('app', 'Sales'),
            'fsitem_bottom_text' => Yii::t('app', 'fsitem_bottom_text'),
            'fsitem_passport_text' => Yii::t('app', 'Паспорт изделия (Описание)'),
            'fsprice_byn' => 'Цена BYN',
            'new_flag' => Yii::t('app', 'finew'),
            'fsitem_bottom_text_en' => Yii::t('app', 'fsitem_bottom_text') . ' En',
            'fsitem_small_desc_en' => Yii::t('app', 'Краткое описание') . ' En',
            'fsseo_en' => Yii::t('app', 'fsseo') . ' En',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParamsValues()
    {
        return $this->hasMany(ParamsValues::className(), ['fiitem_id' => 'fiitem_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMainCatalog()
    {
        return $this->hasOne(CatalogRu::className(), ['ficatalog_id' => 'ficatalog_id'])->viaTable('catalog_items', ['fiitem_id' => 'fiitem_id']);
    }

    public function getSimilarItems()
    {
        return $this
            ->hasMany(Items::className(), ['fiitem_id' => 'similar_item_id'])
            ->viaTable('{{%items__similar}}', ['item_id' => 'fiitem_id'])
        ;
    }

}
