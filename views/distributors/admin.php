<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Town;
use app\models\Region;
use app\models\Country;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DistributorsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'app.internal.distribs');
$this->params['admin'] = [
    'page_title' => $this->title,
];
$towns = Town::getAll();
$country = Country::getAll();
$page = (int)Yii::$app->request->get('page');

?>
<div class="distributors-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'app.internal.create-distrib'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'fitowns' => [
                'class' => \yii\grid\DataColumn::className(),
                'format' => 'html',
                'value' => function(\app\models\Distributors $model) use ($country) {
                    $town = Yii::$app->db->createCommand('select DISTINCT ficountry_id from town where town.fitown_id in (SELECT DISTINCT fitown_id FROM distributors_point WHERE fidistr_id = :distr)', [':distr' => $model->fidistr_id])->queryAll();
                    $show = '';
                    foreach($town as $t) {
                        $show .= ( isset( $country[ (int)$t['ficountry_id'] ] ) ? $country[ (int)$t['ficountry_id'] ] . ', ' : '');
                    }
                    $show = trim($show,', ');
                    return $show;
                }
            ],
            /*'fitown_id' => [
                'class' => \yii\grid\DataColumn::className(),
                'attribute' => 'fitown_id',
                'format' => 'html',
                'value' => function($model) use ($towns) {
                    return $towns[$model->fitown_id];
                }
            ],*/
            /*'ficountry_id' => [
                'class' => \yii\grid\DataColumn::className(),
                'attribute' => 'ficountry_id',
                'format' => 'html',
                'value' => function($model) use ($country) {
                    return $country[$model->ficountry_id];
                }
            ],*/
            /*'firegion_id' => [
                'class' => \yii\grid\DataColumn::className(),
                'attribute' => 'firegion_id',
                'format' => 'html',
                'value' => function($model) use ($regions) {
                    return $regions[$model->firegion_id];
                }
            ],*/
            'fsname',
            'priority',
            // 'fsaddress',
            // 'maps',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'urlCreator' => function($action, $model, $key, $index) use($page) {
                    $params = is_array($key) ? $key : ['id' => (string) $key];
                    $params[0] = $action;
                    $params += ['page' => $page];

                    return Url::toRoute($params);
                },
            ],
        ],
    ]); ?>

</div>
