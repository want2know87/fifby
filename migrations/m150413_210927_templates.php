<?php

use yii\db\Schema;
use yii\db\Migration;

class m150413_210927_templates extends Migration
{
    public function up()
    {
        $this->createTable('site_templates', [
            'fitemplate_id' => 'pk',
            'fstemplate_name' => 'varchar(255)',
            'fstitle' => 'varchar(255)',
            'fscode' => 'varchar(32)',
            'fstemplate_code' => 'text',
            'fstemplate_seo' => 'text',
        ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');
    }

    public function down()
    {
        $this->dropTable('site_templates');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
