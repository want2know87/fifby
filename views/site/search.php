<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ItemsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Поиск');

$prices = ['BYN', 'RUR', 'USD'];

?>
<div class="items-index search-results">
    <div class="page_ttl mod">
        <div class="inner_width">
            <h1><?= Html::encode($this->title) ?></h1>
        </div>
    </div>
    <div class="inner_width">

        <?= ListView::widget([
            'emptyText' => ($dataProvider->getModels() || $dataProvider2->getModels()) ? '' : Yii::t('yii', 'No results found.'),
            'dataProvider' => $dataProvider3,
            'layout' => "{items}\n",
            'itemView' => function ($row, $key, $index, $widget) use ($prices) {
                if ($row->ficatalog_id) return $this->render('catalog-search', ['item' => $row]);
            },

        ]); ?>

        <?= ListView::widget([
            'emptyText' => '',
            'dataProvider' => $dataProvider,
            'layout' => "{items}\n",
            'itemView' => function ($row, $key, $index, $widget) use ($prices) {
                if ($row->fiitem_id) return $this->render('item-search', ['item' => $row, 'prices' => $prices]);
            },

        ]); ?>

        <?= ListView::widget([
            'emptyText' => '',
            'dataProvider' => $dataProvider2,
            'layout' => "{items}\n",
            'itemView' => function ($row, $key, $index, $widget) use ($prices) {
                if ($row->finewsid) return $this->render('news-search', ['item' => $row]);
            },

        ]); ?>
    </div>
</div>

