<?php

use yii\db\Schema;
use yii\db\Migration;

class m150303_183930_manage_catalog extends Migration
{
    public function safeUp()
    {

        $this->db->createCommand('TRUNCATE catalog_items')->execute();

        $this->db->createCommand('delete from params_values where fiitem_id in (select fiitem_id from items where fianother_customer = 1)')->execute();
        $this->db->createCommand('delete from items where fianother_customer = 1')->execute();

        $this->db->createCommand('update params_values set fiparam_id = 7 where fiparam_id = 8')->execute();
        $this->db->createCommand('delete from params_cat where fiparam_id = 8')->execute();

        $this->db->createCommand('update params_values set fiparam_id = 4 where fiparam_id = 42')->execute();
        $this->db->createCommand('delete from params_cat where fiparam_id = 42')->execute();

        $this->db->createCommand('update params_values set fiparam_id = 59 where fiparam_id = 64')->execute();
        $this->db->createCommand('delete from params_cat where fiparam_id = 64')->execute();

        $this->db->createCommand('update params_values set fiparam_id = 5 where fiparam_id = 67')->execute();
        $this->db->createCommand('delete from params_cat where fiparam_id = 67')->execute();

        $this->db->createCommand('update params_values set fiparam_id = 178 where fiparam_id in (189,376,386)')->execute();
        $this->db->createCommand('delete from params_cat where fiparam_id in (189,376,386)')->execute();

        $this->db->createCommand('update params_values set fiparam_id = 263 where fiparam_id in (393)')->execute();
        $this->db->createCommand('delete from params_cat where fiparam_id in (393)')->execute();

        $this->db->createCommand('update params_values set fiparam_id = 181 where fiparam_id in (471)')->execute();
        $this->db->createCommand('delete from params_cat where fiparam_id in (471)')->execute();

        $this->db->createCommand('DELETE from params_values where fiparam_value_id in (386,387,388,389,390,391,392,393,394,395,396,397,398,399,400,401,402,403,404,405,902,903,904,905,906,907,908,909,910,911,912,913,914,915,916,917,918,919,920,921,922,923,924,925,926,927,1535,1536,1537,1538,1539,1540,1541,1542,1543,1544,2544,2545,2546,2547,2548,2549,2550,2551,2552,2553,2554,2555,2556,2557,2558,2559,2560,2561,2562,2563,2564,2565,2566,2567,2568,2569,2570,2571,2572,2573,2574,2575,2576,2577,2578,2579,2580,2581,2582,2583,2584,2585,2586,2587,2653,2654,2655,2656,2657,2658,2659,2660,2661,2662,2663,2664,2665,2666,2667,2668,2669,2670,2671,2672,2673,2674,2675,2676,2677,2678,2679,2680,2681,2682,2794,2795,2796,2797,2798,2799,2800,2801,2802,2917,2918,2919,2920,2921,2922,2923,2924,2925,2926,2927,2928,2929,2930,3201,3202,3203,3204,3205,3206,3207,3208,3209,3210,3211,3212,3213,3214,4081,4082,4083,4084,4085,4086,4087,4098,4099,4100,4101,4102,4103,4104,4105,4106,4107,4128,4129,4130,4131,4132,4133,4134,4135,4136,4137,4138,4139,4140,4141,4142,4143,4144,4145,4146,4147,4148,4149,4150,4151,4152,4153,4154,4155,4156,4157,4158,4159,4160,4161,4244,4245,4246,4247,4248,4249,4250,4251,4252,4253,4254,4255,4256,4257,4285,4286,4287,4288,4289,4290,4291,8348,8349,8350,8351,8352,8353,8354,8355,8356,8357,8358,8359,11808,11809,11810,11811,11812,11813,11814,11815,11816,11817,11818,11819,11820,11821,11822,11823,11824,15681,15682,15683,15684,15685,15686,15687,15688,15689,15690,22677,22678,22679,22680,22681,22682,22683,22684,22685,22686,22687,26887,26888,26889,26890,26891,26892,26893,26894,26895,26896,26897,26898,26899,26900,26901,26902,26903)')->execute();

        $this->db->createCommand("ALTER TABLE `params_values` MODIFY COLUMN `fiparam_id`  int(10) UNSIGNED NOT NULL COMMENT 'Ссылка на таблицу с именами параметров' AFTER `fiitem_id`")->execute();
        $this->db->createCommand("ALTER TABLE `params_values` MODIFY COLUMN `fiitem_id`  int(10) UNSIGNED NOT NULL COMMENT 'Ссылка на таблицу с продуктами' AFTER `fiparam_value_id`")->execute();

        $this->addForeignKey('fk_param_values', 'params_values', 'fiparam_id', 'params_cat', 'fiparam_id');
        $this->addForeignKey('fk_param_items', 'params_values', 'fiitem_id', 'items', 'fiitem_id');
        $this->addColumn('params_values','fiorder','int(10)');
        $this->createIndex('find_param', 'params_cat', 'fsparam_name', true);
        $this->createIndex('find_catalog', 'catalog_items', 'ficatalog_id');
        $this->createIndex('find_catalog_item', 'catalog_items', 'fiitem_id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_param_values', 'params_values');
        $this->dropForeignKey('fk_param_items', 'params_values');
    }
}
