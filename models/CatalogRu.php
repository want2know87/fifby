<?php

namespace app\models;

use Yii;
use app\models\traits\MultiLanguageTrait;

/**
 * This is the model class for table "catalog_ru".
 *
 * @property integer $ficatalog_id
 * @property string $fsname
 * @property integer $fiparent_catalog_id
 * @property string $fscatalog_img
 * @property string $fscatalog_text
 * @property integer $fiorder
 * @property string $fsicon
 * @property string $fsbigicon
 * @property string $fstemplate_code
 * @property string $fslink
 * @property string $fsdestination
 * @property string $fswork
 * @property string $fsuse
 * @property integer $fishow_general_desc
 * @property integer $fivisible
 * @property integer $fishow_with_items
 * @property integer $fscatalog_additional_test
 * @property integer $fsseo
 *
 * @property CatalogRu $fiparentCatalog
 * @property CatalogRu[] $catalogRus
 */
class CatalogRu extends \yii\db\ActiveRecord
{
    use MultiLanguageTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'catalog_ru';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fsname', 'fsname_en', 'fishow_with_items'], 'required'],
            [['fiparent_catalog_id', 'fiorder', 'fishow_general_desc', 'fivisible', 'fishow_with_items','to_menu'], 'integer'],
            [['fsname', 'fsname_en', 'fscatalog_img', 'fsicon', 'fsbigicon'], 'string', 'max' => 255],
            [['fscatalog_bottom_text'], 'string', 'max' => 512],
            [['fscatalog_text', 'fscatalog_text_en'], 'string', 'max' => 4096],
            [['fsdestination', 'fswork', 'fsuse', 'fscatalog_additional_test', 'fsseo', 'fsseo_en'], 'string', 'max' => 2048],
            [['fslink'], 'string', 'max' => 128],
            [['fstemplate_code'], 'string', 'max' => 32],
            [['fsname'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ficatalog_id' => Yii::t('app', 'Ficatalog ID'),
            'fsname' => Yii::t('app', 'catalog.Fsname'),
            'fsname_en' => Yii::t('app', 'catalog.Fsname').' En',
            'fiparent_catalog_id' => Yii::t('app', 'Fiparent Catalog ID'),
            'fscatalog_img' => Yii::t('app', 'Fscatalog Img'),
            'fscatalog_text' => Yii::t('app', 'Fscatalog Text'),
            'fscatalog_text_en' => Yii::t('app', 'Fscatalog Text').' En',
            'fscatalog_bottom_text' => Yii::t('app', 'Нижний текстовый блок'),
            'fiorder' => Yii::t('app', 'sort.order'),
            'fsicon' => Yii::t('app', 'Fsicon'),
            'fsbigicon' => Yii::t('app', 'Fsbigicon'),
            'fstemplate_code' => Yii::t('app', 'Fstemplate Code'),
            'fslink' => Yii::t('app', 'Fslink'),
            'fsdestination' => Yii::t('app', 'fsdestination'),
            'fswork' => Yii::t('app', 'fswork'),
            'fsuse' => Yii::t('app', 'fsuse'),
            'fishow_general_desc' => Yii::t('app', 'Fishow General Desc'),
            'fivisible' => Yii::t('app', 'fivisible'),
            'fishow_with_items' => Yii::t('app', 'fishow_with_items'),
            'to_menu' => Yii::t('app', 'to_menu'),
            'fscatalog_additional_test' => Yii::t('app', 'fscatalog_additional_test'),
            'fsseo' => Yii::t('app', 'fsseo'),
            'fsseo_en' => Yii::t('app', 'fsseo').' En',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFiparentCatalog()
    {
        return $this->hasOne(CatalogRu::className(), ['ficatalog_id' => 'fiparent_catalog_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatalogRus()
    {
        return $this->hasMany(CatalogRu::className(), ['fiparent_catalog_id' => 'ficatalog_id']);
    }
}
