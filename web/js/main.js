﻿$(document).ready(function(){

    $(document).find('.tovar_price div.sel select').each(function() {
       $(this).attr('disabled', 'disabled');
    });

    $("#site-search").focus(function() {
        //$("#site-search").css('width', '640px');
        // 2016-11-22
        $("#site-search").css('width', '480px');
        $(".menu_search:not(.modtog) input:text").addClass('focus');
        $(".menu_search:not(.modtog) span").attr("onClick","search_form.submit()").show();
        $(".menu_search:not(.modtog) input:submit").attr("onClick","search_form.submit()");
    });

    $("#site-search").focusout(function () {
        $(".menu_search:not(.modtog) span").hide(); 
        $("#site-search").addClass('unfocus');
        $("#site-search").css('width', '0px');
        window.setTimeout(function () {
          $("#site-search").removeClass('focus');
        }, 200);
        
        $("#site-search").removeClass('unfocus');
        
    });

    $('#show-search-mini').click(function(){
      $('.modtog').toggle('slow');
    });

    var priceChangeSelect = function() {
        var self = $(this);
        window.setTimeout(function() {
           self.closest('.tovar_price').find('.pr').text($('option:selected', self).data('price'));
          
        });
        
    };
    var priceChange = function() {
        var self = $(this);
        self.closest('.tovar_price').find('.pr').text(self.data('price'));
    };
    var falseFunction = function() {
        return false;
    }
    $(document).on('click', '.tovar_price div.sel select option', falseFunction);
    $(document).on('change', '.tovar_price div.sel select', falseFunction);
    $( document ).ajaxSuccess(function( event, xhr, settings ) {
       $(document).find('.tovar_price div.sel select').each(function() {
       $(this).attr('disabled', 'disabled');
    });
    });
    //$(document).on('click', '.tovar_price div.sel select option', priceChange);
    //$(document).on('change', '.tovar_price div.sel select', priceChangeSelect);

    //$(document).on('click', '.tovar_price div.sel select option', function() {
    //    var self = $(this);
    //    self.closest('.tovar_price').find('.pr').text(self.data('price'));
    //});
    $('.munubtn').click(function(){
        $('.menuline').slideToggle(600);
    }); 
    /*for search*/
    $('.searchbtn').click(function(){
        $('#js-search').slideToggle(600);
    }); 
    /*for support*/
    $('#js-support').click(function(){
        $('#js-support > .support').slideToggle(600);
    });
    /*for subscribe*/            
    $('.jsfancy').fancybox({
        'padding': 0
    }); 
    /*for  menu2  для моб*/
    $('#js-menu, #js-menu2').click(function(){
        if($(window).width()<1180){
            $(this).next().slideToggle(600);
            $(this).parent().toggleClass('open');
            return false;
        }
    });
});

