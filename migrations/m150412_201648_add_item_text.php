<?php

use yii\db\Schema;
use yii\db\Migration;

class m150412_201648_add_item_text extends Migration
{
    public function up()
    {
        $this->addColumn('items', 'fsitem_before_text', 'text');
        $this->addColumn('items', 'fsitem_passport_text', 'text');
        $this->addColumn('catalog_ru', 'fscatalog_bottom_text', 'text');
    }

    public function down()
    {
        $this->dropColumn('catalog_ru', 'fscatalog_bottom_text');
        $this->dropColumn('items', 'fsitem_passport_text');
        $this->dropColumn('items', 'fsitem_before_text');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
