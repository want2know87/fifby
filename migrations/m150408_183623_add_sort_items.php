<?php

use yii\db\Schema;
use yii\db\Migration;

class m150408_183623_add_sort_items extends Migration
{
    public function up()
    {
        $this->addColumn('items', 'fiorder', 'int(10) not null default 100');
    }

    public function down()
    {
        $this->dropColumn('items', 'fiorder');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
