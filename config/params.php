<?php

$localParams = is_file(__DIR__ . '/params-local.php')
    ? require(__DIR__ . '/params-local.php')
    : [];

return array_merge([
     'adminEmail' => 'region1@fif.by',
    //'adminEmail' => 'sl444yer@gmail.com',
    //'adminEmail' => 'xander.sytsevich@gmail.com',
    'googleMapsLanguage' => 'ru',
    'googleMapsLibraries' => 'places',
    'fiftestHost' => 'https://fif.by',
], $localParams);
