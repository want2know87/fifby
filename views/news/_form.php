<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\News */
/* @var $form yii\widgets\ActiveForm */


$this->registerJsFile('@web/js/ckeditor/ckeditor.js');
$this->registerJs("
        CKEDITOR.replace('news-fstext',
            {
                height  : '400px',
                filebrowserUploadUrl: \"" . \yii\helpers\Url::to(['upload/image']) . "\",
                on: {
                instanceReady:function( ev ){
                   jQuery(CKEDITOR.instances['news-fstext'].container.$).mouseleave(function() {
                      CKEDITOR.instances['news-fstext'].updateElement();
                   });
                   CKEDITOR.instances['news-fstext'].on('blur', function() {
                      CKEDITOR.instances['news-fstext'].updateElement();
                   });
                }
             }
            });
        $('#news-fstext').on('change', function() {
          CKEDITOR.instances['news-fstext'].setData($(this).val());
        });
            ");

?>

<div class="news-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>

    <?= $form->field($model, 'lang')->dropDownList(['ru' => 'RU', 'en' => 'EN']) ?>

    <?= $form->field($model, 'fstitle')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fsanons_general')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'fsanons')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'fstext')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'fsseo')->textarea(['rows' => 4]) ?>

    <?= $form->field($model, 'fdtitle_image')->fileInput() ?>

    <?php if($model->fdtitle_image): ?>
      <?php echo Html::img(Yii::$app->request->baseUrl . '/' . $model->fdtitle_image, ['style' => 'max-width:400px;']) ?>
    <?php endif; ?>

    <?= $form->field($model, 'fsimage_news')->fileInput() ?>

    <?php if($model->fsimage_news): ?>
      <?php echo Html::img(Yii::$app->request->baseUrl . '/' . $model->fsimage_news, ['style' => 'max-width:400px;']) ?>
    <?php endif; ?>

    <?php // echo $form->field($model, 'fdcreate_date')->textInput() ?>
    <br/><br/>

    <?php echo $form->field($model, 'fdcreate_date')->widget(\yii\jui\DatePicker::className(), ['language' => 'ru', 'dateFormat' => 'yyyy-MM-dd',]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
