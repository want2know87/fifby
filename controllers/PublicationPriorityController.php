<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use app\models\Publications;
use app\models\PublicationFiles;

class PublicationPriorityController extends \yii\web\Controller
{
    public $layout = 'admin';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionAdmin($type = 0)
    {
        $query = PublicationFiles::find()
            ->innerJoinWith(['fipublic'])
            ->andWhere(['fitype' => $type])
            ->orderBy('priority DESC');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $this->render('admin', [
            'dataProvider' => $dataProvider,
        ]);
    }
}
