<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "slider".
 *
 * @property string $fislide_id
 * @property string $fsslide_picture
 * @property string $fsslide_button_name
 * @property string $fsslide_title
 * @property integer $fislide_visible
 * @property string $fsslide_link
 */
class Slider extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'slider';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fislide_visible'], 'integer'],
            [['fsslide_picture', 'fsslide_title', 'fsslide_link'], 'string', 'max' => 300],
            [['fsslide_button_name'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fislide_id' => 'Внутренний номер',
            'fsslide_picture' => 'Картинка слайда',
            'fsslide_button_name' => 'Название кнопки',
            'fsslide_title' => 'Текст на слайде',
            'fislide_visible' => 'включен',
            'fsslide_link' => 'Ссылка',
        ];
    }
}
