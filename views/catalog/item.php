<?php
use yii\helpers\Html;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use \app\models\CatalogItems;
use \app\models\CatalogRu;

$catSearch = [];
$general = false;
if(isset($page) && $page == 'general') {
    $general = true;
    $catalogs = array_map(function($el) { return $el['items']; }, $cats);
    foreach($catalogs as $cat) {
        $catSearch = array_merge($catSearch, $cat);
    }
    $catalogs = ArrayHelper::map($catSearch, 'id', 'name');
    //$catslogs =
}

/** var array $item */
?>
<div class="item col-xs-12 item-one padding-10 <?php if($item['fisale_id']) echo "sale"?>" data-id="<?php echo $item['fiitem_id'] ?>">
    <div class="title col-xs-8 padding-none">
        <?php echo $item->fsitem_name ?>
    </div>
    <div class="new col-xs-4 padding-none">
      <?php if((int)$item['finew']): ?>
        <span class="new">Новинка</span>
      <?php endif; ?>
    </div>
    <?php if($general): ?>
      <div class="col-xs-12 padding-none cat-link">
          <?php
          $catItem = CatalogItems::find()->where(['fiitem_id' => $item['fiitem_id']])->one();
          if(!isset($catalogs[$catItem['ficatalog_id']])) {
              $catalog = CatalogRu::find()->where(['ficatalog_id' => $catItem['ficatalog_id']])->one();
              $catItem = CatalogRu::find()->where(['ficatalog_id' => $catalog['fiparent_catalog_id']])->one();
          }
          ?>
          <?php echo Html::a(@$catalogs[$catItem['ficatalog_id']], Url::to(['catalog/index','cat' => $catItem['ficatalog_id']])); ?>
      </div>
    <?php endif; ?>
    <div class="col-xs-12 padding-none">
        <div class="img col-xs-5 padding-none">
            <?php $image = file_exists(Yii::$app->basePath . '/web/' . $item['fsitem_picture'] . '.small.jpg') ?
                    Yii::$app->request->baseUrl . '/' . $item['fsitem_picture'] . '.small.jpg' :
                    Yii::$app->request->baseUrl . '/' . $item['fsitem_picture'];
                    ?>
            <?php echo Html::img($image, ['style' => 'max-width:110px;max-height:160px;']) ?>
            <?php // echo Html::img(Yii::$app->request->baseUrl . '/' . $item['fsitem_picture'], ['style' => 'max-width:110px;max-height:160px;']) ?>
        </div>
        <div class="desc col-xs-7 padding-none shadow-text" data-toggle="tooltip" data-placement="top" title="<?php echo $item->fsitem_small_desc ?>">
            <div class="description">
                <?php echo $item->fsitem_small_desc ?>
            </div>
        </div>
    </div>
    <div class="price col-xs-12 padding-none">
        <div class="col-xs-8 padding-none price-cell">
            <?php
            $itemPrice = [];
            foreach($prices as $name) {
                if(!empty($item['fsprice_' . strtolower($name)]) && strlen($item['fsprice_' . strtolower($name)])) {
                    $itemPrice[$name] = $item['fsprice_' . strtolower($name)];
                }
            }
            if(!empty($itemPrice)) {
                echo '<span class="price-name">'.Yii::t('app', 'Цена').': </span>';
                $priceNameDef = '';
                foreach($itemPrice as $name => $pr) {
                    echo '<span class="number">',$pr,'</span>';
                    $priceNameDef = $name;
                    break;
                }
                $items = [];
                foreach($itemPrice as $name => $pr) {
                    $items[] = [
                        'label' => $name,
                        'options' => [
                            'data-price' => $pr,
                        ],
                    ];
                }
                echo ButtonDropdown::widget ( [
                    'label' => $priceNameDef,
                    'options' => [
                        'class' => 'btn-lg btn-default',
                    ],
                    'dropdown' => [
                        'items' => $items,
                    ],
                ] );

            }
            ?>
        </div>
        <div class="col-xs-4 padding-none"><span class="where"><a href="<?php echo \yii\helpers\Url::to(['distributors/index']) ?>"><?=Yii::t('app', 'Где купить')?></a></span></div>
    </div>
    <div class="show-more-wrap">
        <div class="show-more padding-10 col-xs-12 padding-none">
            <a href="<?php echo \yii\helpers\Url::to(['catalog/get-item', 'id' => $item['fiitem_id']]) ?>">
                <?=Yii::t('app', 'Подробное техническое описание')?>
            </a>
        </div>
    </div>
</div>
