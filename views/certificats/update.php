<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Sertificats */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Sertificats',
]) . ' ' . $model->fisert_id;
$this->params['admin'] = [
    'page_title' => $this->title,
    'main_title' => 'Сертификаты',
    'main_url' => 'certificats/admin'
];
?>
<div class="sertificats-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
