<?php

namespace app\controllers;

use app\models\Slider;
use app\models\CatalogSearchRu;
use app\models\NewsSearch;
use Yii;
use yii\base\Exception;
use yii\filters\AccessControl;
use yii\validators\EmailValidator;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\ItemsSearch;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post', 'get'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        $slider = Slider::find()->where('fislide_visible = 1')->all();
        return $this->render('index', ['slider' => $slider]);
    }

    public function actionDownloads() {
        if(Yii::$app->request->get('type') !== null) {
            $type = (int) Yii::$app->request->get('type');
            if ($type == 1)
                $this->redirect('/cert');
            return $this->render('downloads-media', [ 'type' => $type]);
        }
        else {
            return $this->render('downloads');
        }
    }

    public function actionCooperation()
    {
        $result = false;
        if(Yii::$app->request->isPost) {

            $body = '';
            $body .= 'Имя: ' . "\t" . Yii::$app->request->post('name') . "\n";
            $body .= 'Телефон: ' . "\t" . Yii::$app->request->post('phone') . "\n";
            $body .= 'Страна: ' . "\t" . Yii::$app->request->post('country') . "\n";
            $body .= 'Город: ' . "\t" . Yii::$app->request->post('town') . "\n";
            $body .= 'Email: ' . "\t" . Yii::$app->request->post('mail') . "\n";
            $body .= 'Комментарий: ' . "\t" . Yii::$app->request->post('comment') . "\n";

            try {
                $result = Yii::$app->mailer->compose()
                    ->setTo('region1@fif.by')
                    ->setFrom([Yii::$app->request->post('mail') => Yii::$app->request->post('name')])
                    ->setSubject('Заявка на ' . (Yii::$app->request->post('type') == 'diller' ? 'дилера' : 'инсталлятора'))
                    ->setTextBody($body)
                    ->send();
            }
            catch(Exception $ex) {
                $result = false;
            }

        }
        return $this->render('cooperation', ['result' => $result]);
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
        // $this->actionIndex();
    }

    public function actionCertificats() {
        return $this->render('sertification');
    }

    public function actionMail() {
        $mail = Yii::$app->request->post('data');
        if(empty($mail)) {
            echo "Введен пустой адрес";
            Yii::$app->response->statusCode = 500;
            return false;
        }
        else {
            $mail = @$mail[0]['value'];
            if(empty($mail)) {
                echo "Введен пустой адрес";
                Yii::$app->response->statusCode = 500;
                return false;
            }
            $mail = trim($mail);
            $validator = new EmailValidator();
            if($validator->validate($mail)) {
                $res = Yii::$app->db->createCommand('SELECT 1 FROM mail WHERE fsmail = :mail', [':mail' => $mail])->queryAll();
                if(empty($res)) {
                    Yii::$app->db->createCommand('INSERT INTO mail(fsmail) VALUES(:mail)', [':mail' => $mail])->execute();
                    Yii::$app->mailer->compose()
                        ->setTo($mail)
                        ->setFrom([Yii::$app->params['adminEmail'] => 'Евроавтоматика ФиФ'])
                        ->setSubject('Подписка на новости Евроавтоматика ФиФ')
                        ->setHtmlBody(<<<BODY
<p>Уважаемый клиент! Вы подписались на новостную рассылку "Евроавтоматика ФиФ". Вас ждут новинки, интересные способы применения и обзоры продукции!</p>
<p>Письмо сгенерировано автоматически сайтом www.fif.by</p>
<p>При возникновении вопросов напишите нам: region1@fif.by</p>
<hr>
С уважением, команда СООО "Евроавтоматика ФиФ"
BODY
                        )
                        ->send();
                }
            }
            else {
                echo "Неверный формат адреса email";
                Yii::$app->response->statusCode = 500;
                return false;
            }
        }
        echo 'Спасибо, ваша подписка успешно оформлена';
    }

    public function actionGetcats() {
        $id = (int)Yii::$app->request->post('cat');
        echo $this->renderAjax('getitems', ['id' => $id]);
        //echo $id;
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionContact()
    {
        return $this->render('contact');
    }

    public function actionSearch($query, $ajax = false)
    {
        $searchModel = new ItemsSearch();
        @$s['ItemsSearch']['q']=$query;
        $dataProvider = $searchModel->search($s);

        $searchModel2 = new NewsSearch();
        @$s['NewsSearch']['q']=$query;
        $dataProvider2 = $searchModel2->search($s);

        $searchModel3 = new CatalogSearchRu();
        @$s['CatalogSearchRu']['q']=$query;
        $dataProvider3 = $searchModel3->search($s);
        if($ajax) {
            $response=[];
            $limit = 10;
            $i = 0;
            if (($search=$dataProvider->getModels())!==null)
                foreach ($search as $row) {
                    $i++;
                    if ($i > $limit) break;
                    $response[]=[
                        'link'=>\yii\helpers\Url::to(['catalog/items', 'id' => $row['fiitem_id']]),
                        'label'=>$row['fsitem_name'],
                        'value'=>$row['fsitem_name'],
                        'query'=>$query,
                    ];
                }
            return json_encode($response);
        } else {
            return $this->render('search',[
                'searchModel'=>$searchModel,
                'dataProvider'=>$dataProvider,
                'dataProvider2'=>$dataProvider2,
                'dataProvider3'=>$dataProvider3,
            ]);
        }
    }
}
