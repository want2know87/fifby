<?php

namespace app\controllers;

use Yii;
use app\models\Sertificats;
use app\models\SertificatsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;

/**
 * SertificatsController implements the CRUD actions for Sertificats model.
 */
class CertificatsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['admin', 'create', 'update', 'delete', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Sertificats models.
     * @return mixed
     */
    public function actionAdmin()
    {
        $this->layout = 'admin';
        $searchModel = new SertificatsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Sertificats model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->layout = 'admin';
        $model = new Sertificats();
        $file = UploadedFile::getInstanceByName('Sertificats[fssert_img]');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if($file) {
                $file->saveAs(dirname(__FILE__).'/../web/data/cert/item_' . $model->fisert_id . '.' . $file->extension);
                $model->fssert_img = 'data/cert/item_' . $model->fisert_id . '.' . $file->extension;
                $model->save();
            }
            return $this->redirect(['admin']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Sertificats model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $this->layout = 'admin';
        $model = $this->findModel($id);
        $oldImage = $model->fssert_img;
        $file = UploadedFile::getInstanceByName('Sertificats[fssert_img]');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if(empty($file)) {
                $model->fssert_img = $oldImage;
            }
            else {
                if(file_exists(dirname(__FILE__).'/../web/data/cert/item_' . $model->fisert_id . '.' . $file->extension)) {
                    unlink(dirname(__FILE__).'/../web/data/cert/item_' . $model->fisert_id . '.' . $file->extension);
                }
                $file->saveAs(dirname(__FILE__).'/../web/data/cert/item_' . $model->fisert_id . '.' . $file->extension);
                $model->fssert_img = 'data/cert/item_' . $model->fisert_id . '.' . $file->extension;
            }
            $model->save();
            return $this->redirect(['admin']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Sertificats model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['admin']);
    }

    /**
     * Finds the Sertificats model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Sertificats the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Sertificats::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
