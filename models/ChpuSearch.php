<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Chpu;

/**
 * ChpuSearch represents the model behind the search form about `app\models\Chpu`.
 */
class ChpuSearch extends Chpu
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fsyii_link', 'fsurl'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Chpu::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'fsyii_link', $this->fsyii_link])
            ->andFilterWhere(['like', 'fsurl', $this->fsurl]);

        return $dataProvider;
    }
}
