<div class="breadcrumbs">
    <div class="inner_width">
        <a href="<?php echo \yii\helpers\Url::to(['/']) ?>"><i class="ichome"></i>Главная</a>
    </div>
</div>
<div class="page_ttl">
    <div class="inner_width">
        <h1>Страница не найдена</h1>
    </div>
</div>
<div class="inner_width center">
    <div class="mess404">404 <span>Уважаемый  пользователь,  к  сожалению страница  на  которую вы  ссылались  не найдена</span></div>
    <a class="catalog_item_more big" href="<?php echo \yii\helpers\Url::to(['/']) ?>">Скорее домой!</a>
</div>