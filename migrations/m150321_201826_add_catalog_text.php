<?php

use yii\db\Schema;
use yii\db\Migration;

class m150321_201826_add_catalog_text extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn('catalog_ru', 'fsdestination', 'text');
        $this->addColumn('catalog_ru', 'fswork', 'text');
        $this->addColumn('catalog_ru', 'fsuse', 'text');
    }
    
    public function safeDown()
    {
        $this->dropColumn('catalog_ru', 'fsdestination');
        $this->dropColumn('catalog_ru', 'fswork');
        $this->dropColumn('catalog_ru', 'fsuse');
    }

}
