<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "distributors_mail".
 *
 * @property integer $fidistrib_mail_id
 * @property integer $fidistr_id
 * @property string $fsmail
 *
 * @property Distributors $fidistr
 */
class DistributorsMail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'distributors_mail';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fidistr_id'], 'required'],
            [['fidistr_id'], 'integer'],
            [['fsmail'], 'string', 'max' => 255],
            [['fidistr_id', 'fsmail'], 'unique', 'targetAttribute' => ['fidistr_id', 'fsmail'], 'message' => 'The combination of Fidistr ID and Fsmail has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fidistrib_mail_id' => Yii::t('app', 'Fidistrib Mail ID'),
            'fidistr_id' => Yii::t('app', 'Fidistr ID'),
            'fsmail' => Yii::t('app', 'Fsmail'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFidistr()
    {
        return $this->hasOne(Distributors::className(), ['fidistr_id' => 'fidistr_id']);
    }
}
