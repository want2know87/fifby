<?php

namespace app\models;

use Yii;
use app\models\traits\MultiLanguageTrait;

/**
 * This is the model class for table "site_templates".
 *
 * @property integer $fitemplate_id
 * @property string $fstemplate_name
 * @property string $fscode
 * @property string $fstemplate_code
 * @property string $fstemplate_seo
 * @property string $fstitle
 */
class SiteTemplates extends \yii\db\ActiveRecord
{
    use MultiLanguageTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'site_templates';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fstemplate_code', 'fstemplate_code_en', 'fstemplate_seo', 'fstemplate_seo_en'], 'string'],
            [['fstemplate_name', 'fstitle', 'fstitle_en'], 'string', 'max' => 255],
            [['fscode'], 'string', 'max' => 32]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fitemplate_id' => Yii::t('app', 'Fitemplate ID'),
            'fstemplate_name' => Yii::t('app', 'Fstemplate Name'),
            'fscode' => Yii::t('app', 'Fscode'),
            'fstemplate_code' => Yii::t('app', 'Fstemplate Code'),
            'fstemplate_code_en' => 'Код шаблона EN',
            'fstemplate_seo' => Yii::t('app', 'Fstemplate Seo'),
            'fstemplate_seo_en' => Yii::t('app', 'Fstemplate Seo').' En',
            'fstitle' => Yii::t('app', 'fstitle'),
            'fstitle_en' => 'Title шаблона EN',
        ];
    }
}
