<?php

namespace app\controllers;

use app\models\CatalogItems;
use app\models\CatalogTemplates;
use app\models\Items;
use Yii;
use app\models\CatalogRu;
use app\models\CatalogSearchRu;
use yii\base\Exception;
use yii\web\Controller;
use yii\web\Cookie;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;

/**
 * CatalogController implements the CRUD actions for CatalogRu model.
 */
class CatalogController extends Controller
{
    public $defaultAction='no';

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['admin', 'create', 'update', 'update-parent', 'update-order', 'delete', 'index', 'get-item', 'view', 'items'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['index', 'get-item', 'view', 'items'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) {
            return false;
        }
        if (in_array($action->id, ['admin', 'create', 'update'])) {
            Yii::$app->language = 'ru';
        }
        return true;
    }

    /**
     * Lists all CatalogRu models.
     * @return mixed
     */
    public function actionAdmin()
    {
        $this->layout = 'admin';
        $searchModel = new CatalogSearchRu();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('admin', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionItems() {
        $fsnameField = Yii::$app->language == 'en' ? 'fsname_en' : 'fsname';
        $id = Yii::$app->request->get('id');
        /** @var Items $model */
        $model = Items::find()->where(['fiitem_id' => (int)$id])->one();
        if(empty($model)) {
           throw new Exception('Can\'t find item');
        }
        $catalogItem = CatalogItems::findOne(['fiitem_id' => (int)$model->fiitem_id]);
        $catalog = $this->findModel($catalogItem->ficatalog_id);

	$cat = ((int)$catalog->fivisible ? (int)$catalog->ficatalog_id : (int)$catalog->fiparent_catalog_id);
        $catalogs = Yii::$app->db->createCommand("SELECT
                                                c3.ficatalog_id as general_id,
                                              c3.{$fsnameField} as general_name,
                                                c2.ficatalog_id as middle_id,
                                              c2.{$fsnameField} as middle_name,
                                                c1.ficatalog_id as low_id,
                                              c1.{$fsnameField} as low_name
                                            FROM
                                                catalog_ru c1
                                            LEFT JOIN catalog_ru c2 ON c2.ficatalog_id = c1.fiparent_catalog_id
                                            LEFT JOIN catalog_ru c3 ON c3.ficatalog_id = c2.fiparent_catalog_id
                                            WHERE
                                                c1.ficatalog_id = :catalogid",['catalogid' => (int)$cat])->queryOne();

        return $this->render('one-view.php', [
          'item' => $model,
          'catalog' => $catalog,
          'breadCrumbs' => $catalogs,
        ]);
    }

    /**
     * Lists all CatalogRu models.
     * @return mixed
     */
    public function actionIndex()
    {
        $fsnameField = Yii::$app->language == 'en' ? 'fsname_en' : 'fsname';
        $viewTemplate = '';
        $searchModel = new CatalogSearchRu();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $catalog = Yii::$app->request->get('cat');
        if(empty($catalog)) {
            return $this->render('level0');
            $catalog = Yii::$app->db->createCommand('SELECT ficatalog_id from catalog_ru where fiparent_Catalog_id is null or fiparent_catalog_id = "" limit 1')->queryOne();
            $catalog = (int) $catalog['ficatalog_id'];
        }

        $catalogs = Yii::$app->db->createCommand("SELECT
                                                c3.ficatalog_id as general_id,
                                              c3.{$fsnameField} as general_name,
                                                c2.ficatalog_id as middle_id,
                                              c2.{$fsnameField} as middle_name,
                                                c1.ficatalog_id as low_id,
                                              c1.{$fsnameField} as low_name
                                            FROM
                                                catalog_ru c1
                                            LEFT JOIN catalog_ru c2 ON c2.ficatalog_id = c1.fiparent_catalog_id
                                            LEFT JOIN catalog_ru c3 ON c3.ficatalog_id = c2.fiparent_catalog_id
                                            WHERE
                                                c1.ficatalog_id = :catalogid",['catalogid' => (int)$catalog])->queryOne();

        $level = 0;
        if(!empty($catalogs['general_id']) && !empty($catalogs['middle_id'])) {
            $level = 2;
        }
        elseif(!empty($catalogs['middle_id']) && empty($catalogs['general_id'])) {
            $level = 1;
        }

        $podcats = Yii::$app->db->createCommand('SELECT count(*) as `count` from catalog_ru
                                            WHERE
                                                fiparent_catalog_id = :catalogid AND fishow_with_items <> 1',['catalogid' => (int)$catalog])->queryOne();

        if(!$podcats['count']) {
            $level = 2;
        }

        $current = CatalogRu::find()->where(['ficatalog_id' => (int)$catalog])->andWhere(['fivisible' => 1])->one();

        if($current) {
          $this->view->seoFields = @$current->fsseo;
        }

        $params = [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
            'level'        => $level,
            'breadCrumbs'  => $catalogs,
            'subcat'       => $catalog,
        ];

        if(!empty($current->fstemplate_code)) {
            $template = CatalogTemplates::find()->where(['fstemplate_code' => $current->fstemplate_code])->one();
            if(!empty($template)) {
                $params = array_merge($params, ['template' => $template->fstemplate_text]);
                return $this->render('custom_template', $params);
            }
        }

        if(1/*Yii::$app->request->get('view') == 'new' || Yii::$app->request->cookies->has('view')*/) {
            if($level == 2) {
                $viewTemplate = 'new-';
            }
            // Yii::$app->response->cookies->add(new Cookie(['name' => 'view', 'value' => 'new']));

            if($catalog && !$current) {
                $current = CatalogRu::find()->where(['ficatalog_id' => (int)$catalog])->one();
                if(!empty($current)) {
                    $current = CatalogRu::find()->where(['ficatalog_id' => (int)$current->fiparent_catalog_id])->one();
                }
                $this->view->seoFields = @$current->fsseo;
                $params['subcat'] = (int)$current->ficatalog_id;
                $catalogs = Yii::$app->db->createCommand("SELECT
                                                c3.ficatalog_id as general_id,
                                              c3.{$fsnameField} as general_name,
                                                c2.ficatalog_id as middle_id,
                                              c2.{$fsnameField} as middle_name,
                                                c1.ficatalog_id as low_id,
                                              c1.{$fsnameField} as low_name
                                            FROM
                                                catalog_ru c1
                                            LEFT JOIN catalog_ru c2 ON c2.ficatalog_id = c1.fiparent_catalog_id
                                            LEFT JOIN catalog_ru c3 ON c3.ficatalog_id = c2.fiparent_catalog_id
                                            WHERE
                                                c1.ficatalog_id = :catalogid",['catalogid' => (int)$current->ficatalog_id])->queryOne();
                $params['breadCrumbs'] = $catalogs;
            }
        }

        if(Yii::$app->request->get('view') == 'old') {
            Yii::$app->response->cookies->remove('view');
            $viewTemplate = '';
        }
        return $this->render('catalog-index-level-' . $viewTemplate . (int) $level, $params);
    }

     public function actionGetItem() {
         $id = Yii::$app->request->post('id', (int)Yii::$app->request->get('id'));
	return $this->redirect(['catalog/items', 'id' => (int) $id]);
        $params = Yii::$app->db->createCommand('SELECT
                                                    pc.fsparam_name as `name`,
                                                  pv.fsparam_value as `value`
                                                FROM
                                                    params_values pv
                                                LEFT JOIN params_cat pc ON pv.fiparam_id = pc.fiparam_id
                                                WHERE
                                                    pv.fiitem_id = :fiitemid
                                                ORDER BY
                                                    (CASE WHEN pv.fiorder IS NULL then 99999 ELSE pv.fiorder END)', ['fiitemid' => (int)$id])->queryAll();
        $itemInfo = Items::find()->where(['fiitem_id' => (int) $id])->one();
        if(Yii::$app->request->isAjax) {
            return $this->renderAjax('item-props-ajax', ['params' => $params, 'itemInfo' => $itemInfo]);
        }
        else {
            $catalog = Yii::$app->db->createCommand('select ficatalog_id from catalog_items where fiitem_id = :cat limit 1', ['cat' => (int)$id])->queryOne();
            $catalogs = Yii::$app->db->createCommand('SELECT
                                                c3.ficatalog_id as general_id,
                                              c3.fsname as general_name,
                                                c2.ficatalog_id as middle_id,
                                              c2.fsname as middle_name,
                                                c1.ficatalog_id as low_id,
                                              c1.fsname as low_name
                                            FROM
                                                catalog_ru c1
                                            LEFT JOIN catalog_ru c2 ON c2.ficatalog_id = c1.fiparent_catalog_id
                                            LEFT JOIN catalog_ru c3 ON c3.ficatalog_id = c2.fiparent_catalog_id
                                            WHERE
                                                c1.ficatalog_id = :catalogid',['catalogid' => (int)$catalog['ficatalog_id']])->queryOne();

            return $this->render('item-props', ['params' => $params, 'itemInfo' => $itemInfo, 'breadCrumbs'  => $catalogs,]);
        }
    }

    /**
     * Displays a single CatalogRu model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CatalogRu model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->layout = 'admin';
        $model = new CatalogRu();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $file = UploadedFile::getInstanceByName('CatalogRu[fscatalog_img]');
            $fileIcon = UploadedFile::getInstanceByName('CatalogRu[fsicon]');
            $fileBigIcon = UploadedFile::getInstanceByName('CatalogRu[fsbigicon]');

            if($file) {
                $file->saveAs(dirname(__FILE__).'/../web/data/cat/catalog_' . $model->ficatalog_id . '.' . $file->extension);
                $model->fscatalog_img = 'data/cat/catalog_' . $model->ficatalog_id . '.' . $file->extension;
                $model->save();
            }

            if($fileIcon) {
                $fileIcon->saveAs(dirname(__FILE__).'/../web/data/cat/icon/catalog_' . $model->ficatalog_id . '.' . $fileIcon->extension);
                $model->fsicon = 'data/cat/icon/catalog_' . $model->ficatalog_id . '.' . $fileIcon->extension;
                $model->save();
            }

            if($fileBigIcon) {
                $fileBigIcon->saveAs(dirname(__FILE__).'/../web/data/cat/icon_big/catalog_' . $model->ficatalog_id . '.' . $fileBigIcon->extension);
                $model->fsbigicon = 'data/cat/icon_big/catalog_' . $model->ficatalog_id . '.' . $fileBigIcon->extension;
                $model->save();
            }

            return $this->redirect(['catalog/admin']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CatalogRu model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $this->layout = 'admin';
        $model = $this->findModel($id);
        $oldImage = $model->fscatalog_img;
        $oldIcon = $model->fsicon;
        $oldBigIcon = $model->fsbigicon;

        $file = UploadedFile::getInstanceByName('CatalogRu[fscatalog_img]');
        $fileIcon = UploadedFile::getInstanceByName('CatalogRu[fsicon]');
        $fileBigIcon = UploadedFile::getInstanceByName('CatalogRu[fsbigicon]');

        if(Yii::$app->request->isPost) {
            $filters = Yii::$app->request->post('active_filters');
            $filters = json_encode($filters);
            $model->filters = $filters;
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if(!$file) {
                $model->fscatalog_img = $oldImage;
                $model->save();
            }
            else {
                $img = trim($model->fscatalog_img);
                if(!empty($img) && file_exists(dirname(__FILE__) . '/../' . $model->fscatalog_img)) {
                    unlink(dirname(__FILE__) . '/../web/' . $model->fscatalog_img);
                }

                $file->saveAs(dirname(__FILE__).'/../web/data/cat/catalog_' . $model->ficatalog_id . '.' . $file->extension);
                $model->fscatalog_img = 'data/cat/catalog_' . $model->ficatalog_id . '.' . $file->extension;
                $model->save();
            }
            if(!$fileBigIcon) {
                $model->fsbigicon = $oldBigIcon;
                $model->save();
            }
            else {
                $img = trim($model->fsbigicon);
                if(!empty($img) && file_exists(Yii::$app->basePath . '/web/' . $model->fsbigicon)) {
                    unlink(Yii::$app->basePath . '/web/' . $model->fsbigicon);
                }

                $fileBigIcon->saveAs(dirname(__FILE__).'/../web/data/cat/icon_big/catalog_' . $model->ficatalog_id . '.' . $fileBigIcon->extension);
                $model->fsbigicon = 'data/cat/icon_big/catalog_' . $model->ficatalog_id . '.' . $fileBigIcon->extension;
                $model->save();
            }
            if(!$fileIcon) {
                $model->fsicon = $oldIcon;
                $model->save();
            }
            else {
                $img = trim($model->fsicon);
                if(!empty($img) && file_exists(Yii::$app->basePath . '/web/' . $model->fsicon)) {
                    unlink(Yii::$app->basePath . '/web/' . $model->fsicon);
                }

                $fileIcon->saveAs(dirname(__FILE__).'/../web/data/cat/icon/catalog_' . $model->ficatalog_id . '.' . $fileIcon->extension);
                $model->fsicon = 'data/cat/icon/catalog_' . $model->ficatalog_id . '.' . $fileIcon->extension;
                $model->save();
            }
            return $this->redirect(['catalog/admin']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * @param $id
     * @param $parent
     * @throws NotFoundHttpException
     */
    public function actionUpdateParent($id,$parent) {
        $model = $this->findModel($id);
        $model->fiparent_catalog_id = $parent;
        $model->save();
    }

    public function actionUpdateOrder($id,$order) {
        $model = $this->findModel($id);
        $model->fiorder = (int) $order;
        $model->save();
    }

    /**
     * Deletes an existing CatalogRu model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CatalogRu model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CatalogRu the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CatalogRu::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
