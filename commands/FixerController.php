<?php

namespace app\commands;

use yii\console\Controller;
use app\models\Items;

class FixerController extends Controller
{
    public function actionFix()
    {
        $items = require(__DIR__.'/items.php');
        $i = 0;
        foreach ($items as $item) {
            if ( $model = Items::findOne(['fsitem_name' => $item['fsitem_name']])) {
                $model->fsseo = $item['fsseo'];
                $model->save(false);
                $i++;
            }
        }
        echo $i . ' of ' . count($items) . "\n";
        echo "Done\n";
    }
}
