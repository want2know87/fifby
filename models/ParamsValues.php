<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "params_values".
 *
 * @property string $fiparam_value_id
 * @property string $fiitem_id
 * @property string $fiparam_id
 * @property string $fsparam_value
 * @property integer $filight
 * @property integer $fiorder
 *
 * @property Items $fiitem
 * @property ParamsCat $fiparam
 */
class ParamsValues extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'params_values';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fiitem_id', 'fiparam_id'], 'required'],
            [['fiitem_id', 'fiparam_id', 'filight', 'fiorder'], 'integer'],
            [['fsparam_value'], 'string', 'max' => 500]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fiparam_value_id' => Yii::t('app', 'Внутренний номер'),
            'fiitem_id' => Yii::t('app', 'Ссылка на таблицу с продуктами'),
            'fiparam_id' => Yii::t('app', 'Ссылка на таблицу с именами параметров'),
            'fsparam_value' => Yii::t('app', 'Значение параметра'),
            'filight' => Yii::t('app', 'Filight'),
            'fiorder' => Yii::t('app', 'Fiorder'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFiitem()
    {
        return $this->hasOne(Items::className(), ['fiitem_id' => 'fiitem_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFiparam()
    {
        return $this->hasOne(ParamsCat::className(), ['fiparam_id' => 'fiparam_id']);
    }
}
