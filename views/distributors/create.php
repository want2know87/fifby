<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Distributors */

$this->title = Yii::t('app', 'app.internal.create-distrib');
$this->params['admin'] = [
    'page_title' => $this->title,
    'main_title' => 'Дистрибьюторы',
    'main_url' => 'distributors/admin'
];
?>
<div class="distributors-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
