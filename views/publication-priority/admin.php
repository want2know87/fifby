<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use app\models\Publications;

$this->title = 'Приоритет публикаций';
$this->params['admin'] = [
    'page_title' => $this->title,
    'main_title' => $this->title,
    'main_url' => 'publication-priority/admin'
];
?>

<div class="publication-priority-index">
    <div class="row">
        <div class="col-md-12">
            <ul class="list-inline">
                <li><a href="<?= Url::to(['', 'type' => 0]) ?>" class="btn btn-primary" <?= Yii::$app->request->get('type') == 0 ? 'disabled' : null ?>>Прайс листы</a></li>
                <li><a href="<?= Url::to(['', 'type' => 2]) ?>" class="btn btn-primary" <?= Yii::$app->request->get('type') == 2 ? 'disabled' : null ?>>ПО, Схемы</a></li>
                <li><a href="<?= Url::to(['', 'type' => 3]) ?>" class="btn btn-primary" <?= Yii::$app->request->get('type') == 3 ? 'disabled' : null ?>>Каталоги</a></li>
                <li><a href="<?= Url::to(['', 'type' => 4]) ?>" class="btn btn-primary" <?= Yii::$app->request->get('type') == 4 ? 'disabled' : null ?>>Материалы для размещения</a></li>
            </ul>
        </div>
    </div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'fsname',
            [
                'label' => 'Тип',
                'value' => function ($model) {
                    return Publications::$types[$model->fipublic->fitype];
                }
            ],
            'priority',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update-publication-types}',
                'buttons' => [
                    'update-publication-types' => function ($url, $model, $key) {
                        return Html::a(Yii::t('app', 'Update'), ['publication-files/update', 'id' => $model->fipublic_file_id]);
                    }
                ]
            ],
        ]
    ]) ?>
</div>
