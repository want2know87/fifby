<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Items */

$this->title = Yii::t('app', 'Create {modelClass}', [
    'modelClass' => Yii::t('app','Items'),
]);
$this->params['admin'] = [
    'page_title' => $this->title,
    'main_title' => 'Товар',
    'main_url' => 'items/admin'
];
?>


<div class="items-create">

    <?= $this->render('_form', [
        'model' => $model,
        'itemList' => $itemList,
    ]) ?>

</div>
