<?php

namespace app\controllers;

use Yii;
use yii\web\UploadedFile;
use PHPExcel_IOFactory;
use PHPExcel_Cell;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

class AdminController extends \yii\web\Controller
{
    private $dbBY = 'fifby_elekoru9_fifby';
    private $dbRU = 'fifby_elekoru9_fifru';

    public $layout = 'admin';

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'updatechpu' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index'],
                'rules' => [
                    [
                        'actions' => ['index', 'syncdillers', 'syncitems'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionAddsyncitems() {
        $tables = [
            'catalog_items' => 'ficatalog_item_id',
            'items' => 'fiitem_id',
            'items_pictures' => 'fiitem_pic_id',
            'params' => 'param_id',
            'params_cat' => 'fiparam_id',
            'params_values' => 'fiparam_value_id',
        ];

        $dirs = [
            dirname(__FILE__) . '/../web/data/img/',
            dirname(__FILE__) . '/../web/data/files/',
        ];

        $dirsFrom = [
            str_replace('old-fifby','old-fifru', dirname(__FILE__) . '/../web/data/img/'),
            str_replace('old-fifby','old-fifru', dirname(__FILE__) . '/../web/data/files/'),
        ];

        $transaction = \Yii::$app->db->beginTransaction();
        \Yii::$app->db->createCommand('SET foreign_key_checks = 0;')->execute();
        foreach ($tables as $table => $primary) {

            $maxId = \Yii::$app->db->createCommand("SELECT max({$primary}) as max FROM " . $this->dbBY .".{$table}")->queryOne();
            \Yii::$app->db->createCommand("INSERT INTO " . $this->dbBY . ".{$table} SELECT * from " . $this->dbRU . ".{$table} where $primary > " . (int)$maxId['max'])->execute();

        }
        \Yii::$app->db->createCommand('SET foreign_key_checks = 1;')->execute();
        $transaction->commit();

        foreach ($dirs as $dir) {
            array_map('unlink', glob($dir . '*'));
        }

        foreach ($dirs as $index => $dir) {
            $source = realpath($dirsFrom[$index]);
            $dest = realpath($dir . '../');
            shell_exec("cp -r $source $dest");
        }

        return $this->redirect(['admin/index']);
    }

    public function actionSyncitems() {
        $tables = [
            'catalog_items',
            'items',
            'items_pictures',
            'params',
            'params_cat',
            'params_values',
        ];

        $dirs = [
            dirname(__FILE__) . '/../web/data/img/',
            dirname(__FILE__) . '/../web/data/files/',
        ];

        $dirsFrom = [
            str_replace('old-fifby','old-fifru', dirname(__FILE__) . '/../web/data/img/'),
            str_replace('old-fifby','old-fifru', dirname(__FILE__) . '/../web/data/files/'),
        ];

        $transaction = \Yii::$app->db->beginTransaction();
        \Yii::$app->db->createCommand('SET foreign_key_checks = 0;')->execute();
        foreach ($tables as $table) {
            \Yii::$app->db->createCommand("TRUNCATE TABLE {$table}")->execute();
        }
        \Yii::$app->db->createCommand('SET foreign_key_checks = 1;')->execute();
        $transaction->commit();
        $transaction = \Yii::$app->db->beginTransaction();
        \Yii::$app->db->createCommand('SET foreign_key_checks = 0;')->execute();
        foreach ($tables as $table) {
            \Yii::$app->db->createCommand("INSERT INTO " . $this->dbBY . ".{$table} SELECT * from " . $this->dbRU . ".{$table}")->execute();
        }
        \Yii::$app->db->createCommand('SET foreign_key_checks = 1;')->execute();
        \Yii::$app->db->createCommand('UPDATE items SET fsprice_rur = NULL;')->execute();

        $transaction->commit();

	/** файлы берутся из tde-fif.ru, сделаны символические ссылки */
	/*
        foreach ($dirs as $dir) {
            @array_map('unlink', glob($dir . '*'));
        }

        foreach ($dirs as $index => $dir) {
            $source = realpath($dirsFrom[$index]);
            $dest = realpath($dir . '../');
            shell_exec("cp -r $source $dest");
        }
        */

        return $this->redirect(['admin/index']);
    }

    public function actionSyncdillers() {
        $tables = [
            'distributors',
            'distributors_mail',
            'distributors_phones',
            'distributors_point',
            'distributors_point_mail',
            'distributors_point_phones',
            'town',
        ];
        $transaction = \Yii::$app->db->beginTransaction();
        \Yii::$app->db->createCommand('SET foreign_key_checks = 0;')->execute();
        foreach ($tables as $table) {
            \Yii::$app->db->createCommand("TRUNCATE TABLE {$table}")->execute();
        }
        \Yii::$app->db->createCommand('SET foreign_key_checks = 1;')->execute();
        $transaction->commit();
        $transaction = \Yii::$app->db->beginTransaction();
        \Yii::$app->db->createCommand('SET foreign_key_checks = 0;')->execute();
        foreach ($tables as $table) {
            \Yii::$app->db->createCommand("INSERT INTO " . $this->dbBY . ".{$table} SELECT * from " . $this->dbRU . ".{$table}")->execute();
        }
        \Yii::$app->db->createCommand('SET foreign_key_checks = 1;')->execute();
        $transaction->commit();
        return $this->redirect(['admin/index']);
    }

    public function actionIndex()
    {

        if(\Yii::$app->request->isPost) {
            $file = UploadedFile::getInstancesByName('catalog');
            if($file) {
                $file = current($file);
                $name = 'catalog_' . time() . '_' . 'catalog' . '.' . $file->extension;
                $file->saveAs(dirname(__FILE__).'/../web/' . $name );
            }
            $excel = UploadedFile::getInstanceByName('excel');
            if($excel) {
                $name = 'excel_' . time() . '.' . $excel->extension;
                $excel->saveAs(dirname(__FILE__).'/../web/' . $name );
                $excel = PHPExcel_IOFactory::load(dirname(__FILE__).'/../web/' . $name );
                $articleCol = -1;
                $nameCol    = -1;
                $startRow   = -1;
                $priceRows = [
                    'usd' => [
                        'value' => '',
                        'ind' => 'usd',
                        'col' => -1,
                        'search' => ['usd', '$'],
                    ],
                    'rur' => [
                        'value' => '',
                        'ind' => 'rur',
                        'col' => -1,
                        'search' => ['rur'],
                    ],
                    'byn' => [
                        'value' => '',
                        'ind' => 'byn',
                        'col' => -1,
                        'search' => ['byn'],
                    ],
                ];
                foreach($excel->getWorksheetIterator() as $worksheet) {
                    for($row = 0; $row < $worksheet->getHighestRow(); $row ++) {
                        for($col = 0; $col < PHPExcel_Cell::columnIndexFromString($worksheet->getHighestColumn()); $col++) {
                            $cell = $worksheet->getCellByColumnAndRow($col, $row);
                            if('' == trim($cell->getValue())) {
                                continue;
                            }
                            if('Наименование' == $cell->getValue()) {
                                $startRow = $row + 1;
                                $nameCol  = $col;
                            }
                            if('Артикул' == $cell->getValue()) {
                                $articleCol = $col;
                            }
                            foreach($priceRows as $priceKey => $priceVal) {
                                foreach($priceVal['search'] as $val => $search) {
                                    //echo "Compare ", mb_strtolower($cell->getValue()), ":", $search;
                                    if(strpos(mb_strtolower($cell->getValue()), $search) !== false) {
                                        //echo " finded";
                                        $priceRows[$priceKey]['col'] = $col;
                                        break;
                                    }
                                    //echo "\n";
                                }
                            }
                        }
                        if($articleCol >= 0 && $nameCol >= 0 && $startRow >=0) {
                            break;
                        }
                    }

                    if($articleCol >= 0 && $nameCol >= 0 && $startRow >=0) {
                        for($row = $startRow; $row < $worksheet->getHighestRow(); $row++) {
                            $prices = [];
                            foreach($priceRows as $priceRow) {
                                if($priceRow['col'] > 0) {
                                    $prices['fsprice_' . $priceRow['ind']] = round($worksheet->getCellByColumnAndRow($priceRow['col'], $row)->getValue(), 2);
                                }
                            }
                            //$name = trim($worksheet->getCellByColumnAndRow($nameCol, $row)->getValue());
                            $article = trim($worksheet->getCellByColumnAndRow($articleCol, $row)->getValue());
                            if('' != trim($article)) {
                                $finded = \Yii::$app->db->createCommand('SELECT * FROM items where fsarticle = :name', ['name' => $article])->queryOne();
                                if(!empty($finded)) {
                                    $strPrice = '';
                                    foreach($prices as $key => $value) {
                                        $strPrice .= ' ' . $key . ' = ' . '"' . $value . '", ';
                                    }
                                    \Yii::$app->db->createCommand('UPDATE items SET ' . $strPrice . ' fsarticle = :article WHERE fiitem_id = :itemid', ['article' => $article, 'itemid' => $finded['fiitem_id']])->execute();
                                }
                                /*echo "Finded name: {$name}, article: {$article}, prices: ", json_encode($prices), "\n";*/
                            }

                        }
                    }

                    break;
                }
            }
	    \Yii::$app->db->createCommand('update items set fsprice_byn = round(fsprice_byn,2)')->execute();
        }

        return $this->render('index');
    }

    public function actionUpdatechpu()
    {
        Yii::$app->consoleRunner->run('rule/generate');

        return $this->redirect(['index']);
    }

}
