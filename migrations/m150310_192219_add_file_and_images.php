<?php

use yii\db\Schema;
use yii\db\Migration;

class m150310_192219_add_file_and_images extends Migration
{
    public function safeUp()
    {
        $this->addColumn('items', 'fspassport','varchar(255)');
        $this->createTable('items_pictures', [
            'fiitem_pic_id' => 'pk',
            'fiitem_id' => 'int(10) unsigned not null',
            'fsname' => 'varchar(255) not null',
            'fsimage' => 'varchar(255)',
        ]);
        $this->createIndex('ix_item_pic','items_pictures', 'fiitem_id');
        $this->addForeignKey('fk_item_pic','items_pictures', 'fiitem_id', 'items', 'fiitem_id');
    }

    public function safeDown()
    {
        $this->dropColumn('items', 'fspassport');
        $this->dropForeignKey('fk_item_pic', 'items_pictures');
        $this->dropIndex('ix_item_pic', 'items_pictures');
        $this->dropTable('items_pictures');
    }

}
