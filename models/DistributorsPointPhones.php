<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "distributors_point_phones".
 *
 * @property integer $fidistrib_point_mail_id
 * @property integer $fidistr_point_id
 * @property string $fsphone
 *
 * @property DistributorsPoint $fidistrPoint
 */
class DistributorsPointPhones extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'distributors_point_phones';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fidistr_point_id'], 'required'],
            [['fidistr_point_id'], 'integer'],
            [['fsphone'], 'string', 'max' => 255],
            [['fidistr_point_id', 'fsphone'], 'unique', 'targetAttribute' => ['fidistr_point_id', 'fsphone'], 'message' => 'The combination of Fidistr Point ID and Fsphone has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fidistrib_point_mail_id' => Yii::t('app', 'Fidistrib Point Mail ID'),
            'fidistr_point_id' => Yii::t('app', 'Fidistr Point ID'),
            'fsphone' => Yii::t('app', 'Fsphone'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFidistrPoint()
    {
        return $this->hasOne(DistributorsPoint::className(), ['fidistr_point_id' => 'fidistr_point_id']);
    }
}
