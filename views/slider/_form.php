<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Slider */
/* @var $form yii\widgets\ActiveForm */

$enabled = [
    0 => 'Нет',
    1 => 'Да',
];

?>

<div class="slider-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?php if(strlen($model->fsslide_picture)): ?>
        <div class="form-group field-slider-fsslide_picture has-success">
            <?php echo Html::img(Yii::$app->request->baseUrl . '/' . $model->fsslide_picture, ['style' => 'max-width:400px;']) ?>
        </div>
    <?php endif; ?>

    <?= $form->field($model, 'fsslide_picture')->fileInput() ?>

    <?= $form->field($model, 'fsslide_button_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fsslide_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fislide_visible')->dropDownList($enabled) ?>

    <?= $form->field($model, 'fsslide_link')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
