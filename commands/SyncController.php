<?php

namespace app\commands;

use yii\console\Controller;

class SyncController extends Controller
{
    private $dbBY = 'fifby_elekoru9_fifby';
    private $dbRU = 'fifby_elekoru9_fifru';

    public function actionItems()
    {
        $tables = [
            'catalog_items',
            'items',
            'items_pictures',
            'params',
            'params_cat',
            'params_values',
        ];

        $dirs = [
            dirname(__FILE__) . '/../web/data/img/',
            dirname(__FILE__) . '/../web/data/files/',
        ];

        $dirsFrom = [
            dirname(__FILE__) . '/../old-fifru/web/data/img/',
            dirname(__FILE__) . '/../old-fifru/web/data/files/',
        ];

        $transaction = \Yii::$app->db->beginTransaction();
        \Yii::$app->db->createCommand('SET foreign_key_checks = 0;')->execute();
        foreach ($tables as $table) {
            \Yii::$app->db->createCommand("TRUNCATE TABLE {$table}")->execute();
        }
        \Yii::$app->db->createCommand('SET foreign_key_checks = 1;')->execute();
        $transaction->commit();
        $transaction = \Yii::$app->db->beginTransaction();
        \Yii::$app->db->createCommand('SET foreign_key_checks = 0;')->execute();
        foreach ($tables as $table) {
            \Yii::$app->db->createCommand("INSERT INTO " . $this->dbBY . ".{$table} SELECT * from " . $this->dbRU . ".{$table}")->execute();
        }
        \Yii::$app->db->createCommand('SET foreign_key_checks = 1;')->execute();
        \Yii::$app->db->createCommand('UPDATE items SET fsprice_rur = NULL;')->execute();

        $transaction->commit();

	/*
        foreach ($dirs as $dir) {
            @array_map('unlink', glob($dir . '*'));
        }

        foreach ($dirs as $index => $dir) {
            $source = realpath($dirsFrom[$index]);
            $dest = realpath($dir . '../');
            shell_exec("cp -r $source $dest");
        }
        */

        return 0;
    }
}
