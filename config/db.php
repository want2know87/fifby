<?php

$localDb = is_file(__DIR__ . '/db-local.php')
    ? require(__DIR__ . '/db-local.php')
    : [];

return array_merge([
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=fifby_elekoru9_fifby',
    'username' => 'fifby_elekoru9',
    'password' => '123elek_4',
    'charset' => 'utf8',
], $localDb);
