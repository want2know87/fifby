<?php

use yii\db\Schema;
use yii\db\Migration;

class m150331_182528_visible_catalog extends Migration
{
    public function up()
    {
        $this->addColumn('catalog_ru', 'fivisible', 'int(1) default 1');
        $this->createIndex('ix_catalog_visible', 'catalog_ru', 'fivisible');
    }

    public function down()
    {
        $this->dropColumn('catalog_ru', 'fivisible');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
