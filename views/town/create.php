<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Town */
$this->params['admin'] = [
    'page_title' => $this->title,
    'main_title' => 'Создать город',
    'main_url' => 'town/create'
];
?>
<div class="town-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
