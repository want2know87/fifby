<?php

namespace app\controllers;

use app\models\DistributorsPoint;
use app\models\DistributorsPointPhones;
use app\models\DistributorsPointMail;
use app\models\Town;
use Yii;
use app\models\Distributors;
use app\models\DistributorsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * DistributorsController implements the CRUD actions for Distributors model.
 */
class DistributorsPointController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'get-distrib', 'admin', 'create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionGetDistrib($letters, $country, $town)
    {
        $return = [];
        $distribSelect = Distributors::find()
            ->where('fsname like :likefield', ['likefield' => '%' . $letters . '%']);
        if (!empty($country)) {
            $distribSelect->andWhere(['ficountry_id' => (int)$country]);
        }
        if (!empty($town)) {
            $distribSelect->andWhere($distribSelect->where, ['fitown_id' => (int)$town]);
        }
        $distrib = $distribSelect->limit(10)->all();
        /** @var Distributors $distr */
        foreach ($distrib as $distr) {
            $return[$distr->fidistr_id] = $distr->fsname;
        }
        return json_encode($return);
    }

    /**
     * Lists all Distributors models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DistributorsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Distributors models.
     * @return mixed
     */
    public function actionAdmin()
    {
        $this->layout = 'admin';
        $searchModel = new DistributorsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('admin', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Distributors model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Distributors model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->layout = 'admin';
        $model = new DistributorsPoint();

        $model->load(Yii::$app->request->post());
        $model->fidistr_id = Yii::$app->request->post('distr_id');

        if ($model->save()) {

            $additional = Yii::$app->request->post('Distributors');
            DistributorsPointPhones::deleteAll('fidistr_point_id = :distr', [':distr' => $model->fidistr_point_id]);
            DistributorsPointMail::deleteAll('fidistr_point_id = :distr', [':distr' => $model->fidistr_point_id]);
            foreach (['Phones', 'Mails'] as $field) {
                if (isset($additional['distributors' . $field]) && !empty($additional['distributors' . $field])) {
                    $value = $additional['distributors' . $field];
                    $values = array_map('trim', explode("\n", current($value)));
                    switch ($field) {
                        case 'Phones':
                            foreach ($values as $val) {
                                $phone = new DistributorsPointPhones();
                                $phone->fidistr_point_id = $model->fidistr_point_id;
                                $phone->fsphone = $val;
                                $phone->save();
                            }
                            break;
                        case 'Mails':
                            foreach ($values as $val) {
                                $mail = new DistributorsPointMail();
                                $mail->fidistr_point_id = $model->fidistr_point_id;
                                $mail->fsmail = $val;
                                $mail->save();
                            }
                            break;
                        default:
                            break;
                    }
                }
            }

            return $this->redirect(['distributors/update', 'id' => (int)Yii::$app->request->post('distr_id')]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Distributors model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $this->layout = 'admin';
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $additional = Yii::$app->request->post('Distributors');
            DistributorsPointPhones::deleteAll('fidistr_point_id = :distr', [':distr' => $model->fidistr_point_id]);
            DistributorsPointMail::deleteAll('fidistr_point_id = :distr', [':distr' => $model->fidistr_point_id]);
            foreach (['Phones', 'Mails'] as $field) {
                if (isset($additional['distributors' . $field]) && !empty($additional['distributors' . $field])) {
                    $value = $additional['distributors' . $field];
                    $values = array_map('trim', explode("\n", current($value)));
                    switch ($field) {
                        case 'Phones':
                            foreach ($values as $val) {
                                $phone = new DistributorsPointPhones();
                                $phone->fidistr_point_id = $model->fidistr_point_id;
                                $phone->fsphone = $val;
                                $phone->save();
                            }
                            break;
                        case 'Mails':
                            foreach ($values as $val) {
                                $mail = new DistributorsPointMail();
                                $mail->fidistr_point_id = $model->fidistr_point_id;
                                $mail->fsmail = $val;
                                $mail->save();
                            }
                            break;
                        default:
                            break;
                    }
                }
            }

            return $this->redirect(['distributors/update', 'id' => (int)Yii::$app->request->post('distr_id')]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing DistributorsPoint model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id, $distr_id, $page)
    {
        $model = $this->findModel($id);
        DistributorsPointPhones::deleteAll('fidistr_point_id = :distr', [':distr' => $model->fidistr_point_id]);
        DistributorsPointMail::deleteAll('fidistr_point_id = :distr', [':distr' => $model->fidistr_point_id]);
        $model->delete();

        return $this->redirect(['distributors/update', 'id' => $distr_id, 'page' => (int)$page]);
    }

    /**
     * Finds the Distributors model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DistributorsPoint the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DistributorsPoint::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
