<?php

use yii\db\Schema;
use yii\db\Migration;

class m150317_152300_templates extends Migration
{
    public function up()
    {
        $this->addColumn('catalog_ru', 'fstemplate_code', 'varchar(32)');

        $this->createTable('catalog_templates', [
            'fitemplate_id' => 'pk',
            'fstemplate_code' => 'varchar(32) not null',
            'fstemplate_text' => 'text',
        ]);
        $this->createIndex('un_template_name', 'catalog_templates', 'fstemplate_code', true);
    }

    public function down()
    {
        $this->dropTable('catalog_templates');
        $this->dropColumn('catalog_ru', 'fstemplate_code');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
