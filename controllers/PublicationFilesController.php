<?php

namespace app\controllers;

use Yii;
use app\models\PublicationFiles;
use app\models\PublicationFilesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;

/**
 * PublicationFilesController implements the CRUD actions for PublicationFiles model.
 */
class PublicationFilesController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'view', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                      'actions' => ['get-item'],
                      'allow' => true,
                      'roles' => ['?', '@'],
                    ],
                ],
            ],
            /*'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],*/
        ];
    }

    public function actionGetItem($id) {
        $model = $this->findModel((int)$id);
        if ( strpos ( $_SERVER [ 'HTTP_USER_AGENT' ], "MSIE" ) > 0 )
        {
          header ( 'Content-Disposition: attachment; filename="' . rawurlencode ( $model->fsreal_name ) . '"' );
        }
        else
        {
          header( 'Content-Disposition: attachment; filename*=UTF-8\'\'' . rawurlencode ( $model->fsreal_name ) );
        }
        readfile(dirname(__FILE__).'/../web' . '/' . $model->fsfile);
    }

    /**
     * Lists all PublicationFiles models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PublicationFilesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PublicationFiles model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PublicationFiles model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PublicationFiles();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $file = UploadedFile::getInstanceByName('PublicationFiles[fsfile]');

            if($file) {
                if(file_exists(dirname(__FILE__).'/../web/data/pub/pub_item_' . $model->fipublic_id . '.' . $file->extension)) {
                  unlink(dirname(__FILE__).'/../web/data/pub/pub_item_' . $model->fipublic_id . '.' . $file->extension);
                }
                $file->saveAs(dirname(__FILE__).'/../web/data/pub/pub_item_' . $model->fipublic_id . '.' . $file->extension);
                $model->fsfile = 'data/pub/pub_item_' . $model->fipublic_id . '.' . $file->extension;
                $model->fsreal_name = $file->name;
                $model->save();
            }
            return $this->redirect(['publications/update', 'id' => $model->fipublic_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing PublicationFiles model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $this->layout = 'admin';
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['publication-priority/admin', 'type' => $model->fipublic->fitype]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing PublicationFiles model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['publications/update', 'id' => Yii::$app->request->get('publication')]);
    }

    /**
     * Finds the PublicationFiles model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PublicationFiles the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PublicationFiles::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
