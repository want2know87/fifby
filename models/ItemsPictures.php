<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "items_pictures".
 *
 * @property integer $fiitem_pic_id
 * @property string $fiitem_id
 * @property string $fsname
 * @property string $fsimage
 *
 * @property Items $fiitem
 */
class ItemsPictures extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'items_pictures';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fiitem_id', 'fsname'], 'required'],
            [['fiitem_id'], 'integer'],
            [['fsname', 'fsimage'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fiitem_pic_id' => Yii::t('app', 'fiitem_pic_id'),
            'fiitem_id' => Yii::t('app', 'Fiitem ID'),
            'fsname' => Yii::t('app', 'Fsname'),
            'fsimage' => Yii::t('app', 'Fsimage'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFiitem()
    {
        return $this->hasOne(Items::className(), ['fiitem_id' => 'fiitem_id']);
    }
}
