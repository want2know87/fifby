<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SertificatsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Sertificats');
$this->params['admin'] = [
    'page_title' => $this->title,
];
?>
<div class="sertificats-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Sertificats'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'lang',
                'filterInputOptions' => ['prompt' => '', 'class'=> 'form-control'],
                'filter' => ['ru' => 'RU', 'en' => 'EN'],
            ],

            'fssert_name',
            'fssert_text:ntext',
            'fssert_img' => [
                'class' => \yii\grid\DataColumn::className(),
                'attribute' => 'fssert_img',
                'format' => 'raw',
                'value' => function(\app\models\Sertificats $model) {
                    return $model->fssert_img ? Html::img(Yii::$app->request->baseUrl . '/' . $model->fssert_img, ['style' => 'max-width:200px']) : '';
                },
            ],
            [
                'attribute' => 'priority',
                'filter' => false,
            ],

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {delete}'],
        ],
    ]); ?>

</div>
