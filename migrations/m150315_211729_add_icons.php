<?php

use yii\db\Schema;
use yii\db\Migration;

class m150315_211729_add_icons extends Migration
{
    public function up()
    {
        $this->addColumn('catalog_ru', 'fsicon', 'varchar(255)');
    }

    public function down()
    {
        $this->dropColumn('catalog_ru', 'fsicon');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
