<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Selects */
/* @var $form yii\widgets\ActiveForm */

$options = [];

if(Yii::$app->request->post('parent')) {
    $options = ['value' => (int) Yii::$app->request->post('parent')];
}

$js = <<<EOF

EOF;

$this->registerJs($js);

?>

<div class="selects-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'fsselect_name')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'fiparent_id')->hiddenInput($options)->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <?php if(!$model->isNewRecord && (int)$model->fiparent_id != 0): ?>
        <?php echo \yii\jui\AutoComplete::widget([
            'name' => 'item',
            'clientOptions' => [
                'source' => \yii\helpers\Url::to(['items/getall']),
                'autoFill'=>true,
                'minLength'=>3,
                'select' => new \yii\web\JsExpression("function( event, ui ) {
                    console.log(ui.item.id);
                    $('#addme').val(ui.item.id);
                    $('.ui-autocomplete-input').data('id', ui.item.id);
                 }")
            ]
        ]); ?>
        <form id="additem" method="POST" style="display: inline;" action="<?php echo \yii\helpers\Url::to(['selects/additem', 'cat' => $model->fiselect_id ]) ?>">
            <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
            <input type="hidden" name="item" value="" id="addme" />
            <button type="submit">+</button>
        </form>
        <div class="items">
            <?php

            $items = Yii::$app->db->createCommand('SELECT * from selects_items si inner join items i on i.fiitem_id = si.fiitem_id where fiselect_id = ' . (int)$model->fiselect_id)->queryAll();
            foreach ($items as $item) {
                ?>
            <?php echo $item['fsitem_name']; ?>
            <form method="POST" style="display: inline;" action="<?php echo \yii\helpers\Url::to(['selects/deleteitem', 'cat' => $model->fiselect_id , 'id' => $item['fiitem_id']]) ?>">
                <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
                <button type="submit">-</button>
            </form>
            <br/>
            <?php } ?>
        </div>
    <?php endif; ?>

</div>
