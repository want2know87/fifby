<?php

use yii\helpers\Html;
use app\models\CatalogRu;
$parent = [];
$parentsQeury = CatalogRu::find()->where(['fiparent_catalog_id' => null])->andWhere(['fivisible' => 1])->orWhere(['fiparent_catalog_id'=>''])->orderBy('fiorder')->all();
/** @var CatalogRu $parents */
foreach($parentsQeury as $parents) {
    $translates = json_decode($parents->translates, true);
    $parent[] = [
        'id' => $parents->ficatalog_id,
        'name' => $parents['fsname'],
        'big-image' => $parents->fsbigicon,
        'link' => $parents->fslink,
        'subcat' => [],
    ];
}

$all = CatalogRu::find()->where(['fivisible' => 1])->orderBy('fiorder')->all();
/** @var CatalogRU $row */
foreach($all as $row) {
    foreach($parent as $index => $category) {
        if($category['id'] == $row->fiparent_catalog_id) {
            $translates = json_decode($row->translates, true);
            $parent[$index]['subcat'][] = [
                'id' => $row->ficatalog_id,
                'name' => $row['fsname'],
                'subcat' => [],
                'icon' => $row->fsicon];
        }
    }
}

foreach($all as $row) {
    foreach($parent as $indcat => $category) {
        foreach($category['subcat'] as $index => $subcat) {
            if($subcat['id'] == $row->fiparent_catalog_id) {
                $translates = json_decode($row->translates, true);
                $parent[$indcat]['subcat'][$index]['subcat'][] = [
                    'id' => $row->ficatalog_id,
                    'name' => $row->fsname,
                    'subcat' => []];
            }
        }
    }
}

$activeCat = Yii::$app->request->get('cat');
if(empty($activeCat)) {
    $activeCat = $parent[0]['id'];
}

/* @var $this yii\web\View */
/* @var $searchModel app\models\CatalogSearchRu */
/* @var $dataProvider yii\data\ActiveDataProvider */

/*$this->title = Yii::t('app', 'app.cat');*/

//print_r($parent);

?>

<?php echo $this->render('/catalog/breadcrumb', ['breadcrumb' => $breadCrumbs]) ?>

<?php

$filename = '';
$scan = scandir(__DIR__ . '/../../web', SCANDIR_SORT_DESCENDING);
foreach($scan as $file) {
    if(strpos($file, 'catalog_') === 0) {
        $filename = $file;
        break;
    }
}

?>

<div class="page_ttl">
    <div class="inner_width">
        <h1><?=$breadCrumbs['low_name']?></h1>

        <a href="<?php echo Yii::$app->request->baseUrl . '/' . $filename ?>" class="rzacatalog"><?=Yii::t('app', 'catalog.electrical')?>
            <span><?php echo strlen($filename) ? 'pdf, ' . round(filesize(__DIR__ . '/../../web/' . $filename)/1024/1024, 0) . ' ' . Yii::t('app', 'catalog.mb') : 'pdf, 27 мб' ?></span></a>
    </div>
</div>

<div class="inner_width ovfl center">
    <?php echo $this->render('catalog-menu', ['catalogs' => $parent, 'active' => $activeCat]); ?>
</div>
<style>
.catalogcol_item .title-div{
    height: 51px;
    text-align: center;
    font-size: 14px;
    color: #fff;
    border: 2px solid rgba(102,102,102,1);
    margin-right: 2%;
    margin-bottom: 15px;
}
.col-xs-12.padding-none.image {
    min-height: 113px;
}
.catalogcol_item {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    float: none;
    display: inline-block;
    margin-right: -4px;
    vertical-align: top;
}
a.catalogcol_item_ttl{
    margin-bottom: 0px;
}
.title-div .icon {
    position: absolute;
    margin-left: 10px;
    margin-top: 0px;
}
.title-div a {
    text-decoration: none;
    padding-left: 30px;
}
.title-div {
    position: relative;
    display: flex;
    justify-content: center;
    align-content: center;
    flex-direction: column;
}
</style>
<div class="inner_width ovfl">
    <?php /*for($columns=0; $columns < 3; $columns++):*/ ?>
    <div class=""><!-- catalogcol -->
        <?php /* $i = -1; */?>
        <?php foreach($parent[0]['subcat'] as $category): ?>
        <?php /* $i++  */?>
        <?php /*if( ($i%3) != $columns) continue;*/ ?>
        <div class="catalogcol_item col-md-4"><!-- catalogcol_item -->
            <div class="title-div">
                <?php if (!empty($category['icon'])): ?>
                <div class="icon" style="display: inline-block;">
                    <img src="<?=Yii::$app->request->baseUrl . "/" . $category['icon']?>"/>
                </div>
                <?php endif; ?>
                <?php echo Html::a($category['name'], \yii\helpers\Url::to(['catalog/index','cat' => $category['id']]), ['class' => 'catalogcol_item_ttl link ' . (empty($category['icon']) ? '' : 'big-padding-left')]) ?>
            </div>
            <ul class="catalogcol_item_list">
            <?php foreach($category['subcat'] as $sub): ?>
                <li><?php echo Html::a($sub['name'], ['catalog/index','cat' => $sub['id']]) ?></li>
            <?php endforeach; ?>
            </ul>
        </div>
        <?php endforeach; ?>
    </div>
    <?php /*endfor;*/ ?>
</div>
