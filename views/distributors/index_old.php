<?php

use yii\helpers\Html;
use app\models\Town;
use app\models\Country;
use yii\data\ActiveDataProvider;
use app\models\Distributors;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use dosamigos\google\maps\LatLng;
use dosamigos\google\maps\Map;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DistributorsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'app.internal.distribs');

$list = [
    '' => Yii::t('app', 'app.list.empty'),
];

$towns = Town::find()->orderBy('fstown_name');
if(Yii::$app->request->get('country')) {
    $towns->andWhere(['ficountry_id' => (int)Yii::$app->request->get('country')]);
}
$towns = $list + ArrayHelper::map($towns->all(), 'fitown_id', 'fstown_name');

$distribs = $list + ArrayHelper::map(Distributors::find()->groupBy('fsname')->orderBy('fsname')->all(), 'fidistr_id', 'fsname');

$country = $list + Country::getAll();

// $this->registerJsFile('http://maps.googleapis.com/maps/api/js?key=AIzaSyBKo3x7H9_bxGJZHFKORZbSBQvF0BLYfWQ&libraries=places&language=ru');
$this->registerJsFile('https://maps.googleapis.com/maps/api/js?key=AIzaSyBKo3x7H9_bxGJZHFKORZbSBQvF0BLYfWQ&libraries=places&language=ru');

$js = <<< JS
$(function() {
  
 
    $('.sel select').change(function(){
        if($(this).val()!='1'){
            $(this).addClass('bl');
        }
        else{
            $(this).removeClass('bl');
        }
    });
    /*for accordion*/
    $('body').on('click', '.dealerslist_item_tr', function(){
        var fn = $(this).data('fn');
        console.log(fn);
        var a = $(this).parent();
        if(a.hasClass('open')){
            a.removeClass('open');
            a.find('.dealerslist_item_det').slideToggle(600);
        }
        else{
            $('.dealerslist_item').each(function(){
                if($(this).hasClass('open')) { $(this).find('.dealerslist_item_det').slideToggle(600); $(this).removeClass('open'); }
            });
            a.addClass('open');
            a.find('.dealerslist_item_det').slideToggle(600);
            if(window.hasOwnProperty(fn)) {
              window[fn]();
            }
        }
        return false;
    });

    $('.filters').on('change select', '#select-country', function(context, data) {
        if(typeof  data == 'undefined') {
            $.getJSON('%country-url%', {
              country : $(this).val(),
              town : $("#select-town").val(),
              distrib : $("#select-distrib").val()
            }, function(data) {

                var active = $('#select-town').val();
                $('#select-town').find('option[value!=""]').remove();
                for(town in data.towns) {
                    $('#select-town').append(
                        $('<option/>')
                            .val(town)
                            .text(data.towns[town])
                    );
                }
                $('#select-town').val(active);
                $('#select-town').trigger('change', {internal : true});

                active = $('#select-distrib').val();
                $('#select-distrib').find('option[value!=""]').remove();
                for(distrib in data.distribs) {
                    $('#select-distrib').append(
                        $('<option/>')
                            .val(distrib)
                            .text(data.distribs[distrib])
                    );
                }
                $('#select-distrib').val(active);
                $('#select-distrib').trigger('change', {internal : true});
            });
        }
    });

    $('.filters').on('change select', '#select-town', function(context, data) {
        if(typeof  data == 'undefined') {
            $.getJSON('%town-url%', {
              town : $(this).val(),
              country : $("#select-country").val(),
              distrib : $("#select-distrib").val()
            }, function(data) {

                var active = $('#select-country').val();
                $('#select-country').find('option[value!=""]').remove();
                for(country in data.countrys) {
                    $('#select-country').append(
                        $('<option/>')
                            .val(country)
                            .text(data.countrys[country])
                    );
                }
                $('#select-country').val(active);
                $('#select-country').trigger('change', {internal : true});

                active = $('#select-distrib').val();
                $('#select-distrib').find('option[value!=""]').remove();
                for(distrib in data.distribs) {
                    $('#select-distrib').append(
                        $('<option/>')
                            .val(distrib)
                            .text(data.distribs[distrib])
                    );
                }
                $('#select-distrib').val(active);
                $('#select-distrib').trigger('change', {internal : true});
            });
        }
    });

    $('.filters').on('change select', '#select-distrib', function(context, data) {
        if(typeof  data == 'undefined') {
            $.getJSON('%distrib-url%', {
              distrib : $(this).val(),
              country : $("#select-country").val(),
              town : $("#select-town").val()
            }, function(data) {

                var active = $('#select-country').val();
                $('#select-country').find('option[value!=""]').remove();
                for(country in data.countrys) {
                    $('#select-country').append(
                        $('<option/>')
                            .val(country)
                            .text(data.countrys[country])
                    );
                }
                $('#select-country').val(active);
                $('#select-country').trigger('change', {internal : true});

                active = $('#select-town').val();
                $('#select-town').find('option[value!=""]').remove();
                for(town in data.towns) {
                    $('#select-town').append(
                        $('<option/>')
                            .val(town)
                            .text(data.towns[town])
                    );
                }
                $('#select-town').val(active);
                $('#select-town').trigger('change', {internal : true});
            });
        }
    });

    $('.filters').on('change select', 'select', function(context, data) {
        if(typeof data == 'undefined') {
            var filters = {};
            $(this).parents('.filters').find('input,select').each(function() {
                filters[$(this).attr('name')] = $(this).val();
            });
            $.get('%url%', filters)
            .done(function(data) {
                $('ul.dealerslist').replaceWith($(data).find('ul.dealerslist'));
            });
        }
    });
    $('.filters select').each(function() {
        $(this).trigger('change', {internal : true});
        //$(this).trigger('click', {internal : true});
        $(this).trigger('select', {internal : true});
    });
});
JS;

$js = str_replace( [
        '%url%',
        '%country-url%',
        '%town-url%',
        '%distrib-url%',
    ], [
        Url::to(['distributors/index']),
        Url::to(['distributors/select-country']),
        Url::to(['distributors/select-town']),
        Url::to(['distributors/select-distrib']),
    ], $js);

$this->registerJs($js);

?>
<div class="breadcrumbs">
    <div class="inner_width">
        <a href="#"><i class="ichome"></i>Главная</a>
        <span class="curr">Где купить</span>
    </div>
</div>
<div class="page_ttl hide">
    <div class="inner_width">
        <h1>Где купить</h1>
    </div>
</div>

<div class="inner_width content">
    <div class="servicettl">Где купить продукцию<span>Используя фильтры найдите диллера в своем регионе</span></div>
    <form class="filterwh filters">
        <div class="filterwh_tab">
            <div class="sel mod">
                <span class="ar"></span>
                <select id="select-country" name="country">
                    <?php foreach ($country as $id => $c): ?>
                    <option value="<?php echo $id ?>"><?php echo $c; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="sel mod">
                <span class="ar"></span>
                <select id="select-town" name="town">
                    <?php foreach ($towns as $id => $c): ?>
                        <option value="<?php echo $id ?>"><?php echo $c; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="sel mod">
                <span class="ar"></span>
                <select id="select-distrib" name="distrib">
                    <?php foreach ($distribs as $id => $c): ?>
                        <option value="<?php echo $id ?>"><?php echo $c; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="filter_item mod">
            <input id="checkbox1" class="checkbox" type="radio" name="chgr1">
            <label for="checkbox1">Опт</label>
        </div>
        <div class="filter_item mod">
            <input id="checkbox2" class="checkbox" type="radio" name="chgr1">
            <label for="checkbox2">Опт/Розница</label>
        </div>
        <div class="filter_item mod">
            <input id="checkbox3" class="checkbox" type="radio" name="chgr1">
            <label for="checkbox3">Розница</label>
        </div>
        <a class="reserfilter mod" href="#">Сбросить фильтры</a>
        <input type="submit" name="" class="catalog_item_more"/ value="Применить">

    </form>

    <ul class="dealerslist">
            <?php

            $query = Town::find();
            if(Yii::$app->request->get('town')) {
                $query->andWhere(['fitown_id' => (int)Yii::$app->request->get('town')]);
            }
            if(Yii::$app->request->get('country')) {
                $query->andWhere(['ficountry_id' => (int)Yii::$app->request->get('country')]);
            }
            $query->orderBy('fstown_name');

            $provider = new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'pageSize' => 99999,
                ],
            ]);

            ?>
            
                <?php foreach ($provider->models as $model): ?>
                    <?php echo $this->render('view-town', ['model' => $model]); ?>
                <?php endforeach; ?>
        
    </ul>
</div>
