<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CatalogRu */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app','Catalog Ru'),
]) . ' "' . $model->fsname . '"';
$this->params['admin'] = [
    'page_title' => $this->title,
    'main_title' => 'Каталог',
    'main_url' => 'catalog/admin'
];
?>
<div class="catalog-ru-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
