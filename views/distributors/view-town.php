<?php
/**
 * Created by PhpStorm.
 * User: Xander
 * Date: 19.03.2015
 * Time: 23:47
 */

use yii\helpers\Html;
use yii\widgets\DetailView;
use dosamigos\google\maps\LatLng;
use dosamigos\google\maps\Map;
use app\models\DistributorsPoint;
use yii\helpers\ArrayHelper;
use app\models\Town;
use yii\bootstrap\BootstrapPluginAsset;
/* @var $this yii\web\View */
/* @var $model app\models\Town */

$townName = $model->fstown_name;
$townId = $model->fitown_id;
$distributors = \app\models\Distributors::find()->where(['distributors_point.fitown_id' => $model->fitown_id])->innerJoin('distributors_point', 'distributors.fidistr_id = distributors_point.fidistr_id');

if(Yii::$app->request->get('distrib')) {
    $distributors->andWhere('distributors_point.fidistr_id = :distr', ['distr' => (int)Yii::$app->request->get('distrib')]);
}
$distributors->orderBy([
    'priority' => SORT_DESC,
    'fsname' => SORT_ASC,
]);
$distributors = $distributors->all();

if(!Yii::$app->request->isAjax) {
    $distributors = [];
}

$this->registerJs("
    $(function() {
        $('body').on('submit','.callback', function() {
            var mails=[]
            \$(this).closest('.dealerslist_item_td').find('a[href^=\"mailto:\"]').each(function( index ) {
                mails.push(\$(this).text());
            });
            \$.post('',{
                '_csrf':\$('meta[name=\"csrf-token\"]').attr('content'),
                'tel':\$(this).closest('.dealerslist_item_td').find('input[type=text]').val(),
                'mail':mails
            },function(data){
                if (data.send=='yes'){
                    \$('input[type=\"tel\"]').val('')
                    alert('Ваше сообщение отправлено!')
                }
            },'json');

            return false;
        })
    });
");


?>

    <?php foreach($distributors as $distrib) {

        $model = $distrib;
        $points = DistributorsPoint::find()->with('distributorsPointMails','distributorsPointPhones')->where(['fidistr_id' => $model->fidistr_id, 'fitown_id' => $townId])->all();
        if(empty($points)) {
            continue;
        }

        ?>

        <?php foreach($points as $modelPoint): ?>
            <?php
            $mails = [];
            $phones = [];
            foreach($modelPoint->distributorsPointMails as $m) {
                if(strlen($m->fsmail)) {
                    $mails[] = $m;
                }
            }
            foreach($modelPoint->distributorsPointPhones as $p) {
                if(strlen($p->fsphone)) {
                    $phones[] = $p;
                }
            }

            ?>

        <li class="dealerslist_item"
            data-wholesale="<?= $modelPoint->distribution_type_wholesale ?>"
            data-retail="<?= $modelPoint->distribution_type_retail ?>"
        >
            <div class="dealerslist_item_tr" data-fn="disclick<?=$modelPoint->fidistr_point_id?>">
                <div class="dealerslist_item_td">
                    <div class="dealerslist_item_name"><?php echo $model->fsname ?></div>
                </div>
                <div class="dealerslist_item_td">
                    <a class="dealerslist_item_lnk" href="<?php echo $model->fssite ? 'http://' . str_replace('http://','',$model->fssite) : '#' ?>"><?php echo $modelPoint->fsaddress ?></a>
                </div>
                <div class="dealerslist_item_td small">
                    <div class="dealerslist_item_name"><?php echo ''; //DistributorsPoint::getDistTypeName($modelPoint->dist_type); ?></div>
                </div>
                <div class="dealerslist_item_td mini">
                    <button class="dealerslist_item_btn">open</button>
                </div>
            </div>
            <div class="dealerslist_item_det">
                <?php if(strlen(trim($modelPoint->maps))): ?>
                <div class="dealerslist_item_td half" style="padding-right: 15px;">
                    <div class="<?= (strlen(trim($modelPoint->maps))) ? '':'mapp'?>">
                        <div id="map<?=$modelPoint->fidistr_point_id?>" style="height:245px;"></div>

                        <script type="text/javascript">

                            function disclick<?=$modelPoint->fidistr_point_id?>() {
                                function initMap<?=$modelPoint->fidistr_point_id?>() {
                                    var myLatLng<?=$modelPoint->fidistr_point_id?> = new google.maps.LatLng(<?php echo trim($modelPoint->maps,'()')?>);

                                    var map<?=$modelPoint->fidistr_point_id?> = new google.maps.Map(document.getElementById('map<?=$modelPoint->fidistr_point_id?>'), {
                                        zoom: 16,
                                        center: myLatLng<?=$modelPoint->fidistr_point_id?>
                                    });
                                    window.myMap<?=$modelPoint->fidistr_point_id?> = map<?=$modelPoint->fidistr_point_id?>;

                                    var marker = new google.maps.Marker({
                                        position: myLatLng<?=$modelPoint->fidistr_point_id?>,
                                        map: map<?=$modelPoint->fidistr_point_id?>
                                    });
                                }
                                initMap<?=$modelPoint->fidistr_point_id?>();

                                window.setTimeout(function() {
                                    var center = window.myMap<?=$modelPoint->fidistr_point_id?>.getCenter();
                                    google.maps.event.trigger(window.myMap<?=$modelPoint->fidistr_point_id?>, 'resize');
                                    window.myMap<?=$modelPoint->fidistr_point_id?>.setCenter(center);
                                }, 1500);
                            };

                        </script>

                    </div>
                </div>
                <?php endif; ?>
                <div class="dealerslist_item_td half">
                    <div class="dealerslist_item_ttl"><?=Yii::t('app', 'Контакты')?>:</div>
                    <?php foreach ($phones as $phone): ?>
                        <a href="tel:<?php echo $phone->fsphone; ?>" class="lnk"><?php echo $phone->fsphone; ?></a><br/>
                    <?php endforeach; ?>
                    <?php foreach ($mails as $phone): ?>
                        <a href="mailto:<?php echo $phone->fsmail; ?>" class="lnk"><?php echo $phone->fsmail; ?></a><br/>
                    <?php endforeach; ?>
                    <?php if(strlen($modelPoint->fssite)): ?>
                        <a href="<?php echo 'http://' . str_replace('http://','',$modelPoint->fssite) ?>" target="_blank" class="lnk"><?php echo 'http://' . str_replace('http://','',$modelPoint->fssite) ?></a><br/>
                    <?php endif; ?>
                    <?php if(!empty($mails)): ?>
                    <div class="dealerslist_item_ttl mod"><?=Yii::t('app', 'Заказ обратного звонка')?>:</div>
                    <form class="callback">
                        <input type="text" placeholder="<?=Yii::t('app', 'Введите номер с кодом')?>"/>
                        <input type="submit" value="<?=Yii::t('app', 'Заказать')?>"/>
                    </form>
                    <?php endif; ?>
                </div>
            </div>
        </li>


        <?php endforeach; ?>


    <?php }?>
