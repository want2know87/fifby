fif.by
======

Локально развернуть  
```cp ./.htaccess.dist ~/sites/fif/public_html/.htaccess```  
```ln -s ~/sites/fif/repo/fifby/web ~/sites/fif/public_html/```  
```ln -s ~/sites/fif/repo/fifru/web/data/img ~/sites/fif/public_html/web/data```  
```ln -s ~/sites/fif/repo/fifru/web/data/files ~/sites/fif/public_html/web/data```  

Логин и пароль для админа задавать в файле ```/config/admin.php```  

Файлы и папки заигнорены:  
- /config/_rules.php  
- /web/index.php  
- /web/*.xls  
- /web/*.xlsx  
- /web/*.docx  
- /web/*.pdf  
- /web/*.txt  
- /web/data/  
- /web/image/  
- /web/images/  

/views/catalog-index-level-new-2.php - вьюха с фильтром
