<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Distributors */

$this->title = Yii::t('app', 'Update Distributors: ') . ' ' . $model->fsname;
$this->params['admin'] = [
    'page_title' => $this->title,
    'main_title' => 'Дистрибьюторы',
    'main_url' => 'distributors/admin'
];
?>
<div class="distributors-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
