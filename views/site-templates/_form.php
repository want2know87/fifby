<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SiteTemplates */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="site-templates-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'fstemplate_name')->textInput(['maxlength' => 255]) ?>

    <?php if($model->isNewRecord): ?>
    <?= $form->field($model, 'fscode')->textInput(['maxlength' => 32]) ?>
    <?php endif; ?>

    <?= $form->field($model, 'fstitle')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'fstitle_en')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'fstemplate_code')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'fstemplate_code_en')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'fstemplate_seo')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'fstemplate_seo_en')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
