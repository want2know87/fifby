<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "news".
 *
 * @property string $finewsid
 * @property string $fstitle
 * @property string $fstext
 * @property string $fdcreate_date
 * @property string $fdtitle_image
 * @property string $fsanons
 * @property string $fsanons_general
 * @property string $fsimage_news
 * @property string $fsseo
 * @property string $lang
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fstitle'], 'required'],
            [['fstext', 'fsanons', 'fsanons_general', 'fsseo'], 'string'],
            [['fdcreate_date'], 'safe'],
            [['fstitle', 'fdtitle_image', 'fsimage_news'], 'string', 'max' => 256],
            ['lang', 'required'],
            ['lang', 'string', 'max' => 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'finewsid' => Yii::t('app', 'Finewsid'),
            'fstitle' => Yii::t('app', 'Fstitle'),
            'fstext' => Yii::t('app', 'Fstext'),
            'fdcreate_date' => Yii::t('app', 'Fdcreate Date'),
            'fdtitle_image' => Yii::t('app', 'fdtitle_image'),
            'fsanons' => Yii::t('app', 'fsanons'),
            'fsanons_general' => Yii::t('app', 'fsanons_general'),
            'fsimage_news' => Yii::t('app', 'fsimage_news'),
            'fsseo' => Yii::t('app', 'fsseo'),
            'lang' => 'Язык',
        ];
    }
}
