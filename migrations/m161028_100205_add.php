<?php

use yii\db\Schema;
use yii\db\Migration;

class m161028_100205_add extends Migration
{
    public function up()
    {
        $this->addColumn('catalog_ru', 'fsseo_en', Schema::TYPE_TEXT);
        $this->addColumn('site_templates', 'fstemplate_seo_en', Schema::TYPE_TEXT);
    }

    public function down()
    {
        $this->dropColumn('catalog_ru', 'fsseo_en');
        $this->addColumn('site_templates', 'fstemplate_seo_en');
    }
}
