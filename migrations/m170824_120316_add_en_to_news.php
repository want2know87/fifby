<?php

use yii\db\Migration;

class m170824_120316_add_en_to_news extends Migration
{
    public function safeUp()
    {
        $this->addColumn('news', 'lang', $this->string(2)->defaultValue('ru'));
    }

    public function safeDown()
    {
        $this->dropColumn('news', 'lang');
    }
}
