<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "selects_items".
 *
 * @property string $fiselect_item_id
 * @property integer $fiselect_id
 * @property integer $fiitem_id
 */
class SelectsItems extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'selects_items';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fiselect_id', 'fiitem_id'], 'required'],
            [['fiselect_id', 'fiitem_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fiselect_item_id' => 'Fiselect Item ID',
            'fiselect_id' => 'Fiselect ID',
            'fiitem_id' => 'Fiitem ID',
        ];
    }
}
