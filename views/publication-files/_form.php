<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PublicationFiles */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="publication-files-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?php if ($model->isNewRecord): ?>
    <input type="hidden" name="PublicationFiles[fipublic_id]" value="<?php echo Yii::$app->request->get('fipublic_id') ?>" />
    <?php endif; ?>

    <?= $form->field($model, 'fsname')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'priority')->textInput() ?>

    <?= $model->isNewRecord ? $form->field($model, 'fsfile')->fileInput() : '' ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
