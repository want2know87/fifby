<?php

use yii\db\Schema;
use yii\db\Migration;

class m150321_092700_add_link_cat extends Migration
{
    public function up()
    {
        $this->addColumn('catalog_ru', 'fslink', 'varchar(255)');
    }

    public function down()
    {
        $this->dropColumn('catalog_ru', 'fslink');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
