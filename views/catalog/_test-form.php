<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Pjax;
use kartik\typeahead\Typeahead;

$data = isset($data) ? $data : [];
?>

<div class="barinfo hidden1040">
<?php Pjax::begin(['enablePushState' => false]); ?>
    <b><?=Yii::t('app', 'Поиск по аналогам')?></b>
    <p><?=Yii::t('app', 'Корректно введите в строке поиска название аналога')?></p>
    <h5><?=Yii::$app->request->post('q')?></h5>
    <?=Html::beginForm(['test/item'], 'post', ['data-pjax' => '', 'class' => 'form-inline'])?>
        <?php /*echo Html::textInput('q', Yii::$app->request->post('q'), []);*/ ?>
        <div class="form-group">
        <?=Typeahead::widget([
            'name' => 'q',
            'dataset' => [
                [
                    'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
                    'display' => 'value',
                    'remote' => [
                        'url' => Url::to(['test/suggest']) . '?q=%QUERY',
                        'wildcard' => '%QUERY'
                    ],
                    'limit' => 10
                ]
            ],
            'options' => ['class' => 'form-control'],
            'pluginOptions' => ['highlight'=>true],
        ])?>
        </div>
        <?=Html::submitButton(Yii::t('app', 'Поиск'), ['class' => 'btn btn-default'])?>
    <?=Html::endForm()?>
    <?php if (!empty($data)): ?>
    <ul>
        <?php foreach ($data as $item): ?>
        <li><a href="<?=$item['url']?>"><?=$item['fsitem_name']?></a></li>
        <?php endforeach;?>
    </ul>
    <?php elseif (Yii::$app->request->isPost): ?>
    Ничего не найдено
    <?php endif; ?>
<?php Pjax::end(); ?>
</div>
