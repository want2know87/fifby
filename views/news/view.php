<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\News */

$this->registerJsFile('@web/js/social-likes.min.js', ['depends' => \yii\web\JqueryAsset::className()]);
$this->registerCssFile('@web/js/social-likes_flat.css');

?>

<article class="news_item">
  <div class="news_item_img">
    <?php if($model->fsimage_news): ?>
    <a href="<?php echo \yii\helpers\Url::to(['news/view', 'id' => $model->finewsid]) ?>"><?php echo Html::img(Yii::$app->request->baseUrl . '/' . $model->fsimage_news, ['style' => 'max-width:370px;max-height:216px;']) ?></a>
    <?php endif; ?>
  </div>
  <div class="news_item_inf">
    <a href="<?php echo \yii\helpers\Url::to(['news/view', 'id' => $model->finewsid]) ?>" class="catalogcol_item_ttl"><?php echo htmlspecialchars($model->fstitle) ?></a>
    <span class="news_item_date"><?php echo Yii::$app->formatter->asDate($model->fdcreate_date, 'd MMMM, yyyy'); ?></span>
    <p><?php echo htmlspecialchars($model->fsanons); ?></p>
  </div>
</article>
