
<div class="breadcrumbs">
    <div class="inner_width">
        <a href="<?php echo \yii\helpers\Url::to(['site/index']) ?>"><i class="ichome"></i><?= Yii::t('app', 'navigation.general') ?></a>
        <a href="<?php echo \yii\helpers\Url::to(['catalog/index']) ?>"><?= Yii::t('app', 'navigation.product_range') ?></a>
        <?php if (isset($breadcrumb['general_id']) && !empty($breadcrumb['general_id'])): ?>
        <a href="<?php echo \yii\helpers\Url::to(['catalog/index', 'cat' => $breadcrumb['general_id']]) ?>"><?=$breadcrumb['general_name']?></a>
        <?php endif ?>
        <?php if (isset($breadcrumb['middle_id']) && !empty($breadcrumb['middle_id'])): ?>
        <a href="<?php echo \yii\helpers\Url::to(['catalog/index', 'cat' => $breadcrumb['middle_id']]) ?>"><?=$breadcrumb['middle_name']?></a>
        <?php endif ?>
        <?php if (isset($breadcrumb['sublow_id']) && !empty($breadcrumb['sublow_id'])): ?>
            <a href="<?php echo \yii\helpers\Url::to(['catalog/index', 'cat' => $breadcrumb['sublow_id']]) ?>"><?=$breadcrumb['sublow_name']?></a>
        <?php endif ?>
        <span class="curr"><?php echo $breadcrumb['low_name'] ?></span>
    </div>
</div>
