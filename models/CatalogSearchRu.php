<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CatalogRu;

/**
 * CatalogSearchRu represents the model behind the search form about `app\models\CatalogRu`.
 */
class CatalogSearchRu extends CatalogRu
{
    public $q;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ficatalog_id', 'fiparent_catalog_id', 'fiorder'], 'integer'],
            [['fsname', 'fscatalog_img', 'fscatalog_text', 'q'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CatalogRu::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'ficatalog_id' => $this->ficatalog_id,
            'fiparent_catalog_id' => $this->fiparent_catalog_id,
            'fiorder' => $this->fiorder,
        ]);

        $query->andFilterWhere(['like', 'fsname', $this->fsname])
            ->andFilterWhere(['like', 'fscatalog_img', $this->fscatalog_img])
            ->andFilterWhere(['like', 'fscatalog_text', $this->fscatalog_text]);

        $query->orFilterWhere(['like', 'fsname', $this->q])
            ->orFilterWhere(['like', 'fscatalog_text', $this->q]);

        return $dataProvider;
    }
}
