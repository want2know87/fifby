<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Town;
use app\models\Region;
use app\models\Country;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DistributorsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'app.internal.distribs');
$this->params['admin'] = [
    'page_title' => $this->title,
];
$towns = Town::getAll();
$country = Country::getAll();

?>
<div class="distributors-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'app.internal.create-distrib'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'fitown_id' => [
                'class' => \yii\grid\DataColumn::className(),
                'attribute' => 'fitown_id',
                'format' => 'html',
                'value' => function($model) use ($towns) {
                    return $towns[$model->fitown_id];
                }
            ],
            'ficountry_id' => [
                'class' => \yii\grid\DataColumn::className(),
                'attribute' => 'ficountry_id',
                'format' => 'html',
                'value' => function($model) use ($country) {
                    return $country[$model->ficountry_id];
                }
            ],
            /*'firegion_id' => [
                'class' => \yii\grid\DataColumn::className(),
                'attribute' => 'firegion_id',
                'format' => 'html',
                'value' => function($model) use ($regions) {
                    return $regions[$model->firegion_id];
                }
            ],*/
            'fsname',
            // 'fsaddress',
            // 'maps',

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {delete}'],
        ],
    ]); ?>

</div>
