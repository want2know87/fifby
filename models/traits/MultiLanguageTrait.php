<?php

namespace app\models\traits;

use Yii;

trait MultiLanguageTrait
{
    public function __get($name)
    {
        $lang = Yii::$app->language;
        $eng = $name .'_en';
        if ($lang == 'en'
            && $this->hasAttribute($eng)
            && !empty($this->$eng)
        ) {
            $name = $eng;
        }
        return parent::__get($name);
    }
}
