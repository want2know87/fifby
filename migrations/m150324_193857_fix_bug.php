<?php

use yii\db\Schema;
use yii\db\Migration;

class m150324_193857_fix_bug extends Migration
{
    public function up()
    {
        $this->dropForeignKey('fk_distrib_country','distributors');
        $this->alterColumn('distributors', 'ficountry_id', 'int(10) default null');
    }

    public function down()
    {
        echo "m150324_193857_fix_bug cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
