<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Publications;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PublicationsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Управление типами публикаций';
$this->params['admin'] = [
    'page_title' => $this->title,
    'main_title' => $this->title,
    'main_url' => 'publication-type/admin'
];
?>

<div class="publications-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label' => 'Тип',
                'value' => function ($model) {
                    return Publications::$types[$model->key];
                },
                'filter' => false,
            ],

            [
                'attribute' => 'position',
                'filter' => false,
            ],

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update}'],
        ],
    ]); ?>

</div>
