<?php

use yii\db\Schema;
use yii\db\Migration;

class m161003_153507_add_sort_priority_to_publications_files_table extends Migration
{
    public function up()
    {
        $this->addColumn('publication_files', 'priority', Schema::TYPE_INTEGER . ' not null default 0');
    }

    public function down()
    {
        $this->dropColumn('publication_files', 'priority');
    }
}
