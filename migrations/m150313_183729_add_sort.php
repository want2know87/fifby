<?php

use yii\db\Schema;
use yii\db\Migration;

class m150313_183729_add_sort extends Migration
{
    public function up()
    {
        $this->addColumn('catalog_ru', 'fiorder', 'int(10) default 100');
    }

    public function down()
    {
        $this->dropColumn('catalog_ru', 'fiorder');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
