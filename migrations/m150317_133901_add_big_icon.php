<?php

use yii\db\Schema;
use yii\db\Migration;

class m150317_133901_add_big_icon extends Migration
{
    public function up()
    {
        $this->addColumn('catalog_ru', 'fsbigicon', 'varchar(255)');
    }

    public function down()
    {
        $this->dropColumn('catalog_ru', 'fsbigicon');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
