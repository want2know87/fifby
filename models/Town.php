<?php

namespace app\models;

use Yii;
use app\models\traits\MultiLanguageTrait;

/**
 * This is the model class for table "town".
 *
 * @property integer $fitown_id
 * @property string $fstown_name
 * @property integer $ficountry_id
 *
 * @property Distributors[] $distributors
 * @property Country $ficountry
 * @property Region $firegion
 */
class Town extends \yii\db\ActiveRecord
{
    use MultiLanguageTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'town';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ficountry_id'], 'required'],
            [['ficountry_id'], 'integer'],
            [['fstown_name'], 'string', 'max' => 55],
            [['ficountry_id', 'fstown_name'], 'unique', 'targetAttribute' => ['ficountry_id', 'fstown_name'], 'message' => 'The combination of Fstown Name, Firegion ID and Ficountry ID has already been taken.'],
            [['fstown_name_en'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fitown_id' => Yii::t('app', 'app.internal.number'),
            'fstown_name' => Yii::t('app', 'app.internal.town-name'),
            'fstown_name_en' => Yii::t('app', 'app.internal.town-name') . ' En',
            'ficountry_id' => Yii::t('app', 'app.select-country'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistributors()
    {
        return $this->hasMany(Distributors::className(), ['fitown_id' => 'fitown_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFicountry()
    {
        return $this->hasOne(Country::className(), ['ficountry_id' => 'ficountry_id']);
    }

    /**
     * @return array
     */
    public static function getAll() {
        $array = Town::find()->all();
        $list = [];
        /** @var Town $town */
        foreach($array as $town) {
            $list[$town->fitown_id] = $town->fstown_name;
        }
        return $list;
    }
}
