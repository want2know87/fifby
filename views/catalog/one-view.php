<?php

use app\models\Items;
use app\models\ItemsPictures;
use yii\bootstrap\ButtonDropdown;
use yii\bootstrap\Tabs;
use yii\helpers\Html;
/** @var Items $item */

/*foreach($params as $param) {
    echo $param['name'], $param['value'];
}*/
$breadCrumbs['sublow_name']=$breadCrumbs['low_name'];
$breadCrumbs['sublow_id']=$breadCrumbs['low_id'];
$breadCrumbs['low_name']=$item->fsitem_name;
$i = 0;

$prices = ['BYN', 'RUR', 'USD'];

$pictures = ItemsPictures::find()->where(['fiitem_id' => $item->fiitem_id])->all();

$this->title = $item->fsitem_name;
$this->seoFields = $item->fsseo;

$fsparamNameField = Yii::$app->language == 'en' ? 'fsparam_name_en' : 'fsparam_name';
$properties = (new \yii\db\Query())
  ->select('pc.fsparam_name as `name`, pc.fsparam_name_en as `name_en`, pv.fsparam_value as `value`, pv.fiorder')
  ->from(['pv' => 'params_values'])
  ->innerJoin(['pc' => 'params_cat'], 'pv.fiparam_id = pc.fiparam_id')
  ->where(['pv.fiitem_id' => $item->fiitem_id])
  /*->orderBy(['CASE WHEN pv.fiorder IS NULL then 99999 ELSE pv.fiorder END'])*/
  ->all(Yii::$app->db);

usort($properties, function($el1, $el2) {
  return (int)$el1['fiorder'] > (int)$el2['fiorder'];
});

$tech = '';
$i = 0;

foreach($properties as $prop) {
    $tech .= '<tr>' . '<td class="properties_name">'
    . (Yii::$app->language == 'en' && $prop['name_en'] ? $prop['name_en'] : $prop['name'])
    . '</td>' . '<td class="properties_value">' . $prop['value'] . '</td>' . '</tr>';
    $i++;
}

$pictures = ItemsPictures::find()->where(['fiitem_id' => $item->fiitem_id])->all();

$scheme = '<div class="col-xs-12 padding-none schemas">';

$index = 0;

foreach($pictures as $picture) {
  $scheme .= '<div style="float: left; min-width: 50%;"><b>' . $picture->fsname . '</b></br>';
  $scheme .=  \yii\helpers\Html::img(Yii::$app->request->baseUrl . '/' . $picture->fsimage, ['style' => 'max-width:330px;padding-top:10px;']) . '</div>';
}

$scheme .= '</div>';

$js = <<< JS
$(document).ready(function(){
    $('.tovar_navtab span').click(function(){
      var a = $(this).attr('data-tab');
      $('.tovar_navtab span').removeClass('act');
      $('.tovar_navtab i').removeClass('left').removeClass('center').removeClass('right');
      $(this).toggleClass('act');
      if(a==0){ $('.tovar_navtab i').addClass('left');}
      if(a==1){ $('.tovar_navtab i').addClass('center');}
      if(a==2){ $('.tovar_navtab i').addClass('right');}
      $('.tovar_tab.act').hide().removeClass('act');
      $('.tovar_tab').each(function(){
        if($(this).attr('data-tab')==a){
          $(this).slideToggle(600).addClass('act');
        }
      });
    });
  });
JS;

$this->registerJs($js);
?>

<?php echo $this->render('/catalog/breadcrumb', ['breadcrumb' => $breadCrumbs, 'social' => ['title' => $item->fsitem_name, 'link' => \yii\helpers\Url::to(['catalog/items', 'id' => 2], true)] ]) ?>

  <div class="page_ttl">
    <div class="inner_width">
      <h1><?php echo Html::encode($item->fsitem_name) ?></h1>
      <?php if(strlen($item->fspassport) && file_exists(Yii::getAlias('@webroot').'/'.$item->fspassport)): ?>
        <a href="<?php echo Yii::$app->request->baseUrl . '/' . $item->fspassport ?>" class="tovar_pass"><?=Yii::t('app', 'Паспорт Изделия')?> </a>
      <?php endif; ?>
      <a href="http://forum.fif.by/viewforum.php?f=2" class="head_link right"><?=Yii::t('app', 'Обсудить на форуме')?></a>
    </div>
  </div>

  <div class="inner_width">
    <div class="tovar_wrap ">
      <div class="tovar_l">
        <div class="tovar_img">
          <?php echo \yii\helpers\Html::img(Yii::$app->request->baseUrl . '/' . $item->fsitem_picture,['alt'=>$breadCrumbs['sublow_name'].' '.$item->fsitem_name]) ?>
        </div>
        <div class="tovar_price">
          <?php
          $itemPrice = [];
          foreach($prices as $name) {
            if(!empty($item['fsprice_' . strtolower($name)]) && strlen($item['fsprice_' . strtolower($name)])) {
              $itemPrice[$name] = $item['fsprice_' . strtolower($name)];
            }
          }
          if(!empty($itemPrice)) {
            echo '<span>'. Yii::t('app', 'Цена') . ':</span>';
            $priceNameDef = '';
            foreach ($itemPrice as $name => $pr) {
              echo '<span class="pr">', $pr, '</span>';
              $priceNameDef = $name;
              break;
            }
            echo "<div class=\"sel\"><span class=\"ar\"></span>";
            echo "<select class=\"sel\">";
            foreach ($itemPrice as $name => $pr) {
              echo "<option data-price=\"", $pr, "\">", $name, "</option>";
            }
            echo "</select></div>";
          }
          ?>
        </div>
        <a href="<?php echo \yii\helpers\Url::to(['distributors/index']) ?>" class="tovar_buy"><?=Yii::t('app', 'Где купить')?></a>
      </div>
      <div class="tovar_r">
        <div class="tovar_navtab">
          <i></i>
          <span data-tab='0' class="act"><?=Yii::t('app', 'Описание')?></span>
          <span data-tab='1' class='hidden1080'><?=Yii::t('app', 'Технические характеристики')?></span>
          <span data-tab='1' class='hide visible1080bl'><?=Yii::t('app', 'Характеристики')?></span>
          <span data-tab='2' class='hidden1080'><?=Yii::t('app', 'Схема подключения')?></span>
          <span data-tab='2' class='hide visible1080bl'><?=Yii::t('app', 'Схема')?> </span>
        </div>
        <div class='tovar_tab act' data-tab='0'>
          <div class="tovar_features tone_features">
            <b><?=Yii::t('app', 'Отличительные особенности реле')?>:</b>
            <p><?php echo $item->fsitem_small_desc; ?></p>
          </div>
          <p>
          <?php $names = [
                'Назначение' => '<b class="tovar_b">Назначение</b>',
                'Область применения' => '<b class="tovar_b">Область применения</b>',
                'Принцип работы' => '<b class="tovar_b">Принцип работы</b>',
            ];

            // $str = strip_tags($item->fsitem_bottom_text, '<a></a>');
            $str = $item->fsitem_bottom_text;
            foreach ($names as $key => $value) {
              $str = preg_replace('/'.$key.'/', $value, $str, 1);
            }

            echo $str;

            ?>
          </p>
        </div>
        <div class='tovar_tab ' data-tab='1'>
          <b class="tovar_b"><?=Yii::t('app', 'Технические характеристики')?></b>
          <div class="table-container">
            <table>
              <tbody>
              <?php echo $tech; ?>
              </tbody>
            </table>
          </div>
        </div>
        <div class='tovar_tab ' data-tab='2'>
          <div class="tovar_features">
              <div style="width: 100%; ">
                <?php echo $scheme; ?>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>

<div class="inner_width ">
  <div class="description">
    <div class="separator"></div>
  </div>
</div>
<br/>

<?php if ($item->similarItems): ?>
<style>
 .new-product {
    padding-top: 30px;
}

.evr-this {
    padding-bottom: 30px;
    padding-top: 50px;
    font-size: 24px;
    color: #213149;
}
.center-align {
    text-align: center;
}
.padding-none {
    padding: 0;
}
.owl-carousel {
    display: none;
    position: relative;
    width: 100%;
    -ms-touch-action: pan-y;
}
.owl-carousel .owl-wrapper-outer {
    overflow: hidden;
    position: relative;
    width: 100%;
}
.catalogs-items .item-one {
    border: 1px solid #b2b2b2;
    min-height: 330px;
    margin-bottom: 20px;
}
.slide-elements .item-one .title {
    margin-bottom: -3px;
}

.item-one .title {
    margin-top: 5px;
    font-size: 16px;
    font-weight: bold;
    margin-bottom: 15px;
}
.slide-elements .cat-link {
    font-family: Arial;
    font-size: 12.82px;
    margin-bottom: 5px;
}
.item-one .price {
    margin-left: -5px;
}


.item-one .price {
    position: absolute;
    bottom: 70px;
    height: 30px;
}
.show-more-wrap {
    padding: 0 20px 0 0;
    position: absolute;
    bottom: 10px;
    width: 100%;

}
.catalogs-items .owl-item {
    padding: 0 5px;
}
.flex-direction-nav {
    position: relative;
    z-index: 100;
    float: right;
    right: 0;
    z-index: 0;
    margin-top: -45px;
    margin-right: 5px;
}
.slides, .slides>li, .flex-control-nav, .flex-direction-nav {
    margin: 0;
    padding: 0;
    list-style: none;
}
.flex-nav-prev, .flex-nav-next {
    display: inline;
    display: inline-block;
}
.flex-direction-nav span.btton {
    background: #fff none repeat scroll 0 0;
    border: 2px solid #7b7b7b;
    color: #7b7b7b;
    display: inline-block;
    height: 34px;
    text-align: center;
    text-transform: uppercase;
    transition: all .2s ease-out 0s;
    width: 34px;
    font-family: sans-serif;
    font-weight: bold;
    line-height: 30px;
}
.flex-direction-nav span.btton:hover {
    background: #00e086 none repeat scroll 0 0;
    border: 2px solid #06a35c;
    color: #fff;
}

/*.col-md-4 {
    width: 28.333333%;
}*/

.similar_products .item-one {
    float: none;
    display: inline-block;
    margin-right: -4px;
}
.similar_products .slide-elements{
    text-align:center;
}

.item-one{
  margin: 0 10px;
}
</style>
<style>

  /*
 *  Core Owl Carousel CSS File
 *  v1.3.3
 */

/* clearfix */
.owl-carousel .owl-wrapper:after {
  content: ".";
  display: block;
  clear: both;
  visibility: hidden;
  line-height: 0;
  height: 0;
}
/* display none until init */
.owl-carousel{
  display: none;
  position: relative;
  width: 100%;
  -ms-touch-action: pan-y;
}
.owl-carousel .owl-wrapper{
  display: none;
  position: relative;
  -webkit-transform: translate3d(0px, 0px, 0px);
}
.owl-carousel .owl-wrapper-outer{
  overflow: hidden;
  position: relative;
  width: 100%;
}
.owl-carousel .owl-wrapper-outer.autoHeight{
  -webkit-transition: height 500ms ease-in-out;
  -moz-transition: height 500ms ease-in-out;
  -ms-transition: height 500ms ease-in-out;
  -o-transition: height 500ms ease-in-out;
  transition: height 500ms ease-in-out;
}

.owl-carousel .owl-item{
  float: left;
}
.owl-controls .owl-page,
.owl-controls .owl-buttons div{
  cursor: pointer;
}
.owl-controls {
  -webkit-user-select: none;
  -khtml-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
}

/* mouse grab icon */
.grabbing {
    cursor:url(grabbing.png) 8 8, move;
}

/* fix */
.owl-carousel  .owl-wrapper,
.owl-carousel  .owl-item{
  -webkit-backface-visibility: hidden;
  -moz-backface-visibility:    hidden;
  -ms-backface-visibility:     hidden;
  -webkit-transform: translate3d(0,0,0);
  -moz-transform: translate3d(0,0,0);
  -ms-transform: translate3d(0,0,0);
}
.similar_products{
    margin-bottom: 57px;
  }
</style>
<style>
  /*
*   Owl Carousel Owl Demo Theme
* v1.3.3
*/

.owl-theme .owl-controls{
  margin-top: 10px;
  text-align: center;
}

/* Styling Next and Prev buttons */

.owl-theme .owl-controls .owl-buttons div{
  color: #FFF;
  display: inline-block;
  zoom: 1;
  *display: inline;/*IE7 life-saver */
  margin: 5px;
  padding: 3px 10px;
  font-size: 12px;
  -webkit-border-radius: 30px;
  -moz-border-radius: 30px;
  border-radius: 30px;
  background: #869791;
  filter: Alpha(Opacity=50);/*IE7 fix*/
  opacity: 0.5;
}
/* Clickable class fix problem with hover on touch devices */
/* Use it for non-touch hover action */
.owl-theme .owl-controls.clickable .owl-buttons div:hover{
  filter: Alpha(Opacity=100);/*IE7 fix*/
  opacity: 1;
  text-decoration: none;
}

/* Styling Pagination*/

.owl-theme .owl-controls .owl-page{
  display: inline-block;
  zoom: 1;
  *display: inline;/*IE7 life-saver */
}
.owl-theme .owl-controls .owl-page span{
  display: block;
  width: 12px;
  height: 12px;
  margin: 5px 7px;
  filter: Alpha(Opacity=50);/*IE7 fix*/
  opacity: 0.5;
  -webkit-border-radius: 20px;
  -moz-border-radius: 20px;
  border-radius: 20px;
  background: #869791;
}

.owl-theme .owl-controls .owl-page.active span,
.owl-theme .owl-controls.clickable .owl-page:hover span{
  filter: Alpha(Opacity=100);/*IE7 fix*/
  opacity: 1;
}

/* If PaginationNumbers is true */

.owl-theme .owl-controls .owl-page span.owl-numbers{
  height: auto;
  width: auto;
  color: #FFF;
  padding: 2px 10px;
  font-size: 12px;
  -webkit-border-radius: 30px;
  -moz-border-radius: 30px;
  border-radius: 30px;
}

/* preloading images */
.owl-item.loading{
  min-height: 150px;
  background: url(AjaxLoader.gif) no-repeat center center
}
</style>
<div class="inner_width similar_products">
    <div class="container padding-none ">
      <div class="col-xs-12 padding-none new-product evr-this center-align">
      <?=Yii::t('app', 'Похожие товары')?>
      </div>
      <p class="visible-xs">&nbsp;</p>
      <div class="col-xs-12 padding-none catalogs-items slider" style="margin-top: 20px;">
        <div class="slide-elements" id="owl-newitem">
        <?php foreach($item->similarItems as $similar): ?>
          <?php if(!empty($similar['fsitem_before_text'])): ?>
            <div class="col-xs-12 padding-none sub-item-desc">
              <?php echo $similar['fsitem_before_text'] ?>
            </div>
          <?php endif; ?>
          <?php echo $this->render('/catalog/item-similar', [
            'item' => $similar,
            'prices' => $prices,
            'cats' => [],//$cats,
            'page' => 'general'
          ]) ?>
        <?php endforeach; ?>
        </div>
      </div>
    </div>
</div>
<?php endif; ?>
