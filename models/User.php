<?php

namespace app\models;

class User extends \yii\base\Object implements \yii\web\IdentityInterface
{
    public $id;
    public $username;
    public $password;
    public $authKey;
    public $accessToken;

    private static $users = [];

    private static function getUserList()
    {
        if (empty(self::$users)) {
            self::$users = require(__DIR__ . '/../config/admin.php');
        }

        return self::$users;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        $users = self::getUserList();

        $pwd = \Yii::$app->session->get('password');
        if(isset($users[$id]) && $pwd !== $users[$id]['password']) {
            return null;
        }
        return isset($users[$id]) ? new static($users[$id]) : null;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        $users = self::getUserList();

        foreach ($users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param  string      �username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        $users = self::getUserList();

        foreach ($users as $user) {
            if (strcasecmp($user['username'], $username) === 0) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param  string  �password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        $result = $this->password === $password;
        if($result) {
            \Yii::$app->session->set('password', $password);
        }
        return $result;
    }
}
