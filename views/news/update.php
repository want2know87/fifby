<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\News */

$this->title = Yii::t('app', 'Update News') . ' : ' . $model->fstitle . '';

$this->params['admin'] = [
    'page_title' => $this->title,
    'main_title' => 'Новости',
    'main_url' => 'news/admin'
];

?>
<div class="news-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
