<?php

use yii\db\Migration;

class m170925_160458_add_en_to_param extends Migration
{
    public function safeUp()
    {
        $this->addColumn('params_cat', 'fsparam_name_en', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn('params_cat', 'fsparam_name_en');
    }
}
