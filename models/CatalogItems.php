<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "catalog_items".
 *
 * @property integer $ficatalog_item_id
 * @property integer $ficatalog_id
 * @property integer $fiitem_id
 */
class CatalogItems extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'catalog_items';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ficatalog_id', 'fiitem_id'], 'required'],
            [['ficatalog_id', 'fiitem_id'], 'integer'],
            [['ficatalog_id', 'fiitem_id'], 'unique', 'targetAttribute' => ['ficatalog_id', 'fiitem_id'], 'message' => 'The combination of Ficatalog ID and Fiitem ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ficatalog_item_id' => Yii::t('app', 'Ficatalog Item ID'),
            'ficatalog_id' => Yii::t('app', 'Ficatalog ID'),
            'fiitem_id' => Yii::t('app', 'Fiitem ID'),
        ];
    }
}
