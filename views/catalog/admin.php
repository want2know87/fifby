<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\CatalogRu;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

$parent = ['' => Yii::t('app', "app.list.empty"),];
$parentsQeury = CatalogRu::find()->where(['fiparent_catalog_id' => null])->orWhere(['fiparent_catalog_id'=>''])->all();
/** @var CatalogRu $parents */
foreach($parentsQeury as $parents) {
    $parent[$parents->ficatalog_id] = $parents->fsname;
}

$parentsQeury = CatalogRu::find()->where(['fiparent_catalog_id' => array_filter(array_keys($parent))])->all();
/** @var CatalogRu $parents */
foreach($parentsQeury as $parents) {
    $parent[$parents->ficatalog_id] = $parents->fsname;
}

/* @var $this yii\web\View */
/* @var $searchModel app\models\CatalogSearchRu */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Catalog Rus');
$this->params['admin'] = [
    'page_title' => $this->title,
];
$js = <<< JS
$(function() {
    $('table').on('change', 'select.form-control', function() {
        $.get('%url%', {id:$(this).data('id'), parent:$(this).val()});
    });
    $('table').on('click', 'tr button.save-order', function() {
        var button = $(this);
        button.button('loading');
        var name = $(this).closest('td').find('input').attr('name');
        $.get('%urlSaveOrder%', {id : $(this).closest('td').find('input').data('id'), order : $(this).closest('td').find('input').val()})
        .always(function() {
            button.button('reset');
        });
    });
});
JS;

$js = str_replace(
    array(
        '%url%',
        '%urlSaveOrder%'
    ), array(
        \yii\helpers\Url::to(['catalog/update-parent']),
        \yii\helpers\Url::to(['catalog/update-order']),
    ), $js);

\yii\bootstrap\BootstrapPluginAsset::register($this);
$this->registerJs($js);

?>
<div class="catalog-ru-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'add.catalog.ru', [
    'modelClass' => Yii::t('app', 'Catalog Ru'),
]), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            /*'ficatalog_id',*/
            'fsname',
            'fiparent_catalog_id' => [
                'attribute' => 'fiparent_catalog_id',
                'class' => \yii\grid\DataColumn::className(),
                'format' => 'raw',
                'value' => function($model) use($parent) {
                    /** @var CatalogRU $model */
                    return Html::dropDownList('catalog['. $model->ficatalog_id . ']', $model->fiparent_catalog_id, $parent, ['data-id' => $model->ficatalog_id, 'class' => 'form-control']);
                    /*return isset($parent[$model->fiparent_catalog_id]) ? htmlspecialchars($parent[$model->fiparent_catalog_id]) : '';*/
                },
                'filter' => ArrayHelper::map(CatalogRu::find()->asArray()->all(), 'ficatalog_id', 'fsname'),
            ],
            'fscatalog_img' => [
                'attribute' => 'fscatalog_img',
                'class' => \yii\grid\DataColumn::className(),
                'format' => 'raw',
                'value' => function($model){
                    /** @var CatalogRU $model */
                    return Html::img(Yii::$app->request->baseUrl . '/' . $model->fscatalog_img, ['style' => 'max-width:200px;']);
                    /*return isset($parent[$model->fiparent_catalog_id]) ? htmlspecialchars($parent[$model->fiparent_catalog_id]) : '';*/
                },
                'filter' => false,
            ],
            'order' => [
                'class' => \yii\grid\DataColumn::className(),
                'attribute' => 'fiorder',
                'format' => 'raw',
                'value' => function($model) {
                  /** @var CatalogRU $model */
                  return Html::textInput('order[' . $model->ficatalog_id . ']', $model->fiorder, ['style' => 'width:100%;', 'data-id' => $model->ficatalog_id]) . Html::button('Сохранить', ['class' => 'btn btn-primary save-order', "data-loading-text" => "Сохранение...", 'style' => 'margin-left:5px;']);
                },
                'filter' => true, // CatalogRu::find()->orderBy('fiorder')->asArray()->all(),
            ],
            'fscatalog_text',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'urlCreator' => function($action, $model, $key, $index) {
                    $params = is_array($key) ? $key : ['id' => (string) $key];
                    $params[0] = $action;
                    $params += ['page' => (int)Yii::$app->request->get('page')];

                    return Url::toRoute($params);
                },
            ],
        ],
    ]); ?>

</div>
